package com.zjson.alibaba.commons.tools.log;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.zjson.alibaba.commons.tools.annotation.LogOperation;
import com.zjson.alibaba.commons.tools.config.ModuleConfig;
import com.zjson.alibaba.commons.tools.constant.Constant;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import com.zjson.alibaba.commons.tools.utils.HttpContextUtils;
import com.zjson.alibaba.commons.tools.utils.IpUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.http.HttpHeaders;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.*;

/**
 * 操作日志处理抽象类
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2020/6/10 17:53
 */
@Data
@Slf4j
public abstract class AbstractOperationLogger implements OperationLogger {

    @Resource
    private ModuleConfig moduleConfig;

    /**
     * 抽象日志处理方法
     *
     * @param operationLog 操作日志信息
     */
    protected abstract void saveLog(BaseLog operationLog);

    @Override
    public void saveLog(UserDetail user, ProceedingJoinPoint joinPoint, long time, Integer status) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        SysLogOperation logOperation = new SysLogOperation();
        LogOperation annotation = method.getAnnotation(LogOperation.class);
        if (annotation != null) {
            // 注解上的描述
            logOperation.setOperation(annotation.value());
        }

        if (user != null) {
            logOperation.setCreateBy(user.getId());
            logOperation.setCreatorName(user.getUsername());
        }

        logOperation.setType(annotation.type().value());
        logOperation.setModule(moduleConfig.getName());
        logOperation.setStatus(status);
        logOperation.setRequestTime((int) time);
        logOperation.setCreateTime(LocalDateTime.now());

        // 请求相关信息
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        if (Objects.nonNull(request)) {
            logOperation.setIp(IpUtils.getIpAddr(request));
            logOperation.setUserAgent(request.getHeader(HttpHeaders.USER_AGENT));
            logOperation.setRequestUri(request.getRequestURI());
            logOperation.setRequestMethod(request.getMethod());
            Enumeration<String> headerNames = request.getHeaderNames();
            Map<String, List<String>> headers = Maps.newHashMap();
            if (headerNames != null) {
                while (headerNames.hasMoreElements()) {
                    String name = headerNames.nextElement();
                    Enumeration<String> values = request.getHeaders(name);
                    List<String> data = Lists.newArrayList();
                    while (values.hasMoreElements()) {
                        data.add(values.nextElement());
                    }
                    headers.put(name, data);
                }
            }
            List<String> terminalHeaders = headers.get(Constant.TERMINAL_CLIENT);
            // 操作终端类型
            Integer terminalClient = null;
            try {
                terminalClient = CollectionUtils.isEmpty(terminalHeaders) ? null : Integer.valueOf(terminalHeaders.get(0));
            } catch (NumberFormatException e) {
                throw new CustomException("非法的终端标识[" + Constant.TERMINAL_CLIENT + "=" + terminalHeaders.get(0) + "]");
            }
            logOperation.setRequestHeaders(JSON.toJSONString(headers));
            logOperation.setTerminalClient(terminalClient);
        }

        // 请求参数
        Object[] args = joinPoint.getArgs();
        try {
            if (args != null && args.length > 0) {
                String params;
                Object obj = args[0];
                if (obj instanceof MultipartFile) {
                    params = "params type MultipartFile";
                } else if (obj instanceof ServletRequest) {
                    params = "params type ServletRequest";
                } else {
                    params = JSON.toJSONString(obj);
                }
                logOperation.setRequestParams(params);
            }
        } catch (Exception e) {
            log.error("操作参数记录失败，放弃操作参数记录");
            log.error("操作参数记录失败", e);
        }

        //保存到Redis队列里
        saveLog(logOperation);
    }
}
