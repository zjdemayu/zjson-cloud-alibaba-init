package com.zjson.alibaba.commons.tools.serializer;


import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.zjson.alibaba.commons.tools.constant.AesSaltConstant;
import com.zjson.alibaba.commons.tools.utils.encrypt.AesUtils;

import java.lang.reflect.Type;
import java.math.BigDecimal;

/**
 * created on 2020/11/4 18:11
 *
 * @author yaozou
 * @since v1.0.0
 */
public class AmountDeserializer implements ObjectDeserializer {
    @Override
    public BigDecimal deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
        if (fieldName != null){
            String amountStr = (String) fieldName;
            String amountDecrypt = AesUtils.decrypt(amountStr, AesSaltConstant.AMOUNT_SALT);
            return new BigDecimal(amountDecrypt);
        }
        return BigDecimal.ZERO;
    }

    @Override
    public int getFastMatchToken() {
        return 0;
    }
}
