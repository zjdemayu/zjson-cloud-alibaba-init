package com.zjson.alibaba.commons.tools.redis.lock;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * Description
 *
 * @author Administrator
 * @version 1.0
 * @since 2020/11/16 19:46
 */
@Component
public class DistributedLock {

	@Resource
	private RedisTemplate<String, Object> redisTemplate;

	/**
	 * 默认锁住5000ms
	 */
	public static final long DEFAULT_LOCK_MILLS = 5000;

	/**
	 * 获取锁的默认超时时间
	 */
	public static long defaultTimeout = 10000;

	/**
	 * 获得锁 非阻塞
	 *
	 * @param key         key
	 * @param millisecond 锁住时长
	 * @return boolean
	 */
	public boolean getLock(String key, long millisecond) {
		Boolean success = redisTemplate.opsForValue().setIfAbsent(key, "lock",
				millisecond, TimeUnit.MILLISECONDS);
		return success != null && success;
	}

	/**
	 * 自旋锁
	 *
	 * @param key key
	 * @param lockMillisecond 锁住时间
	 * @return boolean
	 */
	public boolean tryLock(String key, long lockMillisecond) {
		long start = System.currentTimeMillis();
		try {
			for ( ; ; ) {
				//SET命令返回OK ，则证明获取锁成功
				Boolean ret = redisTemplate.opsForValue().setIfAbsent(key, "lock",
						lockMillisecond, TimeUnit.MILLISECONDS);
				if (null != ret && ret) {
					return true;
				}
				//否则循环等待，在timeout时间内仍未获取到锁，则获取失败
				long end = System.currentTimeMillis() - start;
				if (end >= defaultTimeout) {
					return false;
				}
			}
		} finally {

		}
	}

	/**
	 * 自旋锁
	 *
	 * @param key key
	 * @return boolean
	 */
	public boolean tryDefaultLock(String key) {
		return this.tryLock(key, DEFAULT_LOCK_MILLS);
	}

	/**
	 * 获得一个5s的分布式锁 非阻塞
	 *
	 * @param lockId key
	 * @return boolean
	 */
	public boolean getDefaultLock(String lockId) {
		return this.getLock(lockId, DEFAULT_LOCK_MILLS);
	}

	/**
	 * 释放锁
	 *
	 * @param lockId key
	 */
	public void releaseLock(String lockId) {
		redisTemplate.delete(lockId);
	}
}
