package com.zjson.alibaba.commons.tools.utils;

import org.apache.tomcat.util.http.MimeHeaders;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;

/**
 * @author yaoozu
 * @description  request处理
 * @date 2020/3/421:28
 * @since v1.0.0
 */
public class RequestUtils {
    public static void reflectSetHeaderParam(HttpServletRequest request, String key, String value){
        Class<? extends HttpServletRequest> requestClass = request.getClass();
        try {
            Field request1 = requestClass.getDeclaredField("request");
            request1.setAccessible(true);
            Object o = request1.get(request);

            Field coyoteRequest = o.getClass().getDeclaredField("coyoteRequest");
            coyoteRequest.setAccessible(true);
            Object o1 = coyoteRequest.get(o);

            Field headers = o1.getClass().getDeclaredField("headers");
            headers.setAccessible(true);

            MimeHeaders o2 = (MimeHeaders)headers.get(o1);
            o2.setValue(key).setString(value);
            //o2.addValue(key).setString(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
