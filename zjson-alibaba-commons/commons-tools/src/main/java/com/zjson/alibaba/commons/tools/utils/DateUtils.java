package com.zjson.alibaba.commons.tools.utils;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * 日期处理工具类
 */
public class DateUtils {
	/** 时间格式(yyyy-MM-dd) */
	public final static String DATE_PATTERN = "yyyy-MM-dd";

    public final static String DATE_SLASH_PATTERN = "yyyy/MM/dd";
	/** 时间格式(yyyy-MM-dd HH:mm:ss) */
	public final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public final static String DATE_TIME_PATTERN_STRING = "yyyyMMddHHmmss";
    /**
     * 长期有效时间
     */
    public final static String USING_END_TIME = "2099/01/01";




	public final static java.time.format.DateTimeFormatter df = java.time.format.DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);

    public final static java.time.format.DateTimeFormatter dfs = java.time.format.DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_STRING);

    public final static java.time.format.DateTimeFormatter dsp = java.time.format.DateTimeFormatter.ofPattern(DATE_SLASH_PATTERN);


    /**
     * 日期格式化 日期格式为：yyyy-MM-dd
     * @param date  日期
     * @return  返回yyyy-MM-dd格式日期
     */
	public static String format(Date date) {
        return format(date, DATE_PATTERN);
    }

    /**
     * 日期格式化 日期格式为：yyyy-MM-dd
     * @param date  日期
     * @param pattern  格式，如：DateUtils.DATE_TIME_PATTERN
     * @return  返回yyyy-MM-dd格式日期
     */
    public static String format(Date date, String pattern) {
        if(date != null){
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            return df.format(date);
        }
        return null;
    }

    /**
     * 日期解析
     * @param date  日期
     * @param pattern  格式，如：DateUtils.DATE_TIME_PATTERN
     * @return  返回Date
     */
    public static Date parse(String date, String pattern) {
        try {
            return new SimpleDateFormat(pattern).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 字符串转换成日期
     * @param strDate 日期字符串
     * @param pattern 日期的格式，如：DateUtils.DATE_TIME_PATTERN
     */
    public static Date stringToDate(String strDate, String pattern) {
        if (StringUtils.isBlank(strDate)){
            return null;
        }

        DateTimeFormatter fmt = DateTimeFormat.forPattern(pattern);
        return fmt.parseLocalDateTime(strDate).toDate();
    }

    /**
     * 根据周数，获取开始日期、结束日期
     * @param week  周期  0本周，-1上周，-2上上周，1下周，2下下周
     * @return  返回date[0]开始日期、date[1]结束日期
     */
    public static Date[] getWeekStartAndEnd(int week) {
        DateTime dateTime = new DateTime();
        LocalDate date = new LocalDate(dateTime.plusWeeks(week));

        date = date.dayOfWeek().withMinimumValue();
        Date beginDate = date.toDate();
        Date endDate = date.plusDays(6).toDate();
        return new Date[]{beginDate, endDate};
    }

    /**
     * 对日期的【秒】进行加/减
     *
     * @param date 日期
     * @param seconds 秒数，负数为减
     * @return 加/减几秒后的日期
     */
    public static Date addDateSeconds(Date date, int seconds) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusSeconds(seconds).toDate();
    }

    /**
     * 对日期的【分钟】进行加/减
     *
     * @param date 日期
     * @param minutes 分钟数，负数为减
     * @return 加/减几分钟后的日期
     */
    public static Date addDateMinutes(Date date, int minutes) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusMinutes(minutes).toDate();
    }

    /**
     * 对日期的【小时】进行加/减
     *
     * @param date 日期
     * @param hours 小时数，负数为减
     * @return 加/减几小时后的日期
     */
    public static Date addDateHours(Date date, int hours) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusHours(hours).toDate();
    }

    /**
     * 对日期的【天】进行加/减
     *
     * @param date 日期
     * @param days 天数，负数为减
     * @return 加/减几天后的日期
     */
    public static Date addDateDays(Date date, int days) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusDays(days).toDate();
    }

    /**
     * 对日期的【周】进行加/减
     *
     * @param date 日期
     * @param weeks 周数，负数为减
     * @return 加/减几周后的日期
     */
    public static Date addDateWeeks(Date date, int weeks) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusWeeks(weeks).toDate();
    }

    /**
     * 对日期的【月】进行加/减
     *
     * @param date 日期
     * @param months 月数，负数为减
     * @return 加/减几月后的日期
     */
    public static Date addDateMonths(Date date, int months) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusMonths(months).toDate();
    }

    /**
     * 对日期的【年】进行加/减
     *
     * @param date 日期
     * @param years 年数，负数为减
     * @return 加/减几年后的日期
     */
    public static Date addDateYears(Date date, int years) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusYears(years).toDate();
    }


    /**
     * 将时间戳转为LocalDateTime类型
     * @param timestamp
     * @return {@link LocalDateTime}
     * @author weigang
     * @date 2019/10/18 11:41
     */
    public static LocalDateTime getLocalDateTimeOfTimestamp(Long timestamp){
        Instant instant = Instant.ofEpochMilli(timestamp);
        ZoneId zone = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(instant, zone);
    }

    /**
     * 将时间戳转为LocalDateTime类型
     * @param date
     * @return {@link LocalDateTime}
     * @author cjh
     * @date 2019/10/18 11:41
     */
    public static LocalDateTime getLocalDateTimeByString(String date){
        return LocalDateTime.parse(date,df);
    }

    /**
     * 将时间戳转为LocalDateTime类型
     * @param date
     * @return {@link String}
     * @author cjh
     * @date 2019/10/18 11:41
     */
    public static String getStringByLocalDateTime(LocalDateTime date){
        if (Objects.isNull(date)) {
            return "";
        }
        return df.format(date);
    }

    /**
     * 转为yyyyMMddHHmmss
     * @param date
     * @return
     */
    public static String getStrByLocalDateTime(LocalDateTime date){
        if (Objects.isNull(date)) {
            return "";
        }
        return dfs.format(date);
    }


    /**
     * 将时间转为LocalDateTime类型
     * @param date
     * @return {@link String}
     * @author cjh
     * @date 2019/10/18 11:41
     */
    public static LocalDateTime date2LocalDateTime(Date date){
        // 将时间转为 秒级时间戳
        long second = date.toInstant().atOffset(ZoneOffset.ofHours(8)).toEpochSecond();
        return LocalDateTime.ofEpochSecond(second, 0, ZoneOffset.ofHours(8));
    }


    public static Long Date2Days(Date begin,Date end){
        if(begin==null || end ==null){
            return null;
        }
        long difference =  (end.getTime()-begin.getTime())/86400000;
        return Math.abs(difference);
    }

    /**
     * 当前日期是星期几
     * @param date
     * @return 当前日期是星期几
     */
    public static String getWeekOfDate(Date date) {
        final String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0){
            w = 0;
        }

        return weekDays[w];
    }
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 != null && date2 != null) {
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(date1);
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(date2);
            return isSameDay(cal1, cal2);
        } else {
            throw new IllegalArgumentException("The date must not be null");
        }
    }
    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 != null && cal2 != null) {
            return cal1.get(0) == cal2.get(0) && cal1.get(1) == cal2.get(1) && cal1.get(6) == cal2.get(6);
        } else {
            throw new IllegalArgumentException("The date must not be null");
        }
    }

    public static int getWeekOfIndex(Date date) {
        String[] weekDayArray = {"星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"};
        int i = 0;
        for (int j = 0; j < weekDayArray.length; j++) {
            String weekOfDate = getWeekOfDate(date);
            if (weekDayArray[j].equals(weekOfDate)) {
                i = j;
                break;
            }
        }
        return i;
    }

    /**
     * 获取当天凌晨时间
     * @author cjh
     * @date 2020年2月27日21:02:38
     * @return 当天凌晨时间 {@link Date}
     */
    public static Date getToDayMorning(){
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    /**
     * 获取两个时间点的月份差
     * @param dt1 第一个时间点
     * @param dt2 第二个时间点
     * @return int，即需求的月数差
     */
    public static int monthDiff(LocalDateTime dt1,LocalDateTime dt2){
        //获取第一个时间点的月份
        int month1 = dt1.getMonthValue();
        //获取第一个时间点的年份
        int year1 = dt1.getYear();
        //获取第二个时间点的月份
        int month2 = dt2.getMonthValue();
        //获取第二个时间点的年份
        int year2 = dt2.getYear();
        //返回两个时间点的月数差
        return (year2 - year1) *12 + (month2 - month1);
    }

    /**
     *  将两个时间改为“2021/1/21-2021/5/2”的格式
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @return str
     */
    public static String getTwoDateToStr(java.time.LocalDate startDate, java.time.LocalDate endDate) {
        return startDate.format(dsp) +"-"+ (Objects.isNull(endDate)?USING_END_TIME:endDate.format(dsp));
    }



}
