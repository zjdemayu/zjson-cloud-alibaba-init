package com.zjson.alibaba.commons.tools.tree;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 树节点
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/4/14 14:42
 */
@Data
public class TreeNode<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String parentId;

	private List<T> children = new ArrayList<>();

	public List<T> getChildren() {
		return children;
	}

	public void setChildren(List<T> children) {
		this.children = children;
	}
}
