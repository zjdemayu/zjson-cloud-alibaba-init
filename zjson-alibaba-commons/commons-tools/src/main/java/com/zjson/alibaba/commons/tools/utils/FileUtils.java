/*
 * Copyright (c) 2020. 成都美源网络科技有限公司（https://www.meiy365.com/）
 */

package com.zjson.alibaba.commons.tools.utils;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.Map;

/**
 * 文件操作工具类
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/8/17 13:19
 */
@Slf4j
public class FileUtils {

    /**
     * 获取文件夹下所有文件
     *
     * @param path 文件夹路径
     * @return 文件列表
     */
    public static File[] listDirectoryFiles(String path) {
        File directory = new File(path);
        if (!directory.isDirectory()) {
            throw new RuntimeException("文件夹不存在：" + path);
        }

        return directory.listFiles();
    }

    /**
     * 获取文件夹下所有文件
     *
     * @param path 文件夹路径
     * @return 文件列表
     */
    public static File[] listDirectoryFiles(String path, FilenameFilter filter) {
        File directory = new File(path);
        if (!directory.isDirectory()) {
            throw new RuntimeException("文件夹不存在：" + path);
        }
        return directory.listFiles(filter);
    }

    /**
     * 读取指定文件
     *
     * @param file 文件
     */
    public static void readUser(File file) {
        if (!file.exists()) {
            throw new RuntimeException("文件不存在：" + file.getAbsolutePath());
        }

        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            File targetFile = new File("E:\\data\\mysql\\share\\files\\saas_user_new.sql");
            if (!targetFile.exists()) {
                if (!targetFile.createNewFile()) {
                    throw new RuntimeException("文件创建失败");
                }
            }
            writer = new BufferedWriter(new FileWriter(targetFile));
            reader = new BufferedReader(new FileReader(file));
            String temp;
            boolean flag = true;
            int count = 0;
            while ((temp = reader.readLine()) != null) {
                if (temp.contains("DROP TABLE IF EXISTS `user_goods_snapshot`;")) {
                    flag = false;
                }
                if (!flag && temp.contains("DROP TABLE IF EXISTS") && !temp.contains("DROP TABLE IF EXISTS `user_goods_snapshot`;")) {
                    flag = true;
                }
                if (flag) {
                    writer.write(temp);
                    writer.newLine();
                    writer.flush();
                }
                log.info("rowNum：{}-{}", ++count, flag ? "write" : "skip");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        String orderFileName = "E:\\data\\mysql\\share\\files\\saas_order.sql";
        String userFileName = "E:\\data\\mysql\\share\\files\\saas_user.sql";
        // readUser(new File(userFileName));
        readOrder(new File(orderFileName));
    }

    /**
     * 读取指定文件
     *
     * @param file 文件
     */
    public static void readOrder(File file) {
        if (!file.exists()) {
            throw new RuntimeException("文件不存在：" + file.getAbsolutePath());
        }
        BufferedReader reader = null;
        BufferedWriter writer = null;
        Map<Integer, BufferedWriter> writerMap = Maps.newHashMap();
        try {
            File targetFile = new File("E:\\data\\mysql\\share\\files\\saas_order_new.sql");
            if (!targetFile.exists()) {
                if (!targetFile.createNewFile()) {
                    throw new RuntimeException("文件创建失败");
                }
            }
            writer = new BufferedWriter(new FileWriter(targetFile));
            reader = new BufferedReader(new FileReader(file));
            String temp;
            boolean flag = true;
            boolean lockFlag = false;
            int count = 0;
            int snapshotCount = 0;
            while ((temp = reader.readLine()) != null) {
                if (temp.contains("DROP TABLE IF EXISTS `orders_snapshot`;")) {
                    flag = false;
                }
                if (!flag && temp.contains("DROP TABLE IF EXISTS") && !temp.contains("DROP TABLE IF EXISTS `orders_snapshot`;")) {
                    flag = true;
                }
                if (flag) {
                    writer.write(temp);
                    writer.newLine();
                    writer.flush();
                } else {
                    int tmp = ++snapshotCount / 500;
                    BufferedWriter snapshotWriter = writerMap.get(tmp);
                    if (snapshotWriter == null) {
                        File snapshotTmpFile = new File("E:\\data\\mysql\\share\\files\\saas_order_snapshot_" + tmp +".sql");
                        log.info("create new file:{}", snapshotTmpFile.getName());
                        snapshotWriter = new BufferedWriter(new FileWriter(snapshotTmpFile));
                        writerMap.put(tmp, snapshotWriter);
                    }
                    if (!lockFlag && temp.contains("LOCK TABLES `orders_snapshot` WRITE;")) {
                        lockFlag = true;
                    } else {
                        if (lockFlag && temp.contains("UNLOCK TABLES;")) {
                            lockFlag = false;
                        } else {
                            snapshotWriter.write(temp);
                            snapshotWriter.newLine();
                            snapshotWriter.flush();
                        }
                    }
                }
                log.info("rowNum：{}-{}", ++count, flag ? "write" : "skip");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            writerMap.forEach((key, value) -> {
                try {
                    value.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
