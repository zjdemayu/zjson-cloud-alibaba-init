package com.zjson.alibaba.commons.tools.page;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 *
 * @author admin
 */
@Data
@NoArgsConstructor
@ApiModel(value = "分页数据")
public class PageData<T> implements Serializable {

    private static final long serialVersionUID = -1040046475073184836L;

    @ApiModelProperty(value = "总记录数")
    private int total;

    @ApiModelProperty(value = "列表数据")
    private List<T> list;

    public PageData(List<T> list, long total) {
        this.list = list;
        this.total = (int) total;
    }

    public static <T> PageData<T> of(long total, List<T> list) {
        return new PageData<>(list, total);
    }

    public void setTotal(long total) {
        this.total = (int) total;
    }

}
