package com.zjson.alibaba.commons.tools.log.producer;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.zjson.alibaba.commons.tools.config.ModuleConfig;
import com.zjson.alibaba.commons.tools.log.AbstractOperationLogger;
import com.zjson.alibaba.commons.tools.log.BaseLog;
import com.zjson.alibaba.commons.tools.log.OperationLogger;
import com.zjson.alibaba.commons.tools.redis.RedisKeys;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.concurrent.*;

/**
 * 日志通过redis队列，异步保存到数据库
 *
 * @author yaozou
 */
@Component
public class LogProducer implements OperationLogger {

    ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("log-producer-pool-%d").build();

    ExecutorService pool = new ThreadPoolExecutor(5, 200, 0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private ModuleConfig moduleConfig;

    private final AbstractOperationLogger operationLogger = new AbstractOperationLogger() {

        @Override
        protected void saveLog(BaseLog operationLog) {
            LogProducer.this.saveLog(operationLog);
        }
    };

    @PostConstruct
    public void initLogger() {
        // 初始化模块配置
        operationLogger.setModuleConfig(moduleConfig);
    }

    @Override
    public void saveLog(UserDetail user, ProceedingJoinPoint joinPoint, long time, Integer status) {
        operationLogger.saveLog(user, joinPoint, time, status);
    }

    /**
     * 保存Log到Redis消息队列
     */
    public void saveLog(BaseLog log) {
        String key = RedisKeys.getSysLogKey();
        if (redisTemplate != null) {
            //异步保存到队列
            pool.execute(() -> redisTemplate.opsForList().leftPush(key, log));
        }
    }

}
