package com.zjson.alibaba.commons.tools.log;

import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * 操作日志处理抽象接口
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2020/6/10 16:42
 */
public interface OperationLogger {

    /**
     * 保存操作日志
     *
     * @param user      登录用户信息
     * @param joinPoint 操作信息
     * @param time      操作处理时长
     * @param status    操作结果状态
     */
    void saveLog(UserDetail user, ProceedingJoinPoint joinPoint, long time, Integer status);
}
