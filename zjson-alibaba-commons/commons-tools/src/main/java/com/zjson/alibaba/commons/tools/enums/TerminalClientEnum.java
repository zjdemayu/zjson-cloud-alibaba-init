/*
 * Copyright (c) 2019.
 * hnf Co.Ltd. 2002-
 * All rights resolved
 */
package com.zjson.alibaba.commons.tools.enums;

import com.zjson.alibaba.commons.tools.exception.CustomException;
import lombok.Getter;

/**
 * 用户终端枚举定义
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2019/10/18 9:41
 */
@Getter
public enum TerminalClientEnum {

    /**
     * 用户终端枚举定义
     */
    PLATFORM_CLIENT(1, "平台端"),

    MERCHANT_CLIENT(2, "商户端"),

    MINI_APP_CLIENT(3, "小程序");

    /**
     * 类型code
     */
    private final int code;

    /**
     * 类型描述
     */
    private final String desc;

    TerminalClientEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 解析终端主类型code
     *
     * @param code 主类型code
     * @return 终端主类型
     */
    public static TerminalClientEnum parse(int code) {
        for (TerminalClientEnum client : values()) {
            if (client.code == code) {
                return client;
            }
        }
        throw new CustomException("无法解析的终端主类型code=" + code);
    }

}
