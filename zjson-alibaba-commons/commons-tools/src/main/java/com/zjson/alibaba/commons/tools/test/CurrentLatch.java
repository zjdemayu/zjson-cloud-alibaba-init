package com.zjson.alibaba.commons.tools.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.IntStream;

/**
 * @Author: zj
 * @Date: 2021/10/15 0015 15:34
 * @Description:
 */
public class CurrentLatch {

    /**
     *  上传文件线程
     */
    static class UploadCallable implements Callable {
        private CountDownLatch countDownLatch;
        public UploadCallable(CountDownLatch countDownLatch) {
            this.countDownLatch = countDownLatch;
        }
        @Override
        public Object call() throws Exception {
            TimeUnit.SECONDS.sleep(2);
            countDownLatch.countDown();
            return Thread.currentThread().getName() +":" +System.currentTimeMillis();
        }
    }

    /**
     *  测试使用 CountDownLatch + 线程池异步跑线程
     * @throws InterruptedException ex
     */
    public void test() throws InterruptedException {
        long a = System.currentTimeMillis();
        CountDownLatch countDownLatch = new CountDownLatch(10);
        ThreadPoolExecutor uploadExecutor = new ThreadPoolExecutor(
                5, 10, 10,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(10),
                Executors.defaultThreadFactory());
        List<Future<Object>> futures = new ArrayList<>();
        IntStream.range(1, 11).forEach(x -> futures.add(uploadExecutor.submit(new UploadCallable(countDownLatch))));
        countDownLatch.await();
        uploadExecutor.shutdown();
        futures.forEach(f -> {
            try {
                System.out.println(f.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        System.out.println("耗时：" + (System.currentTimeMillis() - a));
    }

    public static void main(String[] args) throws Exception {
        CurrentLatch cl = new CurrentLatch();
        cl.test();
        // range从左边开始，比右边小1结束
        IntStream.range(0, 2).forEach(System.out::println);
    }
}
