package com.zjson.alibaba.commons.tools.constant;

import java.time.format.DateTimeFormatter;

/**
 * DateTimeFormatter 常量
 *
 * @author zengzhi
 * @version 1.0
 * @since 2021/6/8 15:44
 */
public class DateTimeFormatterConstant {

    /**
     * LocalDateTime格式化字符串
     */
    public static final DateTimeFormatter LOCAL_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static final DateTimeFormatter LOCAL_DATE_TIME_NONSS_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    /**
     * LocalDate格式化字符串
     */
    public static final DateTimeFormatter LOCAL_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /**
     * LocalTime格式化字符串
     */
    public static final DateTimeFormatter LOCAL_TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");

    public static final DateTimeFormatter LOCAL_TIME_NONSS_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

}
