/*
 * Copyright (c) 2019.
 * hnf Co.Ltd. 2002-
 * All rights resolved
 */
package com.zjson.alibaba.commons.tools.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * SQL 工具类
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2019/10/12 17:55
 */
public class SqlUtil {

    private final static String BACKSLASH = "\\";
    private final static String BACKSLASH_REPLACE = "\\\\";

    private final static String PERCENT = "%";
    private final static String PERCENT_REPLACE = "\\%";

    private final static String UNDERLINE = "_";
    private final static String UNDERLINE_REPLACE = "\\_";

    /**
     * MySQL 模糊查询时特殊字符转义
     *
     * @param param 模糊查询参数
     * @return 转义后的查询参数
     */
    public static String convertSpecialLikeParam(String param) {
        // 空检查
        if (StringUtils.isBlank(param)) {
            return param;
        }
        // 反斜杠检查
        if (param.contains(BACKSLASH)) {
            param = param.replace(BACKSLASH, BACKSLASH_REPLACE);
        }
        // 百分号检查
        if (param.contains(PERCENT)) {
            param = param.replace(PERCENT, PERCENT_REPLACE);
        }
        // 下划线检查
        if (param.contains(UNDERLINE)) {
            param = param.replace(UNDERLINE, UNDERLINE_REPLACE);
        }

        return param;
    }

}
