/*
 * Copyright (c) 2019.
 * hnf Co.Ltd. 2002-
 * All rights resolved
 */
package com.zjson.alibaba.commons.tools.annotation;


import java.lang.annotation.*;

/**
 * 校验需要用户登录注解
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2019/10/22 13:40
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoginChecker {

    /**
     * 是否开启登录校验
     *
     * @return 默认开启
     */
    boolean check() default true;
}
