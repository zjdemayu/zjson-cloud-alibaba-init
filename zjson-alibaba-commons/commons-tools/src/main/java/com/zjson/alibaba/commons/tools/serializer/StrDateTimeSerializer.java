package com.zjson.alibaba.commons.tools.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Objects;

/**
 * 字符串类型的时间序列化输出
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/12/10 18:24
 */
public class StrDateTimeSerializer extends StdSerializer<String> {

    /**
     * yyyy-MM-dd HH:mm:ss
     */
    private final static int STANDARD_LENGTH = 19;

    public StrDateTimeSerializer() {
        super(String.class, false);
    }

    public StrDateTimeSerializer(Class<String> t) {
        super(t);
    }

    public StrDateTimeSerializer(JavaType type) {
        super(type);
    }

    public StrDateTimeSerializer(Class<?> t, boolean dummy) {
        super(t, dummy);
    }

    public StrDateTimeSerializer(StdSerializer<?> src) {
        super(src);
    }

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (Objects.nonNull(value) && value.length() > STANDARD_LENGTH) {
            value = value.substring(0, STANDARD_LENGTH);
        }
        gen.writeString(value);
    }

}
