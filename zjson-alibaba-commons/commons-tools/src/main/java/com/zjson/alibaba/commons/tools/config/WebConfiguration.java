package com.zjson.alibaba.commons.tools.config;

import com.zjson.alibaba.commons.tools.utils.SpringContextUtils;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author yaozou
 * @description: web层配置
 * @date 2019-11-30 11:51
 * @since 1.0.0
 */
@SpringBootConfiguration
public class WebConfiguration implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        if (SpringContextUtils.containsBean("realNameHandlerInterceptor")) {
            Object realNameHandlerInterceptor = SpringContextUtils.getBean("realNameHandlerInterceptor");
            registry.addInterceptor((HandlerInterceptor) realNameHandlerInterceptor).addPathPatterns("/**");
        }

        if (SpringContextUtils.containsBean("apiHandlerInterceptor")) {
            Object apiHandlerInterceptor = SpringContextUtils.getBean("apiHandlerInterceptor");
            registry.addInterceptor((HandlerInterceptor) apiHandlerInterceptor).addPathPatterns("/**");
        }

    }

}
