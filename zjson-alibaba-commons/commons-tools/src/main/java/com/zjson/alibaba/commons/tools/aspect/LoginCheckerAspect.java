package com.zjson.alibaba.commons.tools.aspect;

import com.zjson.alibaba.commons.tools.enums.ErrorCodeEnum;
import com.zjson.alibaba.commons.tools.annotation.LoginChecker;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.security.user.SecurityUser;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Objects;

/**
 * 用户登录校验，切面处理类
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2019/10/22 13:40
 */
@Aspect
@Component
public class LoginCheckerAspect {

    @Pointcut("@annotation(com.zjson.alibaba.commons.tools.annotation.LoginChecker)")
    public void loginCheckerPointCut() {

    }

    @Before("loginCheckerPointCut()")
    public void before(JoinPoint point) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        LoginChecker annotation = method.getAnnotation(LoginChecker.class);
        if (annotation.check()) {
            UserDetail loginUser = SecurityUser.getUser();
            if (Objects.isNull(loginUser)) {
                throw new CustomException(ErrorCodeEnum.UNAUTHORIZED.getCode(), ErrorCodeEnum.UNAUTHORIZED.getDesc());
            }
        }
    }
}
