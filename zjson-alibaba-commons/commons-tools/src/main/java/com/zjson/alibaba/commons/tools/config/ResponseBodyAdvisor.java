package com.zjson.alibaba.commons.tools.config;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.net.URI;
import java.util.List;

/**
 * created on 2020/12/8 9:39
 *
 * @author yaozou
 * @since v1.0.0
 */
@Slf4j
@ControllerAdvice
public class ResponseBodyAdvisor implements ResponseBodyAdvice<Object> {


    private static final List<String> IGNORE_LOG_URI = Lists.newArrayList("/auth/resource", "/check/health");

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return AbstractJackson2HttpMessageConverter.class.isAssignableFrom(converterType)
                || returnType.getMethod().isAnnotationPresent(ResponseBody.class);
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType,
                                  MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  ServerHttpRequest request, ServerHttpResponse response) {
        URI uri = request.getURI();
        // 响应值转JSON串输出到日志系统
        if (log.isDebugEnabled()
                && !IGNORE_LOG_URI.contains(uri.getPath())) {
            log.debug("response body {}: {}", uri, JSON.toJSONString(body));
        }
        return body;
    }

}
