package com.zjson.alibaba.commons.tools.exception;

/**
 * 错误编码，由5位数字组成，前2位为模块编码，后3位为业务编码
 * <p>
 * 如：10001（10代表系统模块，001代表业务代码）
 * </p>
 *
 * @author Administrator
 */
public interface ErrorCode {

    /**
     * 系统异常
     */
    int INTERNAL_SERVER_ERROR = 500;

    /**
     * 未授权
     */
    int UNAUTHORIZED = 401;

    /**
     * 禁止访问
     */
    int FORBIDDEN = 403;

    /**
     * 账号已在其他设备登录
     */
    int AUTH_SIGNED_IN_OTHER = 405;

    /**
     * NOT_NULL
     */
    int NOT_NULL = 10001;

    /**
     * 密码错误
     */
    int AUTH_PASSWORD_ERROR = 10002;

    /**
     * 账号已被禁用
     */
    int AUTH_DISABLE = 10003;

    /**
     * 账号不存在
     */
    int AUTH_NOT_EXIST = 10004;

    /**
     * 账号信息发生变更，请重新登录
     */
    int AUTH_INFORMATION_CHANGED = 10006;

    /**
     * 账号权限发生变更，请重新登录
     */
    int AUTH_PERMISSION_CHANGED = 10007;


}
