package com.zjson.alibaba.commons.tools.security.password;

import java.util.UUID;

/**
 * 密码工具类
 */
public class PasswordUtils {
    private static PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    /**
     * 加密
     *
     * @param str 字符串
     * @return 返回加密字符串
     */
    public static String encode(String str) {
        return passwordEncoder.encode(str);
    }


    /**
     * 比较密码是否相等
     *
     * @param str      明文密码
     * @param password 加密后密码
     * @return true：成功    false：失败
     */
    public static boolean matches(String str, String password) {
        return passwordEncoder.matches(str, password);
    }


    public static void main(String[] args) {
        String str = UUID.randomUUID().toString();
        // String password = encode(str);
        String password = "21218cca77804d2ba1922c33e0151105";
        System.out.println(str);
        System.out.println(encode(str));
        System.out.println(matches(str, encode(password)));


    }

}
