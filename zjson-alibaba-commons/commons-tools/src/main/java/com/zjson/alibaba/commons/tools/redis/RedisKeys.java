package com.zjson.alibaba.commons.tools.redis;

import org.apache.commons.lang3.StringUtils;

/**
 * redis的key常量
 *
 * @author admin
 */
public class RedisKeys {

    /**
     * 登录验证码Key
     */
    public static String getLoginCaptchaKey(String uuid) {
        return "sys:captcha:" + uuid;
    }

    /**
     * 系统日志Key
     */
    public static String getSysLogKey() {
        return "sys:log";
    }

    /**
     * 用户菜单导航Key
     */
    public static String getUserMenuNavKey(String userId) {
        return "sys:user:nav:" + userId + "_*";
    }

    /**
     * 用户权限标识Key
     */
    public static String getUserPermissionsKey(String userId) {
        return "sys:user:permissions:" + userId;
    }

    /**
     * 小程序-登录用户Key
     */
    public static String getSecurityUserMiniAppKey(String id) {
        return StringUtils.join("security:mini_app:", id);
    }

    /**
     * 平台端-登录用户Key
     */
    public static String getSecurityUserPlatformKey(String id) {
        return StringUtils.join("security:platform:", id);
    }

    /**
     * 平台端-登录用户Key
     */
    public static String getSecurityUserMerchantKey(String id) {
        return StringUtils.join("security:merchant:", id);
    }

    /**
     * 系统的区域Key
     */
    public static String getAreaKey() {
        return "sys:area";
    }

    /**
     * 系统的区域Key
     */
    public static String getIssuedToCustomerKey(String merchantNum, String evidenceId) {
        return "issued:customer:" + merchantNum + ":" + evidenceId;
    }


    public static String getEvidenceSettlementRecordKey(String evidenceId) {
        return "evidence:settlement:record:" + evidenceId;
    }
}
