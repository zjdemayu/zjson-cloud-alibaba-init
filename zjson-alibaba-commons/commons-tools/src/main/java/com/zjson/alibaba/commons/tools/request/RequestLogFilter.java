/*
 * Copyright (c) 2020. 成都美源网络科技有限公司（https://www.meiy365.com/）
 */

package com.zjson.alibaba.commons.tools.request;

import com.zjson.alibaba.commons.tools.utils.HttpContextUtils;
import com.zjson.alibaba.commons.tools.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * 请求日志过滤器
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/10/13 11:35
 */
@Slf4j
public class RequestLogFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        // log请求日志
        logRequestInfo(request);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    /**
     * LOG 请求信息
     *
     * @param request 请求信息
     */
    private void logRequestInfo(HttpServletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("request-ip:{}", HttpContextUtils.getRealIp(request));
            log.debug("request-uri:{} {}", request.getMethod(), request.getRequestURI());
            Map<String, String> headers = HttpContextUtils.getHeaders(request);
            if (!CollectionUtils.isEmpty(headers)) {
                log.debug("request-headers:{}", JsonUtils.objectToJson(headers));
            }
            if (!CollectionUtils.isEmpty(request.getParameterMap())) {
                log.debug("request-params:{}", JsonUtils.objectToJson(request.getParameterMap()));
            }
        }
    }
}
