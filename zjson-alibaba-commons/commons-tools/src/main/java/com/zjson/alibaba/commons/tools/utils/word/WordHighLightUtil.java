package com.zjson.alibaba.commons.tools.utils.word;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * created on 2020/11/2 10:49
 *
 * @author yaozou
 * @since v1.0.0
 */
public class WordHighLightUtil {

    private static String[] colors = {"#4791FF", "#FF7C4D", "#FFAD42", "#8080FF", "#30C77B", "#26ADF0", "#FF7070"};

    /**
     * 高亮文字
     * @param text 文字
     * @param keywords 高亮的关键字
     * @return 标识高亮的文字
     */
    public static String highLight(String text, List<String> keywords) {
        if(StringUtils.isBlank(text)){
            return text;
        }
        for (int i = 0;i<keywords.size();i++) {
            String s = Arrays.asList(colors).stream().findAny().get();
            if(text.contains(keywords.get(i))){
                String value = String.format("<span style='color: %s; font-size:16px; border:1px solid %s'>%s</span>", s, s, keywords.get(i));
                text = text.replace(keywords.get(i), value);
            }
        }
        return text;
    }

    /**
     * 获取顾客标签
     * @param text 文字
     * @param tags 高亮的关键字
     * @return 标识高亮的文字
     */
    public static List<String> getTags(String text, List<String> tags) {
        List<String> result = new ArrayList<>();
        for (int i = 0;i<tags.size();i++) {
            if(text.contains(tags.get(i))){
                result.add(tags.get(i));
            }
        }
        return result;
    }

    public static void main(String[] args){
        System.out.println(highLight("Dfghjfghjfg",
                new ArrayList<String>(){{
                    add("a");
                    add("b");
                    add("c");
                    add("d");
                    add("e");
                    add("f");
                    add("g");
                    add("h");
                    add("i");
                    add("j");
                    add("k");
                }}));
    }
}
