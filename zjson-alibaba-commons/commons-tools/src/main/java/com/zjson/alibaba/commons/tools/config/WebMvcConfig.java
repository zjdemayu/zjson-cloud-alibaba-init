package com.zjson.alibaba.commons.tools.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.zjson.alibaba.commons.tools.convert.EnumConverterFactory;
import com.zjson.alibaba.commons.tools.utils.DateUtils;
import com.zjson.alibaba.commons.tools.convert.DateConverter;
import com.zjson.alibaba.commons.tools.security.resolver.UserDetailHandlerMethodArgumentResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.zjson.alibaba.commons.tools.constant.DateTimeFormatterConstant.*;

/**
 * MVC配置
 *
 * @author admin
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {


    @Autowired
    private UserDetailHandlerMethodArgumentResolver userDetailHandlerMethodArgumentResolver;

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(String.class, LocalDateTime.class, source -> source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}$") ?
                LocalDateTime.parse(source, LOCAL_DATE_TIME_NONSS_FORMATTER) : LocalDateTime.parse(source, LOCAL_DATE_TIME_FORMATTER));
        registry.addConverter(String.class, LocalDate.class, source -> LocalDate.parse(source, LOCAL_DATE_FORMATTER));
        registry.addConverter(String.class, LocalTime.class, source -> source.matches("^\\d{1,2}:\\d{1,2}$") ?
                LocalTime.parse(source, LOCAL_TIME_NONSS_FORMATTER) : LocalTime.parse(source, LOCAL_TIME_FORMATTER));
        registry.addConverter(String.class, Date.class, new DateConverter());
        registry.addConverterFactory(new EnumConverterFactory());
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(userDetailHandlerMethodArgumentResolver);
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new ByteArrayHttpMessageConverter());
        converters.add(new ResourceHttpMessageConverter());
        converters.add(new AllEncompassingFormHttpMessageConverter());
        converters.add(new StringHttpMessageConverter());
        converters.add(jackson2HttpMessageConverter());
    }

    @Bean
    public MappingJackson2HttpMessageConverter jackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper objectMapper = new ObjectMapper();
        //日期格式转换
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setDateFormat(new SimpleDateFormat(DateUtils.DATE_TIME_PATTERN));
        objectMapper.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        // 配置日期格式序列化、反序列化
        objectMapper.registerModule(new JavaTimeModule());

        // Long类型转String类型，以解决js中的精度丢失。
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        objectMapper.registerModule(simpleModule);

        converter.setObjectMapper(objectMapper);
        return converter;
    }

}
