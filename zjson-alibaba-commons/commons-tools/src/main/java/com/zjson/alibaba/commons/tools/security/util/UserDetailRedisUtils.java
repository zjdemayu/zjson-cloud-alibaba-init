package com.zjson.alibaba.commons.tools.security.util;

import cn.hutool.core.bean.BeanUtil;
import com.zjson.alibaba.commons.tools.enums.TerminalClientEnum;
import com.zjson.alibaba.commons.tools.redis.RedisKeys;
import com.zjson.alibaba.commons.tools.security.enums.SecurityStatusEnum;
import com.zjson.alibaba.commons.tools.security.enums.UserKillEnum;
import com.zjson.alibaba.commons.tools.utils.JsonUtils;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.security.user.SecurityUser;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * 用户Redis登录信息工具类
 *
 * @author admin
 */
@Component
@Slf4j
public class UserDetailRedisUtils {

    private final HashOperations<String, String, Object> hashOperations;

    private final RedisTemplate<String, Object> redisTemplate;

    private static final String SECURITY_STATUS_CODE_ENUM_REDIS_HASH_KEY = "securityStatusEnum";

    private static final EnumMap<TerminalClientEnum, Function<String, String>> TERMINAL_CLIENT_ENUM_FUNCTION_MAP = new EnumMap<>(TerminalClientEnum.class);

    static {
        TERMINAL_CLIENT_ENUM_FUNCTION_MAP.put(TerminalClientEnum.PLATFORM_CLIENT, RedisKeys::getSecurityUserPlatformKey);
        TERMINAL_CLIENT_ENUM_FUNCTION_MAP.put(TerminalClientEnum.MERCHANT_CLIENT, RedisKeys::getSecurityUserMerchantKey);
        TERMINAL_CLIENT_ENUM_FUNCTION_MAP.put(TerminalClientEnum.MINI_APP_CLIENT, RedisKeys::getSecurityUserMiniAppKey);
    }

    public UserDetailRedisUtils(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.hashOperations = redisTemplate.opsForHash();
    }


    /**
     * 功能描述: 获取登录的redis的key
     */
    private String getLoginRedisKey(String userId) {
        // 根据不同端获取对应redis的key
        return getLoginRedisKey(userId, SecurityUser.getTerminalClient());
    }

    /**
     * 功能描述: 获取指定端登录的redis的key
     */
    private String getLoginRedisKey(String userId, TerminalClientEnum terminalClient) {
        // 根据不同端获取对应redis的key
        return Optional.ofNullable(TERMINAL_CLIENT_ENUM_FUNCTION_MAP.get(terminalClient).apply(userId))
                .orElseThrow(() -> new CustomException("未获取到终端" + terminalClient.getDesc() + "的登录缓存key"));
    }

    /**
     * 放入用户信息到redis
     *
     * @param user   用户信息
     * @param expire 过期时间
     */
    public void set(UserDetail user, long expire) {
        if (Objects.isNull(user)) {
            return;
        }
        String key = getLoginRedisKey(user.getId());
        redisTemplate.delete(key);
        //bean to map
        user.setKill(UserKillEnum.NO.getValue());
        Map<String, Object> map = BeanUtil.beanToMap(user, false, true);
        hashOperations.putAll(key, map);
        redisTemplate.expire(key, expire, TimeUnit.SECONDS);

        //用户登录时，清空菜单导航、权限标识
        redisTemplate.delete(RedisKeys.getUserMenuNavKey(user.getId()));
        redisTemplate.delete(RedisKeys.getUserPermissionsKey(user.getId()));
    }

    /**
     * 更改登录信息状态
     *
     * @param userId             userId
     * @param securityStatusEnum securityStatusEnum
     */
    public void updateSecurityStatus(String userId, SecurityStatusEnum securityStatusEnum) {
        String key = getLoginRedisKey(userId);
        if (Boolean.TRUE.equals(redisTemplate.hasKey(key))) {
            hashOperations.put(key, SECURITY_STATUS_CODE_ENUM_REDIS_HASH_KEY, securityStatusEnum);
        }
    }

    /**
     * 修改指定端的登录状态
     *
     * @param userId             userId
     * @param securityStatusEnum securityStatusEnum
     * @param terminalClientEnum terminalClientEnum
     */
    public void updateSecurityStatus(String userId, SecurityStatusEnum securityStatusEnum, TerminalClientEnum terminalClientEnum) {
        String key = getLoginRedisKey(userId, terminalClientEnum);
        if (Boolean.TRUE.equals(redisTemplate.hasKey(key))) {
            hashOperations.put(key, SECURITY_STATUS_CODE_ENUM_REDIS_HASH_KEY, securityStatusEnum);
        }
    }

    /**
     * 获取用户信息
     *
     * @param userId userId
     * @return UserDetail
     */
    public UserDetail get(String userId) {
        Map<String, Object> map = hashOperations.entries(getLoginRedisKey(userId));
        if (MapUtils.isEmpty(map)) {
            return null;
        }
        // 枚举反序列化需要用到jackson操作
        return JsonUtils.objectToObject(map, UserDetail.class);
    }

    /**
     * 用户退出
     *
     * @param userId 用户ID
     */
    public void logout(String userId) {
        String key = getLoginRedisKey(userId);
        hashOperations.put(key, "kill", UserKillEnum.YES.getValue());
        redisTemplate.delete(key);
    }

}
