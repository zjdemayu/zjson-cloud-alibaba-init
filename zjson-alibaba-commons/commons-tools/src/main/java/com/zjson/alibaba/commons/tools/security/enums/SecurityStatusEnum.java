package com.zjson.alibaba.commons.tools.security.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;


/**
 * 登录信息状态
 *
 * @author zengzhi
 */
@Getter
@AllArgsConstructor
public enum SecurityStatusEnum {
    /**
     * 登录信息状态
     */
    AUTH_INFORMATION_CHANGED(ErrorCode.AUTH_INFORMATION_CHANGED, "账号信息发生变更，请重新登录！"),
    AUTH_PERMISSION_CHANGED(ErrorCode.AUTH_PERMISSION_CHANGED, "账号权限发生变更，请重新登录！"),
    AUTH_ROLE_CHANGED(ErrorCode.UNAUTHORIZED, "角色信息改变，请重新登录！");

    private final Integer code;

    @JsonValue
    private final String desc;

    @JsonCreator
    public static SecurityStatusEnum parse(String str) {
        if (Objects.isNull(str)) {
            return null;
        }
        Integer integer = StringUtils.isNumeric(str) ? Integer.valueOf(str) : null;
        for (SecurityStatusEnum statusEnum : values()) {
            if (statusEnum.getCode().equals(integer)
                    || statusEnum.getDesc().equals(str)
                    || statusEnum.name().equals(str)) {
                return statusEnum;
            }
        }
        throw new CustomException("非法的类型：" + str);
    }
}
