package com.zjson.alibaba.commons.tools.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;


/**
 * 白名单配置
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/5/13 10:25
 */
@Getter
@Setter
@Configuration
@RefreshScope
@ConfigurationProperties(prefix = "rights")
public class RightsSmsProperties {

    /**
     * 验证码配置
     */
    private SmsVerifyCode smsVerifyCode;

    /**
     * 下发凭证短信模板code
     */
    private String rightsIssuedCode;


}
