package com.zjson.alibaba.commons.tools.tree;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 树结构工具类
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/4/14 14:42
 */
public class TreeUtil {

	/**
	 * 指定父id构建树结构
	 * @param treeNodes 未成树结构的list集合
	 * @param pid 初始的父id
	 * @param <T> 实体对象
	 * @return
	 */
	 public static  <T extends  TreeNode> List<T> build(List<T> treeNodes, String pid) {
		 List<T> result = new ArrayList<>();
		 treeNodes.forEach(treeNode -> {
		 	if (pid.equals(treeNode.getParentId())) {
				result.add(findChildTreeNode(treeNodes, treeNode));
			}
		 });
		 return result;
	 }

	/**
	 *  根据root节点
	 * @param treeNodes 未成树结构的list集合
	 * @param rootNode root节点
	 * @param <T>  实体对象
	 * @return
	 */
	private static <T extends TreeNode> T findChildTreeNode(List<T> treeNodes, T rootNode) {
		treeNodes.forEach(treeNode -> {
			if (rootNode.getId().equals(treeNode.getParentId())) {
				rootNode.getChildren().add(findChildTreeNode(treeNodes, treeNode));
			}
		});
	 	return rootNode;
	}

	/**
	 * 不指定父id构建树结构 ==> 耗时少
	 * @param treeNodes
	 * @param <T>
	 * @return
	 */
	public static <T extends TreeNode> List<T> build1(List<T> treeNodes) {
		List<T> result = new ArrayList<>();
		//list转map
		Map<String, T> nodeMap = new LinkedHashMap<>(treeNodes.size());
		for(T treeNode : treeNodes){
			nodeMap.put(treeNode.getId(), treeNode);
		}

		for(T node : nodeMap.values()) {
			T parent = nodeMap.get(node.getParentId());
			if(parent != null && !(node.getId().equals(parent.getId()))){
				parent.getChildren().add(node);
				continue;
			}
			result.add(node);
		}

		return result;
	}

	/**
	 *  不指定父id构建树结构 ==> 耗时多
	 * @param treeNodes
	 * @param <T>
	 * @return
	 */
	public static  <T extends  TreeNode> List<T> build2(List<T> treeNodes) {
		List<T> result = new ArrayList<>();
		// 按父Id分组
		Map<String, List<T>> mapParent = treeNodes.stream().collect(Collectors.groupingBy(T::getParentId));
		// 筛选id在mapParent中的实体<必定是父节点>，转换为map
		Map<String, T> mapTree = treeNodes.stream()
				.filter(map-> mapParent.containsKey(map.getId()))
				.collect(Collectors.toMap(T::getId, Function.identity()));
		Map<String, T> nodeMap = new LinkedHashMap<>(treeNodes.size());
		for(T treeNode : treeNodes){
			nodeMap.put(treeNode.getId(), treeNode);
		}
		mapParent.forEach((k,v) -> {
			// 获得父实体，如果为空，作为第一个节点；不为空则将值作为该节点的子树
			T t = mapTree.get(v.get(0).getParentId());
			if (null != t) {
				t.getChildren().addAll(v);
			}else {
				result.addAll(v);
			}
		});
		return result;
	}


	/**
	 *  分类转换为树 ==>耗时多
	 * @param treeNodes  分类 list
	 * @return 分类树
	 */
	public static <T extends  TreeNode>  List<T> build3(List<T> treeNodes) {
		return treeNodes.stream()
				//找到顶级父类
				.filter(treeNode ->  Objects.equals(treeNode.getParentId(), "0"))
				//父类添加子类
				.peek(treeNode -> treeNode.setChildren(getChildTree(treeNode, treeNodes)))
				.collect(Collectors.toList());
	}

	/**
	 * 子分类分类转换为树
	 * @param rootNode 父节点
	 * @param treeNodes 分类 list
	 * @return 分类树
	 */
	private  static <T extends  TreeNode>  List<T> getChildTree(T rootNode, List<T> treeNodes) {
		return treeNodes.stream()
				.filter(treeNode -> Objects.equals(treeNode.getParentId(), rootNode.getId()))
				.peek(treeNode ->treeNode.setChildren(getChildTree(treeNode, treeNodes)))
				.collect(Collectors.toList());
	}




}
