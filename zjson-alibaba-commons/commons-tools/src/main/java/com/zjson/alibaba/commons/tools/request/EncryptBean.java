package com.zjson.alibaba.commons.tools.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zjson
 * @Description 加密后的bean字段
 * @date 2021-01-09 14:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EncryptBean {
    private String aesData;
    private String rsaData;

}
