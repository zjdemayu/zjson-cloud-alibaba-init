package com.zjson.alibaba.commons.tools.convert;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.ModifierUtil;
import cn.hutool.core.util.ReflectUtil;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * springmvc接收枚举参数时的转换器
 * 使用反射调用枚举里的静态方法
 * 参考hutool工具包里的枚举转换
 *
 * @author zz
 */
@SuppressWarnings({"rawtypes", "unchecked", "NullableProblems"})
public class EnumConverterFactory implements ConverterFactory<String, Enum> {

    private static final Map<Class, Converter> CONVERTERS = Maps.newHashMap();

    @Override
    public <T extends Enum> Converter<String, T> getConverter(Class<T> targetType) {
        Converter<String, T> converter = CONVERTERS.get(targetType);
        if (converter == null) {
            converter = new EnumConverter(targetType);
            CONVERTERS.put(targetType, converter);
        }
        return converter;
    }

    static class EnumConverter<T extends Enum> implements Converter<String, T> {

        private final Map<Integer, T> ENUM_VALUE_KEY_MAP = Maps.newHashMap();

        private final Class<T> enumClass;

        public EnumConverter(Class<T> enumClass) {
            this.enumClass = enumClass;
        }

        @Override
        public T convert(String source) {
            if (!StringUtils.isNumeric(source)) {
                // 抛出该异常后，会调用 spring 的默认转换方案，即使用 枚举字面量进行映射
                throw new IllegalArgumentException("无法匹配对应的枚举类型");
            }
            Integer value = Integer.valueOf(source);
            return Optional.ofNullable(ENUM_VALUE_KEY_MAP.get(value))
                    .orElseGet(() -> {
                        T t = (T) tryConvertEnum(value, enumClass);
                        ENUM_VALUE_KEY_MAP.put(value, t);
                        return t;
                    });
        }

        /**
         * 尝试找到类似转换的静态方法调用实现转换且优先使用<br>
         *
         * @param value     被转换的值
         * @param enumClass enum类
         * @return 对应的枚举值
         */
        private Enum tryConvertEnum(Object value, Class enumClass) {
            if (value == null) {
                return null;
            }
            // 用户自定义方法
            // 查找枚举中所有返回值为目标枚举对象的方法，如果发现方法参数匹配，就执行之
            final Map<Class<?>, Method> methodMap = getMethodMap(enumClass);
            if (MapUtil.isNotEmpty(methodMap)) {
                final Class<?> valueClass = value.getClass();
                for (Map.Entry<Class<?>, Method> entry : methodMap.entrySet()) {
                    if (ClassUtil.isAssignable(entry.getKey(), valueClass)) {
                        return ReflectUtil.invokeStatic(entry.getValue(), value);
                    }
                }
            }
            // 抛出该异常后，会调用 spring 的默认转换方案，即使用 枚举字面量进行映射
            throw new IllegalArgumentException("无法匹配对应的枚举类型");
        }

        /**
         * 获取用于转换为enum的所有static方法
         *
         * @param enumClass 枚举类
         * @return 转换方法map，key为方法参数类型，value为方法
         */
        private Map<Class<?>, Method> getMethodMap(Class<?> enumClass) {
            return Arrays.stream(enumClass.getMethods())
                    .filter(ModifierUtil::isStatic)
                    .filter(m -> m.getReturnType() == enumClass)
                    .filter(m -> m.getParameterCount() == 1)
                    .filter(m -> !"valueOf".equals(m.getName()))
                    .collect(Collectors.toMap(m -> m.getParameterTypes()[0], m -> m));
        }

    }

}