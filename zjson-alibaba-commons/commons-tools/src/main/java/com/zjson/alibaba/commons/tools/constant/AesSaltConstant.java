package com.zjson.alibaba.commons.tools.constant;

/**
 * created on 2020/11/4 18:09
 *
 * @author yaozou
 * @since v1.0.0
 */
public interface AesSaltConstant {

    String AMOUNT_SALT = "amount&&salt";

}
