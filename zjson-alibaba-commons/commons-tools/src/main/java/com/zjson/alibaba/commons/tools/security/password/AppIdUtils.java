package com.zjson.alibaba.commons.tools.security.password;

import org.apache.commons.codec.binary.Base64;

import java.util.UUID;

/**
 * 密码工具类
 * @author Administrator
 */
public class AppIdUtils {

    /**
     *  获得appId
     * @param key key
     * @return appId
     */
    public static String getAppId(String key) {
        return key + UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 获得appKey
     * @param key key
     * @return appKey
     */
    public static String getAppKey(String key) {
        return Base64.encodeBase64String((UUID.randomUUID()+key).getBytes());
    }

    public static void main(String[] args) {
        String str = UUID.randomUUID().toString();
        // String password = encode(str);
        String password = "21218cca77804d2ba1922c33e0151105";
        System.out.println(getAppKey(password));

    }
}
