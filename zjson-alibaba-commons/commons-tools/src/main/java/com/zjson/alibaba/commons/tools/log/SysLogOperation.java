package com.zjson.alibaba.commons.tools.log;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 操作日志
 *
 * @author coolmyyihong
 * @version v1.0
 * @since 2019/10/4 00:00
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysLogOperation extends BaseLog {

    private static final long serialVersionUID = 1L;

    /**
     * 模块名称，如：sys
     */
    private String module;

    /**
     * 用户操作
     */
    private String operation;

    /**
     * 请求URI
     */
    private String requestUri;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 请求Headers
     */
    private String requestHeaders;

    /**
     * 请求参数
     */
    private String requestParams;

    /**
     * 请求时长(毫秒)
     */
    private Integer requestTime;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 操作IP
     */
    private String ip;

    /**
     * 状态  0：失败   1：成功
     */
    private Integer status;

    /**
     * 操作终端类型
     */
    private Integer terminalClient;

    /**
     * 操作终端子类型
     */
    private Integer terminalChildClient;

    /**
     * 用户名
     */
    private String creatorName;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

}
