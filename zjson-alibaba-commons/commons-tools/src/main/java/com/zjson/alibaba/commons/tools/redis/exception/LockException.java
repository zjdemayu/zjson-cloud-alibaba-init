package com.zjson.alibaba.commons.tools.redis.exception;


import com.zjson.alibaba.commons.tools.exception.CustomException;

/**
 * 锁异常
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2019/12/4 19:43
 */
public class LockException extends CustomException {

    public LockException(int code) {
        super(code);
    }

    public LockException(int code, String... params) {
        super(code, params);
    }

    public LockException(int code, Throwable e) {
        super(code, e);
    }

    public LockException(int code, Throwable e, String... params) {
        super(code, e, params);
    }

    public LockException(String msg) {
        super(msg);
    }

    public LockException(String msg, Throwable e) {
        super(msg, e);
    }
}
