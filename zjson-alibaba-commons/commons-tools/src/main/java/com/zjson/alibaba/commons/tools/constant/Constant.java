package com.zjson.alibaba.commons.tools.constant;

/**
 * 常量
 *
 * @author admin
 */
public interface Constant {

    /**
     * 用户标识
     */
    String USER_KEY = "userId";

    /**
     * token header
     */
    String TOKEN_HEADER = "token";

    /**
     * 终端类型
     */
    String TERMINAL_CLIENT = "terminal_client";

    /**
     * 商户标识
     */
    String MERCHANT_KEY = "merchant";

    /**
     * 本地IP6
     */
    String LOCALHOST_KEY1 = "0:0:0:0:0:0:0:1";
    /**
     * 本地IP4
     */
    String LOCALHOST_KEY2 = "127.0.0.1";

    /** 所有权限标识 */
    String ALL_PERMISSION = "*:*:*";

    /** 超级管理员角色权限标识 */
    String SUPER_ADMIN = "SUPER_ADMIN";

    /** 超级管理员ID */
    String SUPER_ADMIN_ID = "1";

}
