package com.zjson.alibaba.commons.tools.security.password;

import cn.hutool.core.lang.UUID;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * md5工具
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/5/28 14:19
 */
public class Md5Utils {

	public static String encode(String text){
		try {
			MessageDigest digest = MessageDigest.getInstance("md5");
			byte[] buffer = digest.digest(text.getBytes());
			StringBuffer sb = new StringBuffer();
			for (byte b : buffer) {
				int a = b & 0xff;
				String hex = Integer.toHexString(a);

				if (hex.length() == 1) {
					hex = 0 + hex;
				}
				sb.append(hex);
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		String ps = UUID.randomUUID().toString();
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String md5Str = Md5Utils.encode(ps);
		System.out.println(md5Str);
		System.out.println(passwordEncoder.encode(md5Str));
	}
}
