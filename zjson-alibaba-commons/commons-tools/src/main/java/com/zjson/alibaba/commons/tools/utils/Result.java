package com.zjson.alibaba.commons.tools.utils;

import com.zjson.alibaba.commons.tools.exception.ErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 响应数据
 *
 * @author coolmyyihong
 * @version v1.0.0
 * @date 2019/10/19 16:54
 */
@Data
@ApiModel(value = "响应")
@SuppressWarnings("all")
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 编码：0表示成功，其他值表示失败
     */
    @ApiModelProperty(value = "编码：0表示成功，其他值表示失败")
    private int code = 0;
    /**
     * 消息内容
     */
    @ApiModelProperty(value = "消息内容")
    private String msg = "success";
    /**
     * 响应数据
     */
    @ApiModelProperty(value = "响应数据")
    private T data;

    /**
     * 成功返回
     *
     * @param <T> 返回类型
     * @return Result
     */
    public static <T> Result<T> succ() {
        return new Result<T>().ok(null);
    }

    /**
     * 成功返回
     *
     * @param <T>  返回类型
     * @param data 返回结果
     * @return Result
     */
    public static <T> Result<T> succ(T data) {
        return new Result<T>().ok(data);
    }

    /**
     * 失败返回
     *
     * @param <T> 返回类型
     * @return Result
     */
    public static <T> Result<T> fail() {
        return new Result<T>().error();
    }

    /**
     * 失败返回
     *
     * @param <T>  返回类型
     * @param code 失败错误码
     * @return Result
     */
    public static <T> Result<T> fail(int code) {
        return new Result<T>().error(code);
    }

    /**
     * 失败返回
     *
     * @param <T> 返回类型
     * @param msg 失败错误提示
     * @return Result
     */
    public static <T> Result<T> fail(String msg) {
        return new Result<T>().error(msg);
    }

    /**
     * 失败返回
     *
     * @param <T>  返回类型
     * @param code 失败错误码
     * @param msg  失败错误提示
     * @return Result
     */
    public static <T> Result<T> fail(int code, String msg) {
        return new Result<T>().error(code, msg);
    }

    /**
     * 更新返回
     * @param num
     * @param <T>
     * @return
     */
    public static <T> Result<T> updateRe(int num) {
        if (num > 0) {
            return succ();
        }else {
            return fail();
        }
    }

    public Result<T> ok(T data) {
        this.setData(data);
        return this;
    }

    public boolean success() {
        return code == 0;
    }

    public boolean failure() {
        return code != 0;
    }

    public Result<T> error() {
        this.code = ErrorCode.INTERNAL_SERVER_ERROR;
        this.msg = MessageUtils.getMessage(this.code);
        return this;
    }

    public Result<T> error(int code) {
        this.code = code;
        this.msg = MessageUtils.getMessage(this.code);
        return this;
    }

    public Result<T> error(int code, String msg) {
        this.code = code;
        this.msg = msg;
        return this;
    }

    public Result<T> error(String msg) {
        this.code = ErrorCode.INTERNAL_SERVER_ERROR;
        this.msg = msg;
        return this;
    }
}
