package com.zjson.alibaba.commons.tools.utils.encrypt;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES 对称加密，用于前后端“核心信息”传递
 * created on 2020/11/2 11:40
 * @author yaozou
 * @since v1.0.0
 */
public class AesUtils {

    /**
     * 加密
     * @param data 明文
     * @param key 加密key
     * @return 加密结果
     */
    public static String encrypt(String data,String key){
        try {
            String iv = key;
            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            int blockSize = cipher.getBlockSize();

            byte[] dataBytes = data.getBytes();
            int plaintextLength = dataBytes.length;
            if (plaintextLength % blockSize != 0) {
                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
            }

            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            byte[] encrypted = cipher.doFinal(plaintext);

            return new Base64().encodeToString(encrypted);
        }catch (Exception e){
            e.printStackTrace();
        }
        return data;
    }

    /**
     * 解密
     * @param data 加密文本
     * @param key 加密key
     * @return 解密结果
     */
    public static String decrypt(String data,String key){
        try {
            String iv = key;
            byte[] encrypted1 = new Base64().decode(data);

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

            byte[] original = cipher.doFinal(encrypted1);
            String originalString = new String(original);
            return originalString.trim();
        }catch (Exception e){
            e.printStackTrace();
        }
        return data;
    }

    public static void main(String[] args) {
        try {
            String salt = "qwerrtt!@$#%1234";
            String encode = AesUtils.encrypt("110101200808087633",salt);
            System.out.println(encode);
            System.out.println(AesUtils.decrypt(encode,salt));
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
