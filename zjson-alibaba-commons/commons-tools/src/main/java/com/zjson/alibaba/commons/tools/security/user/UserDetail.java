package com.zjson.alibaba.commons.tools.security.user;

import com.zjson.alibaba.commons.tools.security.bo.ResourceBO;
import com.zjson.alibaba.commons.tools.security.enums.SecurityStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * 登录用户信息
 *
 * @author admin
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "登录账号信息")
public class UserDetail implements Serializable {

    private static final long serialVersionUID = 951927399121640721L;

    /**
     * 账号表主键id
     */
    @ApiModelProperty(value = "用户id")
    private String id;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 真实名字
     */
    @ApiModelProperty(value = "真实名字")
    private String realName;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    private String headPortrait;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String phone;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String mail;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id")
    private String merchantId;

    /**
     * 商户编号
     */
    @ApiModelProperty(value = "商户编号")
    private String merchantNum;

    /**
     * 门店id
     */
    @ApiModelProperty(value = "门店id")
    private String storeId;

    /**
     * 门店名称
     */
    @ApiModelProperty(value = "门店名称")
    private String storeName;

    /**
     * 是否被管理员踢出   0：正常   1：被踢出，无权调用接口
     */
    @ApiModelProperty(value = "是否被管理员踢出   0：正常   1：被踢出，无权调用接口")
    private int kill;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id")
    private String roleId;

    /**
     * 角色名字
     */
    @ApiModelProperty(value = "角色名字")
    private String roleName;

    /**
     * 小程序端，是否有发放权限
     */
    @ApiModelProperty(value = "小程序端，是否有发放权限")
    private Boolean isHasIssuedPermission;

    /**
     * 用户资源列表
     */
    @ApiModelProperty(value = "用户资源列表")
    private List<ResourceBO> resourceList;

    /**
     * 权限列表
     */
    @ApiModelProperty(value = "权限列表")
    private Set<String> permissions;

    /**
     * 角色列表
     */
    @ApiModelProperty(value = "角色列表")
    private Set<String> roles;

    /**
     * token
     */
    @ApiModelProperty(value = "token")
    private String token;

    /**
     * 登录信息状态码
     */
    @ApiModelProperty(value = "登录信息状态码")
    private SecurityStatusEnum securityStatusEnum;

    @ApiModelProperty(value = "商户后台:0,三方对接1")
    private Integer clientType;

}
