package com.zjson.alibaba.commons.tools.config;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import java.lang.reflect.Type;

/**
 * created on 2020/12/8 9:31
 *
 * @author yaozou
 * @since v1.0.0
 */
@Slf4j
@ControllerAdvice
public class RequestBodyAdvisor extends RequestBodyAdviceAdapter {

    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return methodParameter.getParameterAnnotation(RequestBody.class) != null;
    }

    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        // 参数对象转JSON字符串
        String jsonBody;
        if (StringHttpMessageConverter.class.isAssignableFrom(converterType)) {
            jsonBody = body.toString();
        } else {
            jsonBody = JSON.toJSONString(body);
        }

        // 自定义日志输出
        if (log.isDebugEnabled()) {
            log.debug("request-body:{}", jsonBody);
        }
        return super.afterBodyRead(body, inputMessage, parameter, targetType, converterType);
    }
}
