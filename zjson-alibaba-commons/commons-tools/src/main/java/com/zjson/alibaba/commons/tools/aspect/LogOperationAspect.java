package com.zjson.alibaba.commons.tools.aspect;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.zjson.alibaba.commons.tools.annotation.LogOperation;
import com.zjson.alibaba.commons.tools.log.AbstractOperationLogger;
import com.zjson.alibaba.commons.tools.log.enums.LogTypeEnum;
import com.zjson.alibaba.commons.tools.log.enums.OperationStatusEnum;
import com.zjson.alibaba.commons.tools.log.producer.LogProducer;
import com.zjson.alibaba.commons.tools.security.user.SecurityUser;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * 操作日志，切面处理类
 */
@Slf4j
@Aspect
@Component
public class LogOperationAspect {

    @Autowired(required = false)
    private AbstractOperationLogger operationLogger;

    @Resource
    private LogProducer logProducer;

    @Pointcut("@annotation(com.zjson.alibaba.commons.tools.annotation.LogOperation)")
    public void logPointCut() {

    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        LogOperation annotation = method.getAnnotation(LogOperation.class);

        UserDetail user = SecurityUser.getUserWithoutUnauthorizedException();
        long beginTime = System.currentTimeMillis();
        try {
            //执行方法
            Object result = point.proceed();
            //执行时长(毫秒)
            long time = System.currentTimeMillis() - beginTime;
            if (LogTypeEnum.LOGIN.equals(annotation.type()) && "0".equals(annotation.value())) {
                JSON json = JSONUtil.parse(result);
                user = new UserDetail();
                user.setId(json.getByPath("data.id", String.class));
                user.setUsername(json.getByPath("data.username", String.class));
            }
            //保存日志
            saveLog(user, point, time, OperationStatusEnum.SUCCESS.value());

            return result;
        } catch (Exception e) {
            //执行时长(毫秒)
            long time = System.currentTimeMillis() - beginTime;
            //保存日志
            saveLog(user, point, time, OperationStatusEnum.FAIL.value());

            throw e;
        }
    }

    private void saveLog(UserDetail user, ProceedingJoinPoint joinPoint, long time, Integer status) {
        if (Objects.isNull(operationLogger)) {
            log.warn("系统未配置操作日志处理器，采用默认Redis日志处理器。");
            logProducer.saveLog(user, joinPoint, time, status);
        } else {
            operationLogger.saveLog(user, joinPoint, time, status);
        }
    }

}
