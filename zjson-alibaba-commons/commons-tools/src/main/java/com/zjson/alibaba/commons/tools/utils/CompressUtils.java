package com.zjson.alibaba.commons.tools.utils;

import com.zjson.alibaba.commons.tools.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * 压缩与解压缩工具类
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/12/21 14:24
 */
@Slf4j
public class CompressUtils {

    /**
     * GZIP 文本压缩
     *
     * @param data 原始文本数据
     * @return 压缩后的Base64转码内容
     */
    public static String compress(String data) {
        if (StringUtils.isBlank(data)) {
            return data;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = null;
        try {
            gzip = new GZIPOutputStream(out);
            gzip.write(data.getBytes());
        } catch (IOException e) {
            log.error("数据GZIP压缩异常", e);
            throw new CustomException("数据GZIP压缩异常");
        } finally {
            if (gzip != null) {
                try {
                    gzip.close();
                } catch (IOException e) {
                    log.error("数据GZIP压缩异常", e);
                }
            }
        }
        return new BASE64Encoder().encode(out.toByteArray());
    }

    /**
     * GZIP 解压缩
     *
     * @param data 压缩后的Base64转码内容
     * @return 原始文本数据
     */
    public static String uncompress(String data) {
        if (StringUtils.isBlank(data)) {
            return data;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = null;
        GZIPInputStream inGzip = null;
        byte[] compressed;
        String decompressed;
        try {
            compressed = new BASE64Decoder().decodeBuffer(data);
            in = new ByteArrayInputStream(compressed);
            inGzip = new GZIPInputStream(in);
            byte[] buffer = new byte[1024];
            int offset;
            while ((offset = inGzip.read(buffer)) != -1) {
                out.write(buffer, 0, offset);
            }
            decompressed = out.toString();
        } catch (IOException e) {
            log.error("数据GZIP解压缩异常", e);
            throw new CustomException("数据GZIP解压缩异常");
        } finally {
            if (inGzip != null) {
                try {
                    inGzip.close();
                } catch (IOException e) {
                    log.error("解压缩-inGzip流关闭异常", e);
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.error("解压缩-in流关闭异常", e);
                }
            }
            try {
                out.close();
            } catch (IOException e) {
                log.error("解压缩-out流关闭异常", e);
            }
        }
        return decompressed;
    }

    public static void main(String[] args) {
        String originalData = "{\"id\": \"1336880041402322945\", \"fee\": null, \"fees\": null, \"rootId\": \"1336863817433911297\", \"endDate\": null, \"skuCode\": \"NSKU202012101146540262\", \"spuCode\": \"NSP202012101146540100\", \"isEnable\": 1, \"storeId\": null, \"goodsName\": \"美白面膜\", \"goodsType\": 1, \"goodsUnit\": \"片\", \"categoryId\": \"1336878548540477443\", \"childGoods\": null, \"merchantId\": \"1336855578808455170\", \"salesPrice\": 0.01, \"totalTimes\": null, \"expiredDate\": null, \"goodsStatus\": 1, \"infoPicture\": \"https://dev-my-common.meiy520.com/normal-saas/20201210/66704ad12c274ccba1965da532b8936f.png\", \"isLimitTime\": null, \"listPicture\": \"https://dev-my-common.meiy520.com/normal-saas/20201210/66704ad12c274ccba1965da532b8936f.png\", \"marketPrice\": null, \"productType\": 1, \"categoryName\": \"my二级家装\", \"goodsBrandId\": \"1336858649967497217\", \"isServiceFee\": false, \"isUsestores\": true, \"storeIdList\": null, \"skuAttribute\": \"10\", \"goodsSynopsis\": null, \"isUseProperty\": null, \"bindMerchantId\": \"1336858649865261059\", \"goodsChildType\": null, \"serviceFeeType\": null, \"clientIndexShow\": 2, \"serviceDuration\": null, \"categoryDiscounts\": null, \"goodsDescribeText\": \"<p>美白急救</p>\", \"storeWarningCount\": null, \"salesCommissionType\": null, \"salesCommissionValue\": null}";
        System.out.println(originalData + "\n" + originalData.length());

        String compressedData = CompressUtils.compress(originalData);
        System.out.println("GZIP\n" + compressedData + "\n" + compressedData.length());

        String uncompressedData = CompressUtils.uncompress(compressedData);
        System.out.println("GZIP\n" + uncompressedData + "\n" + uncompressedData.length() + "\nequals:\t" + originalData.equals(uncompressedData));
    }
}
