package com.zjson.alibaba.commons.tools.redis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 字典信息redis获取
 *
 * @author yaozou
 * @date 2020/02/10 18:30
 */
@Component
@Slf4j
public class DictRedis {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public Map<Integer, String> getNames(String type) {
        Object object = redisTemplate.opsForValue().get(type);
        if (object == null) {
            return new HashMap<>();
        }
        JSONArray jsonArray = JSON.parseArray(object.toString());
        Map<Integer, String> map = new HashMap<>(jsonArray.size());
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject json = jsonArray.getJSONObject(i);
            map.put(json.getInteger("dictValue"), json.getString("dictLabel"));
        }
        return map;
    }

}
