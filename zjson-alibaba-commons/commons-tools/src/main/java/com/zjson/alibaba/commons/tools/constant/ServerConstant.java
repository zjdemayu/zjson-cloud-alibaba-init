package com.zjson.alibaba.commons.tools.constant;

/**
 * 微服务名称常量
 *
 * @author zz
 */
public interface ServerConstant {

    /**
     * 授权服务
     */
    String ZJSON_ALIBABA_AUTH = "zjson-alibaba-auth";

    /**
     * 系统服务
     */
    String ZJSON_ALIBABA_SYSTEM = "zjson-alibaba-system";


}
