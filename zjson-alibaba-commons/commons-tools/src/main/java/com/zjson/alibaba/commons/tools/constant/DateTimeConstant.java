package com.zjson.alibaba.commons.tools.constant;

/**
 * 日期时间相关常量定义
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2019/10/30 17:33
 */
public class DateTimeConstant {

    public final static String PATTERN_STANDARD = "yyyy-MM-dd HH:mm:ss";

    public final static String PATTERN_yyyy_MM_dd = "yyyy-MM-dd";

    public final static String PATTERN_HH_mm_ss = "HH:mm:ss";

    public final static String PATTERN_STANDARD_6 = "yyyy-MM-dd HH:mm:ss.SSSSSS";

}
