package com.zjson.alibaba.commons.tools.enums;

/**
 * 超级管理员枚举
 * 0-否，1-是
 */
public enum SuperAdminEnum {
    YES(1),
    NO(0);

    private int value;

    SuperAdminEnum(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }
}
