package com.zjson.alibaba.commons.tools.annotation;


import com.zjson.alibaba.commons.tools.log.enums.LogTypeEnum;

import java.lang.annotation.*;

/**
 * 操作日志注解
 * @author admin
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogOperation {

    LogTypeEnum type() default LogTypeEnum.OPERATION;

    String value() default "";

}
