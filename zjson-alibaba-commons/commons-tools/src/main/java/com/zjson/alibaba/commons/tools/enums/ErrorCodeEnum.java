package com.zjson.alibaba.commons.tools.enums;

import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.exception.ErrorCode;
import lombok.Getter;

/**
 * 错误状态枚举
 *
 * @author zengzhi
 */
@Getter
public enum ErrorCodeEnum {
    /**
     * 错误状态枚举
     */
    INTERNAL_SERVER_ERROR(ErrorCode.INTERNAL_SERVER_ERROR, "系统开小差了，请稍后重试！"),
    UNAUTHORIZED(ErrorCode.UNAUTHORIZED, "未授权！"),
    AUTH_SIGNED_IN_OTHER(ErrorCode.AUTH_SIGNED_IN_OTHER, "您的账号已在其他设备登录"),
    FORBIDDEN(ErrorCode.FORBIDDEN, "拒绝访问，没有权限！");


    private final Integer code;

    private final String desc;

    ErrorCodeEnum(final Integer code, final String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ErrorCodeEnum parse(Integer errorCode) {
        for (ErrorCodeEnum statusCodeEnum : values()) {
            if (statusCodeEnum.getCode().equals(errorCode)) {
                return statusCodeEnum;
            }
        }
        throw new CustomException("非法的code码：" + errorCode);
    }
}
