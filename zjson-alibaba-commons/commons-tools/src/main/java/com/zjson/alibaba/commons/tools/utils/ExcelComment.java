package com.zjson.alibaba.commons.tools.utils;

import lombok.Data;

/**
 * excel批注
 */
@Data
public class ExcelComment {
    /**
     * 行
     */
    private int row;
    /**
     *  列
     */
    private int cell;
    /**
     * 批注内容
     */
    private String comment;

}
