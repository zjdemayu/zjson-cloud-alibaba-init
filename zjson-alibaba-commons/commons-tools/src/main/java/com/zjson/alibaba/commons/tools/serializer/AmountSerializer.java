package com.zjson.alibaba.commons.tools.serializer;


import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.zjson.alibaba.commons.tools.constant.AesSaltConstant;
import com.zjson.alibaba.commons.tools.utils.encrypt.AesUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;

/**
 * created on 2020/11/4 18:01
 *
 * @author yaozou
 * @since v1.0.0
 */
public class AmountSerializer implements ObjectSerializer {

    @Override
    public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
        if (object != null){
            BigDecimal amount = (BigDecimal) object;
            AesUtils aesUtils = new AesUtils();
            String amountStr = AesUtils.encrypt(amount.toPlainString(), AesSaltConstant.AMOUNT_SALT);
            serializer.write(amountStr);
        }
    }
}
