/*
 * Copyright (c) 2020. 成都美源网络科技有限公司（https://www.meiy365.com/）
 */

package com.zjson.alibaba.commons.tools.request;

import com.zjson.alibaba.commons.tools.exception.CustomException;

/**
 * HTTP请求异常
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/7/2 14:21
 */
public class HttpRequestException extends CustomException {


    public HttpRequestException(int code) {
        super(code);
    }

    public HttpRequestException(int code, String... params) {
        super(code, params);
    }

    public HttpRequestException(int code, String msg) {
        super(code, msg);
    }

    public HttpRequestException(int code, Throwable e) {
        super(code, e);
    }

    public HttpRequestException(int code, Throwable e, String... params) {
        super(code, e, params);
    }

    public HttpRequestException(String msg) {
        super(msg);
    }

    public HttpRequestException(String msg, Throwable e) {
        super(msg, e);
    }
}
