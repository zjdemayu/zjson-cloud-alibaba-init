package com.zjson.alibaba.commons.tools.properties;

import lombok.Data;

/**
 * 短信验证码配置
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/6/7 10:39
 */
@Data
public class SmsVerifyCode {
	/**
	 * 是否开启调试模式
	 */
	private Boolean debugSwitch;
	/**
	 * 默认验证码
	 */
	private String defaultCode;

	/**
	 * 短信验证码编号code
	 */
	private String rightsVerifyCode;

	/**
	 * 默认过期时间
	 */
	private Integer defaultExpire;
}
