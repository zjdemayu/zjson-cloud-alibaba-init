package com.zjson.alibaba.commons.tools.utils;


import com.zjson.alibaba.commons.tools.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

/**
 * 功能描述:
 * @author:  cjh
 * @date: 2020/1/7 11:06
 */
@Slf4j
public class MultipartUtil {

    /**
     * 功能描述: 商户中心文件上传场景
     * @param multipartFile
     * @param fileDir
     * @param fileName
     * @param fileSuffix
     * @return
     * @author:  cjh
     * @date: 2020/1/7 11:32
     */
    public static void uploadMerchantFile(MultipartFile multipartFile, String fileDir, String fileName, String fileSuffix){
        if (multipartFile.isEmpty() || multipartFile.getSize() <= 0) {
            throw new CustomException("文件内容为空");
        }
        String originalFilename = multipartFile.getOriginalFilename();
        if (StringUtils.isBlank(originalFilename)) {
            throw new CustomException("非法文件名");
        }
        String suffix = originalFilename.substring(multipartFile.getOriginalFilename().lastIndexOf(".") + 1);
        if (!fileSuffix.equals(suffix)){
            throw new CustomException("非法文件格式");
        }

        File file = new File(fileDir, fileName);
        if (!file.exists()){
            File file1 = new File(fileDir);
            if (!file1.exists() && !file1.mkdirs()) {
                throw new CustomException("创建文件目录失败");
            }
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                if (log.isErrorEnabled()) {
                    log.error("创建文件失败，系统异常：【{}】", e.getMessage());
                }
                throw new CustomException("文件上传失败，系统异常");
            }
        }

        try (InputStream inputStream = multipartFile.getInputStream();
             FileOutputStream fileOutputStream = new FileOutputStream(file)){
            byte[] size = new byte[1024];
            int len;
            while ((len = inputStream.read(size)) != -1){
                fileOutputStream.write(size, 0, len);
                fileOutputStream.flush();
            }
        } catch (IOException e){
            e.printStackTrace();
            if (log.isErrorEnabled()) {
                log.error("文件上传失败，系统异常：【{}】", e.getMessage());
            }
            throw new CustomException("文件上传失败，系统异常");
        }
    }

    /**
     * 功能描述: 商户中心文件复制上传场景
     * @param oldFileName
     * @param newFileName
     * @param fileDir
     * @return
     * @author:  cjh
     * @date: 2020/1/7 11:42
     */
    public static void copyMerchantFile(String oldFileName, String newFileName, String fileDir) {
        File file = new File(fileDir + newFileName);
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                if (log.isErrorEnabled()) {
                    log.error("创建文件失败，系统异常：【{}】", e.getMessage());
                }
                throw new CustomException("文件保存失败，系统异常");
            }
        }

        try (InputStream inputStream = new FileInputStream(fileDir + oldFileName);
             FileOutputStream fileOutputStream = new FileOutputStream(file)){
            byte[] size = new byte[1024];
            int len;
            while ((len = inputStream.read(size)) != -1){
                fileOutputStream.write(size, 0, len);
                fileOutputStream.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            if (log.isErrorEnabled()) {
                log.error("文件保存失败，系统异常：【{}】", e.getMessage());
            }
            throw new CustomException("文件保存失败，系统异常");
        }
    }
}
