package com.zjson.alibaba.commons.tools.exception;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import com.zjson.alibaba.commons.tools.config.ModuleConfig;
import com.zjson.alibaba.commons.tools.enums.ErrorCodeEnum;
import com.zjson.alibaba.commons.tools.security.user.SecurityUser;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import com.zjson.alibaba.commons.tools.utils.HttpContextUtils;
import com.zjson.alibaba.commons.tools.utils.IpUtils;
import com.zjson.alibaba.commons.tools.utils.Result;
import com.zjson.alibaba.commons.tools.log.SysLogError;
import com.zjson.alibaba.commons.tools.log.enums.LogTypeEnum;
import com.zjson.alibaba.commons.tools.log.producer.LogProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.UncategorizedDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.*;


/**
 * 异常处理器
 *
 * @author admin
 */
@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler {

    private static final String INCORRECT_STRING_VALUE_CONTENT = "Incorrect string value";

    @Autowired
    private ModuleConfig moduleConfig;

    @Autowired
    private LogProducer logProducer;

    /**
     * 处理自定义异常
     */
    @ExceptionHandler(CustomException.class)
    public Result<Boolean> handleRrException(CustomException ex) {
        log.error(ex.getMsg(), ex);
        return Result.fail(ex.getCode(), ex.getMsg());
    }

    /**
     * 处理主键重复异常
     */
    @ExceptionHandler(DuplicateKeyException.class)
    public Result<Boolean> handleDuplicateKeyException(DuplicateKeyException ex) {
        log.error(ex.getMessage(), ex);
        return Result.fail(ErrorCodeEnum.INTERNAL_SERVER_ERROR.getDesc());
    }

    /**
     * handleUncategorizedDataAccessException
     */
    @ExceptionHandler(UncategorizedDataAccessException.class)
    public Result<Boolean> handleUncategorizedDataAccessException(UncategorizedDataAccessException ex) {
        String errorMsg = ex.getMessage();
        log.error(errorMsg, ex);
        if (StringUtils.isNotBlank(errorMsg) && errorMsg.contains(INCORRECT_STRING_VALUE_CONTENT)) {
            return Result.fail("请勿输入表情符号");
        }
        saveLog(ex);
        return Result.fail(ErrorCodeEnum.INTERNAL_SERVER_ERROR.getDesc());
    }

    /**
     * 处理参数效验异常
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result<Boolean> parameterExceptionHandler(MethodArgumentNotValidException e) {
        // 获取异常信息
        BindingResult exceptions = e.getBindingResult();
        // 判断异常中是否有错误信息，如果存在就使用异常中的消息，否则使用默认消息
        if (exceptions.hasErrors()) {
            List<ObjectError> errors = exceptions.getAllErrors();
            if (!errors.isEmpty()) {
                // 这里列出了全部错误参数，按正常逻辑，只需要第一条错误即可
                FieldError fieldError = (FieldError) errors.get(0);
                return Result.fail(fieldError.getDefaultMessage());
            }
        }
        return Result.fail();
    }

    /**
     * 处理其他异常
     */
    @ExceptionHandler(Exception.class)
    public Result<Boolean> handleException(Exception ex) {
        log.error(ex.getMessage(), ex);
        saveLog(ex);
        return Result.fail(ErrorCodeEnum.INTERNAL_SERVER_ERROR.getDesc());
    }

    /**
     * 权限异常
     */
    @ExceptionHandler(PreAuthorizeException.class)
    public Result<Boolean> preAuthorizeException(PreAuthorizeException ex) {
        log.error(ex.getMessage(), ex);
        saveLog(ex);
        return Result.fail(ErrorCodeEnum.FORBIDDEN.getCode(), ErrorCodeEnum.FORBIDDEN.getDesc());
    }

    /**
     * handleConstraintViolationException
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public Result<Boolean> handleConstraintViolationException(ConstraintViolationException ex) {
        log.error(ex.getMessage(), ex);
        saveLog(ex);
        return Result.fail(ex.getMessage());
    }


    /**
     * 保存异常日志
     */
    private void saveLog(Exception ex) {
        SysLogError log = new SysLogError();
        log.setType(LogTypeEnum.ERROR.value());
        log.setModule(moduleConfig.getName());

        //请求相关信息
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        if (Objects.nonNull(request)) {
            log.setUserAgent(request.getHeader(HttpHeaders.USER_AGENT));
            log.setRequestUri(request.getRequestURI());
            log.setRequestMethod(request.getMethod());
            log.setIp(IpUtils.getIpAddr(request));
            Map<String, String> params = HttpContextUtils.getParameterMap(request);
            if (MapUtil.isNotEmpty(params)) {
                log.setRequestParams(JSON.toJSONString(params));
            }
        }
        //登录用户ID
        log.setCreateBy(Optional.ofNullable(SecurityUser.getUserWithoutUnauthorizedException()).map(UserDetail::getId).orElse(null));

        //异常信息
        log.setErrorInfo(ExceptionUtils.getErrorStackTrace(ex));

        //保存到Redis队列里
        log.setCreateTime(LocalDateTime.now());
        logProducer.saveLog(log);
    }


}
