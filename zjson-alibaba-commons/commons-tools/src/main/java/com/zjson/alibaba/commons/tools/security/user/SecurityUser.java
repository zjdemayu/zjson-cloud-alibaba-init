package com.zjson.alibaba.commons.tools.security.user;


import com.zjson.alibaba.commons.tools.constant.Constant;
import com.zjson.alibaba.commons.tools.enums.ErrorCodeEnum;
import com.zjson.alibaba.commons.tools.enums.TerminalClientEnum;
import com.zjson.alibaba.commons.tools.security.util.UserDetailRedisUtils;
import com.zjson.alibaba.commons.tools.utils.HttpContextUtils;
import com.zjson.alibaba.commons.tools.utils.SpringContextUtils;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * 用户
 *
 * @author admin
 */
public class SecurityUser {

    private static final UserDetailRedisUtils USER_DETAIL_REDIS;

    static {
        USER_DETAIL_REDIS = SpringContextUtils.getBean(UserDetailRedisUtils.class);
    }

    /**
     * 获取用户信息，用户未登录返回null，不抛异常
     */
    public static UserDetail getUserWithoutUnauthorizedException() {
        return Optional.ofNullable(HttpContextUtils.getHttpServletRequest())
                .map(request -> request.getHeader(Constant.USER_KEY))
                .filter(StringUtils::isNotBlank)
                .map(USER_DETAIL_REDIS::get)
                .orElse(null);
    }

    /**
     * 获取用户信息，用户未登录抛出401异常
     */
    public static UserDetail getUser() {
        return Optional.ofNullable(getUserId())
                .map(USER_DETAIL_REDIS::get)
                .orElseThrow(() -> new CustomException(ErrorCodeEnum.UNAUTHORIZED.getCode(),ErrorCodeEnum.UNAUTHORIZED.getDesc()));
    }

    /**
     * 获取用户ID，用户未登录抛出401异常
     */
    public static String getUserId() {
        return Optional.ofNullable(HttpContextUtils.getHttpServletRequest())
                .map(request -> request.getHeader(Constant.USER_KEY))
                .filter(StringUtils::isNotBlank)
                .orElseThrow(() -> new CustomException(ErrorCodeEnum.UNAUTHORIZED.getCode(),ErrorCodeEnum.UNAUTHORIZED.getDesc()));
    }

    /**
     * 获取门店ID，用户未登录抛出401异常
     */
    public static String getStoreId() {
        return getUser().getStoreId();
    }

    /**
     * 获取商户ID，用户未登录抛出401异常
     */
    public static String getMerchantId() {
        return getUser().getMerchantId();
    }

    /**
     * 获取用户请求的终端类型
     *
     * @return 终端类型
     */
    public static TerminalClientEnum getTerminalClient() {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        String terminalClient = request == null ? null : request.getHeader(Constant.TERMINAL_CLIENT);
        if (StringUtils.isBlank(terminalClient)) {
            throw new CustomException("缺少终端标识[" + Constant.TERMINAL_CLIENT + "]");
        }
        try {
            return TerminalClientEnum.parse(Integer.parseInt(terminalClient));
        } catch (NumberFormatException e) {
            throw new CustomException("非法的终端标识[" + Constant.TERMINAL_CLIENT + "=" + terminalClient + "]");
        }
    }

    /**
     * 获取用户请求的终端类型
     *
     * @return 终端类型
     */
    public static TerminalClientEnum getTerminalClientDefaultNull() {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        String terminalClient = request == null ? null : request.getHeader(Constant.TERMINAL_CLIENT);
        if (StringUtils.isBlank(terminalClient)) {
            return null;
        }
        try {
            return TerminalClientEnum.parse(Integer.parseInt(terminalClient));
        } catch (NumberFormatException e) {
            throw new CustomException("非法的终端标识[" + Constant.TERMINAL_CLIENT + "=" + terminalClient + "]");
        }
    }

}
