package com.zjson.alibaba.commons.tools.security.enums;

/**
 * 用户被踢出枚举
 *
 * @author zz
 */
public enum UserKillEnum {
    /**
     * 被踢出
     */
    YES(1),
    NO(0);

    private final int value;

    UserKillEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
