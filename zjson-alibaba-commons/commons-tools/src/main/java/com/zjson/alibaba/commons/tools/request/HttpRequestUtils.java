/*
 * Copyright (c) 2020. 成都美源网络科技有限公司（https://www.meiy365.com/）
 */

package com.zjson.alibaba.commons.tools.request;

import com.zjson.alibaba.commons.tools.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;

/**
 * HTTP 请求工具类
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/7/2 13:59
 */
@Slf4j
public class HttpRequestUtils {

    private static final Header[] DEFAULT_HEADERS = getDefaultHeaders();

    /**
     * 获取默认的请求头信息
     * <p>
     * <ol>
     * <li>Content-Type:application/json</li>
     * </ol>
     * </p>
     *
     * @return 默认的请求头信息
     */
    public static Header[] getDefaultHeaders() {
        return new Header[]{
                new BasicHeader("Content-Type", "application/json")
        };
    }

    /**
     * GET 请求（无参）
     *
     * @param url 请求URL
     * @return 请求返回结果
     */
    public static String doGet(String url) {
        return doGet(url, null);
    }

    /**
     * GET 请求
     *
     * @param url   请求URL
     * @param param 请求参数
     * @return 请求返回结果
     */
    public static String doGet(@NotNull String url, Map<String, String> param) {
        // 创建Httpclient对象
        CloseableHttpClient client = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String resultString = "";
        try {
            // 创建URI
            URIBuilder builder = new URIBuilder(url);
            if (MapUtils.isNotEmpty(param)) {
                param.forEach(builder::addParameter);
            }
            URI uri = builder.build();
            // 创建GET请求
            HttpGet get = new HttpGet(uri);
            get.setHeaders(DEFAULT_HEADERS);
            // 执行请求
            response = client.execute(get);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                resultString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            }
        } catch (Exception e) {
            String msg = spliceExceptionMsg(HttpGet.METHOD_NAME, url, param);
            log.error(msg, e);
            throw new HttpRequestException(msg);
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                client.close();
            } catch (IOException e) {
                String msg = spliceExceptionMsg(HttpGet.METHOD_NAME, url, param);
                log.warn(msg, e);
            }
        }
        return resultString;
    }

    /**
     * GET 请求
     *
     * @param url   请求URL
     * @param param 请求参数
     * @return 请求返回结果
     */
    public static String doGet(@NotNull String url, Map<String, String> param, Header[] headers) {
        // 创建Httpclient对象
        CloseableHttpClient client = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String resultString = "";
        try {
            // 创建URI
            URIBuilder builder = new URIBuilder(url);
            if (MapUtils.isNotEmpty(param)) {
                param.forEach(builder::addParameter);
            }
            URI uri = builder.build();
            // 创建GET请求
            HttpGet get = new HttpGet(uri);
            if (headers != null && headers.length > 0) {
                get.setHeaders(headers);
            } else {
                get.setHeaders(DEFAULT_HEADERS);
            }
            // 执行请求
            response = client.execute(get);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                resultString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            }
        } catch (Exception e) {
            String msg = spliceExceptionMsg(HttpGet.METHOD_NAME, url, param);
            log.error(msg, e);
            throw new HttpRequestException(msg);
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                client.close();
            } catch (IOException e) {
                String msg = spliceExceptionMsg(HttpGet.METHOD_NAME, url, param);
                log.warn(msg, e);
            }
        }
        return resultString;
    }

    /**
     * POST 请求（无参）
     *
     * @param url 请求URL
     * @return 请求返回结果
     */
    public static String doPost(String url) {
        return doPost(url, null);
    }

    /**
     * POST 请求
     *
     * @param url  请求URL
     * @param body 请求体
     * @return 请求返回结果
     */
    public static String doPost(String url, Object body) {
        // 创建Httpclient对象
        CloseableHttpClient client = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String resultString = "";
        try {
            // 创建POST请求
            HttpPost post = new HttpPost(url);
            post.setHeaders(DEFAULT_HEADERS);
            // 创建请求体信息
            if (Objects.nonNull(body)) {
                StringEntity entity = new StringEntity(JsonUtils.objectToJson(body), ContentType.APPLICATION_JSON);
                post.setEntity(entity);
            }
            // 执行请求
            response = client.execute(post);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                resultString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            }
        } catch (Exception e) {
            String msg = spliceExceptionMsg(HttpPost.METHOD_NAME, url, body);
            log.error(msg, e);
            throw new HttpRequestException(msg);
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                client.close();
            } catch (IOException e) {
                String msg = spliceExceptionMsg(HttpPost.METHOD_NAME, url, body);
                log.warn(msg, e);
            }
        }

        return resultString;
    }

    /**
     * 打印异常请求信息信息
     *
     * @param method 请求方式
     * @param url    请求URL
     * @param param  请求参数
     * @return 异常信息
     */
    private static String spliceExceptionMsg(String method, String url, Map<String, String> param) {
        String msg = "HTTP请求异常：" + method + "\turl=" + url;
        if (MapUtils.isNotEmpty(param)) {
            msg += "\tparam={}" + JsonUtils.objectToJson(param);
        }
        return msg;
    }

    /**
     * 打印异常请求信息信息
     *
     * @param method 请求信息
     * @param url    请求URL
     * @param body   请求体信息
     * @return 异常信息
     */
    private static String spliceExceptionMsg(String method, String url, Object body) {
        String msg = "HTTP请求异常：" + method + "\turl=" + url;
        if (body != null) {
            msg += "\tbody=" + JsonUtils.objectToJson(body);
        }
        return msg;
    }
}
