package com.zjson.alibaba.commons.tools.config;

/**
 * 模块配置信息
 */
public interface ModuleConfig {

    /**
     * 获取模块名称
     */
    String getName();

}
