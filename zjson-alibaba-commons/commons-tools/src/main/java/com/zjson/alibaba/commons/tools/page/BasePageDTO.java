package com.zjson.alibaba.commons.tools.page;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Project: saas-cloud
 * @package com.zjson.alibaba.commons.tools.page
 * @author : luojianhong
 * @date Date : 2019年10月17日 10:40
 * @version V1.0
 * 分页基础类
 */
@Data
public class BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "当前页")
    private Long pageNo;

    @ApiModelProperty(value = "每页大小,不分页传：-1")
    private Long pageSize;

}
