package com.zjson.alibaba.commons.tools.validator;

import lombok.Data;

import javax.validation.Valid;
import java.util.List;

/**
 * 参数 List<T>验证参数
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2020/12/8 16:24
 */
@Data
public class ListRequestDTO<T> {

	@Valid
	private List<T> list;

	public ListRequestDTO(@Valid List<T> list) {
		this.list = list;
	}

}
