package com.zjson.alibaba.commons.mybatis.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.zjson.alibaba.commons.mybatis.constant.FieldNameConstant;
import com.zjson.alibaba.commons.mybatis.enums.DelFlagEnum;
import com.zjson.alibaba.commons.tools.security.user.SecurityUser;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;


/**
 * 公共字段，自动填充值
 *
 * @author zz
 */
@Component
public class FieldMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        LocalDateTime localDateTime = LocalDateTime.now();
        // 创建时间
        if (metaObject.hasGetter(FieldNameConstant.CREATE_TIME) && Objects.isNull(metaObject.getValue(FieldNameConstant.CREATE_TIME))) {
            strictInsertFill(metaObject, FieldNameConstant.CREATE_TIME, LocalDateTime.class, localDateTime);
        }
        // 更新时间
        if (metaObject.hasGetter(FieldNameConstant.UPDATE_TIME) && Objects.isNull(metaObject.getValue(FieldNameConstant.UPDATE_TIME))) {
            strictInsertFill(metaObject, FieldNameConstant.UPDATE_TIME, LocalDateTime.class, localDateTime);
        }
        // 删除标识
        if (metaObject.hasGetter(FieldNameConstant.DEL_FLAG_ENUM) && Objects.isNull(metaObject.getValue(FieldNameConstant.DEL_FLAG_ENUM))) {
            strictInsertFill(metaObject, FieldNameConstant.DEL_FLAG_ENUM, DelFlagEnum.class, DelFlagEnum.NORMAL);
        }
        // 用户信息
        Optional.ofNullable(SecurityUser.getUserWithoutUnauthorizedException())
                .ifPresent(userDetail -> {
                    // 创建人id
                    if (Objects.isNull(getFieldValByName(FieldNameConstant.CREATE_BY, metaObject))) {
                        strictInsertFill(metaObject, FieldNameConstant.CREATE_BY, String.class, userDetail.getId());
                    }
                    // 创建人名
                    if (Objects.isNull(getFieldValByName(FieldNameConstant.CREATE_NAME, metaObject))) {
                        strictInsertFill(metaObject, FieldNameConstant.CREATE_NAME, String.class, userDetail.getId());
                    }
                    // 更新人id
                    if (Objects.isNull(getFieldValByName(FieldNameConstant.UPDATE_BY, metaObject))) {
                        strictInsertFill(metaObject, FieldNameConstant.UPDATE_BY, String.class, userDetail.getId());
                    }
                    // 更新人名
                    if (Objects.isNull(getFieldValByName(FieldNameConstant.UPDATE_NAME, metaObject))) {
                        strictInsertFill(metaObject, FieldNameConstant.UPDATE_NAME, String.class, userDetail.getId());
                    }
                });
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 更新时间
        if (metaObject.hasGetter(FieldNameConstant.UPDATE_TIME) && Objects.isNull(metaObject.getValue(FieldNameConstant.UPDATE_TIME))) {
            strictUpdateFill(metaObject, FieldNameConstant.UPDATE_TIME, LocalDateTime.class, LocalDateTime.now());
        }
        // 用户信息
        Optional.ofNullable(SecurityUser.getUserWithoutUnauthorizedException())
                .ifPresent(userDetail -> {
                    // 更新人id
                    if (Objects.isNull(getFieldValByName(FieldNameConstant.UPDATE_BY, metaObject))) {
                        strictInsertFill(metaObject, FieldNameConstant.UPDATE_BY, String.class, userDetail.getId());
                    }
                    // 更新人名
                    if (Objects.isNull(getFieldValByName(FieldNameConstant.UPDATE_NAME, metaObject))) {
                        strictInsertFill(metaObject, FieldNameConstant.UPDATE_NAME, String.class, userDetail.getId());
                    }
                });
    }

}
