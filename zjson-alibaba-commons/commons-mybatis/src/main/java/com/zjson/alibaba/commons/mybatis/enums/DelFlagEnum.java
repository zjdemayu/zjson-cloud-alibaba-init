package com.zjson.alibaba.commons.mybatis.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 删除标识枚举类 是否删除 0：否；1：删除
 *
 * @author admin
 */
public enum DelFlagEnum {
    /**
     * 是否删除 0：否；1：删除
     */
    NORMAL(false),
    DEL(true);

    @EnumValue
    @JsonValue
    private final Boolean value;

    DelFlagEnum(Boolean value) {
        this.value = value;
    }

    public Boolean getValue() {
        return this.value;
    }
}
