package com.zjson.alibaba.commons.mybatis.handler;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.springframework.stereotype.Component;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 自定义mybatis类型转换
 *
 * @author zz
 */
@MappedTypes({List.class})
@MappedJdbcTypes({JdbcType.VARCHAR})
@Component
public class StringToListTypeHandler extends BaseTypeHandler<List<String>> {

    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, List<String> parameter, JdbcType jdbcType) throws SQLException {
        preparedStatement.setString(i, Joiner.on(",").skipNulls().join(parameter));
    }

    @Override
    public List<String> getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
        return this.stringToList(resultSet.getString(columnName));
    }

    @Override
    public List<String> getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
        return this.stringToList(resultSet.getString(columnIndex));
    }

    @Override
    public List<String> getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
        return this.stringToList(callableStatement.getString(columnIndex));
    }

    private List<String> stringToList(String str) {
        return StringUtils.isBlank(str) ?
                Lists.newArrayList() :
                Arrays.stream(StringUtils.split(str, ","))
                        .filter(Objects::nonNull).collect(Collectors.toList());
    }

}
