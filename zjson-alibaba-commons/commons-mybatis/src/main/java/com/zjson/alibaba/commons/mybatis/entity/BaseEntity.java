package com.zjson.alibaba.commons.mybatis.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.zjson.alibaba.commons.mybatis.enums.DelFlagEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 基础实体类，所有实体都需要继承
 *
 * @author admin
 */
@Data
public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = -974707820668478484L;

    /**
     * id
     */
    @TableId
    private String id;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 更新人
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 是否删除（0：未删除[默认]；1：已删除）
     */
    @TableLogic
    @TableField(value = "is_del", fill = FieldFill.INSERT)
    private DelFlagEnum delFlagEnum;

}
