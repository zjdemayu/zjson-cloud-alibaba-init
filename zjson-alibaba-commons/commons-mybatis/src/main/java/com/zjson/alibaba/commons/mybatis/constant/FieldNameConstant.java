package com.zjson.alibaba.commons.mybatis.constant;

/**
 * 公共字段名称
 *
 * @author zengzhi
 * @version 1.0
 * @since 2020/10/9 19:35
 */
public interface FieldNameConstant {

    /**
     * 创建时间
     */
    String CREATE_TIME = "createTime";

    /**
     * 创建人id
     */
    String CREATE_BY = "createBy";

    /**
     * 创建人名
     */
    String CREATE_NAME = "createName";

    /**
     * 更新时间
     */
    String UPDATE_TIME = "updateTime";

    /**
     * 更新人id
     */
    String UPDATE_BY = "updateBy";

    /**
     * 更新人名
     */
    String UPDATE_NAME = "updateName";

    /**
     * 逻辑删除
     */
    String DEL_FLAG_ENUM = "delFlagEnum";

}
