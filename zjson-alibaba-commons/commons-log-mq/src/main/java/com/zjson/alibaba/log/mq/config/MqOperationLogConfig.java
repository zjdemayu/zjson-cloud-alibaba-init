package com.zjson.alibaba.log.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 操作日志MQ消息配置
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2020/6/10 17:26
 */
@Configuration
public class MqOperationLogConfig {

    public final static String OPERATION_LOG_EXCHANGE = "operation-log.direct.exchange";

    public final static String OPERATION_LOG_QUEUE = "operation-log.queue";

    public final static String OPERATION_LOG_BINDING = "operation-log.key";

    /**
     * 操作日志直传交换机
     *
     * @return the exchange
     */
    @Bean
    public Exchange operationLogExchange() {
        return ExchangeBuilder.directExchange(OPERATION_LOG_EXCHANGE).durable(Boolean.TRUE).build();
    }

    /**
     * 操作日志消息队列
     *
     * @return the queue
     */
    @Bean
    public Queue operationLogQueue() {
        return QueueBuilder.durable(OPERATION_LOG_QUEUE).build();
    }

    /**
     * 操作日志消息路由键
     *
     * @return the binding
     */
    @Bean
    public Binding operationLogBinding() {
        return BindingBuilder.bind(operationLogQueue()).to(operationLogExchange()).with(OPERATION_LOG_BINDING)
                .noargs();
    }
}
