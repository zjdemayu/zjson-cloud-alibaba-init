package com.zjson.alibaba.log.mq;

import com.zjson.alibaba.log.mq.config.MqOperationLogConfig;
import com.zjson.alibaba.commons.tools.log.AbstractOperationLogger;
import com.zjson.alibaba.commons.tools.log.BaseLog;
import com.zjson.alibaba.commons.tools.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Redis 日志处理实现
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2020/6/10 16:53
 */
@Slf4j
@Service
public class MqOperationLogger extends AbstractOperationLogger {

    @Resource
    private AmqpTemplate amqpTemplate;

    /**
     * 保存Log到MQ消息队列
     */
    @Override
    protected void saveLog(BaseLog operationLog) {
        amqpTemplate.convertAndSend(MqOperationLogConfig.OPERATION_LOG_EXCHANGE, MqOperationLogConfig.OPERATION_LOG_BINDING, JsonUtils.objectToJson(operationLog));
    }
}
