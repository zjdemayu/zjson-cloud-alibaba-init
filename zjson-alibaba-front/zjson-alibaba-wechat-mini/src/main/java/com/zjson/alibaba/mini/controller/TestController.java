package com.zjson.alibaba.mini.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试controller
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/5/12 10:09
 */
@RestController
@RequestMapping("test")
public class TestController {

	@GetMapping("t")
	public String test() {
		return "test";
	}
}
