package com.zjson.alibaba.mini.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.cloud.sleuth.instrument.async.LazyTraceExecutor;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author zengzhi
 */
@Slf4j
@Configuration
public class AsyncConfig implements AsyncConfigurer {

    private static final int CORE_POOL_SIZE = Runtime.getRuntime().availableProcessors() * 2;

    private static final int MAX_POOL_SIZE = Math.max(CORE_POOL_SIZE * 4, 256);

    private static final int QUEUE_CAPACITY = 200;

    private static final String THREAD_NAME_PREFIX = "asyn-executor-";

    private final BeanFactory beanFactory;

    public AsyncConfig(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Override
    public Executor getAsyncExecutor() {
        // 自定义线程池
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        // 核心线程数
        taskExecutor.setCorePoolSize(CORE_POOL_SIZE);
        // 最大线程数
        taskExecutor.setMaxPoolSize(MAX_POOL_SIZE);
        // 线程名前缀
        taskExecutor.setThreadNamePrefix(THREAD_NAME_PREFIX);
        // 线程队列最大线程数
        taskExecutor.setQueueCapacity(QUEUE_CAPACITY);
        // 被拒绝后的策略，线程队列满了以后交给调用者执行,也就是同步执行
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 初始化线程池
        taskExecutor.initialize();
        // 使用LazyTraceExecutor再次封装，以满足多线程traceId的传递
        return new LazyTraceExecutor(beanFactory, taskExecutor);
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (ex, method, params) ->
                log.error("Error Occurs in async method:{}; params:{}; message:", method, params, ex);
    }

}