package com.zjson.alibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 小程序端
 *
 * @author zz
 */
@EnableAsync
@SpringCloudApplication
public class FrontMiniAppApplication {

    public static void main(String[] args) {

        SpringApplication.run(FrontMiniAppApplication.class, args);
    }

}
