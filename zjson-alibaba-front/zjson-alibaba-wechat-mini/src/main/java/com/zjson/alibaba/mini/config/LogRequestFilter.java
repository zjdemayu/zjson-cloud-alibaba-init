package com.zjson.alibaba.mini.config;

import com.zjson.alibaba.commons.tools.utils.HttpContextUtils;
import com.zjson.alibaba.commons.tools.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * LOG 请求日志
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2019/11/5 14:16
 */
@Order(0)
@Slf4j
@WebFilter(urlPatterns = "/*")
public class LogRequestFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("request-ip:{}", HttpContextUtils.getRealIp(request));
            log.debug("request-uri:{} {}", request.getMethod(), request.getRequestURI());
            Map<String, String[]> paramMap = request.getParameterMap();
            Map<String, String> headerMap = HttpContextUtils.getHeaders(request);
            if (!CollectionUtils.isEmpty(headerMap)) {
                log.debug("request-headers:{}", JsonUtils.objectToJson(headerMap));
            }
            if (!CollectionUtils.isEmpty(paramMap)) {
                log.debug("request-params:{}", JsonUtils.objectToJson(paramMap));
            }
        }

        filterChain.doFilter(request, response);
    }
}
