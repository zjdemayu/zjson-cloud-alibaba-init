package com.zjson.alibaba.mini.controller.msg;

import com.zjson.alibaba.commons.tools.utils.Result;
import com.zjson.alibaba.message.business.dto.VerifyCodeDTO;
import com.zjson.alibaba.message.business.service.SmsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 凭证(Evidence)表控制层
 *
 * @author zengzhi
 * @date 2021-05-20 09:37:42
 */
@RestController
@RequestMapping("/v1/msg")
@Api(tags = "短信相关")
public class MsgController {

    @Resource
    private SmsService smsService;

    /**
     * 发送短信验证码
     *
     * @param verifyCodeDTO verifyCodeDTO
     * @return 验证码
     */
    @ApiOperation(value = "发送短信验证码", notes = "v1.0  发送短信验证码")
    @PutMapping(value = "/send/verifyCode")
    public Result<Boolean> sendVerifyCode(@RequestBody VerifyCodeDTO verifyCodeDTO) {
        String verifyCode = smsService.sendVerifyCode(verifyCodeDTO);
        return Result.succ(StringUtils.isNotBlank(verifyCode));
    }

    /**
     * 校验短信验证码
     *
     * @param verifyCodeDTO verifyCodeDTO
     * @return true false
     */
    @ApiOperation(value = "校验短信验证码", notes = "v1.0  校验短信验证码")
    @PutMapping(value = "/validate/verifyCode")
    public Result<Boolean> validateVerifyCode(@RequestBody VerifyCodeDTO verifyCodeDTO) {
        return Result.succ(smsService.validateVerifyCode(verifyCodeDTO));
    }

}
