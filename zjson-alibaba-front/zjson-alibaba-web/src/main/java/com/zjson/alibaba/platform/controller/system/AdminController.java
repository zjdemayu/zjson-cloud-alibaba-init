package com.zjson.alibaba.platform.controller.system;

import com.zjson.alibaba.commons.tools.annotation.LogOperation;
import com.zjson.alibaba.commons.tools.annotation.PreAuthorize;
import com.zjson.alibaba.commons.tools.utils.Result;
import com.zjson.alibaba.system.dto.AdminDTO;
import com.zjson.alibaba.system.feign.SysAdminFeignClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 平台管理员信息表(Admin)表控制层
 *
 * @author zengzhi
 * @date 2021-05-13 11:52:07
 */
@RestController
@RequestMapping("/v1/admin")
@Api(tags = "平台管理员管理")
public class AdminController {

    @Resource
    private SysAdminFeignClient sysAdminFeignClient;

    /**
     * 新增账户
     *
     * @return Result<String>
     */
    @LogOperation(value = "新增账户")
    @PreAuthorize(hasPermissions = "platform:admin:save")
    @PostMapping(value = "")
    @ApiOperation(value = "新增账户 platform:admin:save", notes = "v1.0  新增账户")
    public Result<String> save(@RequestBody AdminDTO adminDTO) {
        return sysAdminFeignClient.save(adminDTO);
    }


}
