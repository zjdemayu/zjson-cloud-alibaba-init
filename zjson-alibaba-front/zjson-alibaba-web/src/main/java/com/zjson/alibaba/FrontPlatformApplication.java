package com.zjson.alibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 平台端
 *
 * @author zz
 */
@SpringCloudApplication
@EnableFeignClients(basePackages = {"com.zjson"})
public class FrontPlatformApplication {

    public static void main(String[] args) {

        SpringApplication.run(FrontPlatformApplication.class, args);
    }

}
