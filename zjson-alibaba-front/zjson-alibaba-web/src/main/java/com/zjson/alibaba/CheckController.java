package com.zjson.alibaba;

import cn.hutool.http.HttpStatus;
import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.registry.NacosRegistration;
import com.alibaba.cloud.nacos.registry.NacosServiceRegistry;
import com.zjson.alibaba.commons.tools.constant.Constant;
import com.zjson.alibaba.commons.tools.utils.HttpContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 服务下线 与 健康检查
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/5/20 10:39
 */
@RestController
@RequestMapping("check")
@Slf4j
public class CheckController {

	 @Resource
	 private NacosDiscoveryProperties nacosDiscoveryProperties;
	@Resource
	private NacosRegistration nacosRegistration;
	@Resource
	private NacosServiceRegistry nacosServiceRegistry;


	/**
	 *  服务下线
	 * @return
	 */
	@GetMapping(value = "/deregister")
	public int deregisterInstance(HttpServletRequest request) {
		String realIp = HttpContextUtils.getRealIp(request);
		if (!Constant.LOCALHOST_KEY1.equals(realIp)
				&& !Constant.LOCALHOST_KEY2.equals(realIp)) {
			log.error("realIp:{},调用下线接口被阻止。", realIp);
			return HttpStatus.HTTP_FORBIDDEN;
		}
		String serviceName = nacosDiscoveryProperties.getService();
		String groupName = nacosDiscoveryProperties.getGroup();
		String clusterName = nacosDiscoveryProperties.getClusterName();
		String ip = nacosDiscoveryProperties.getIp();
		int port = nacosDiscoveryProperties.getPort();
		log.info("deregister from nacos, serviceName:{}, groupName:{}, clusterName:{}, ip:{}, port:{}", serviceName, groupName, clusterName, ip, port);
		nacosServiceRegistry.deregister(nacosRegistration);
		return HttpStatus.HTTP_OK;
	}

	/**
	 * 健康检查
	 * @return
	 */
	@GetMapping(value = "/health")
	public int healthCheck() {
		return HttpStatus.HTTP_OK;
	}

}
