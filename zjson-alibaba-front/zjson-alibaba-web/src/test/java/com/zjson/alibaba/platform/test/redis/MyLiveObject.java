package com.zjson.alibaba.platform.test.redis;

import lombok.Data;
import org.redisson.api.annotation.REntity;
import org.redisson.api.annotation.RId;
import org.redisson.api.annotation.RIndex;

@Data
@REntity
public class MyLiveObject {

    @RId
    private String id;

    @RIndex
    private String value;

    private MyLiveObject parent;

    public MyLiveObject(String id) {
        this.id = id;
    }

    public MyLiveObject() {
    }

    // getters and setters

}
