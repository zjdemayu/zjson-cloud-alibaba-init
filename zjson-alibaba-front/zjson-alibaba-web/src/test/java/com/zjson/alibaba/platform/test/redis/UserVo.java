package com.zjson.alibaba.platform.test.redis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserVo implements Serializable {

    private static final long serialVersionUID = 8577233461151272159L;

    private String id;

    private String name;

    private String address;

    private Integer age;


}