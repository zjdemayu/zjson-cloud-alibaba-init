package com.zjson.alibaba.platform.test.redis;

import cn.hutool.core.bean.BeanUtil;
import com.zjson.alibaba.commons.tools.enums.TerminalClientEnum;
import com.zjson.alibaba.commons.tools.redis.RedisKeys;
import com.zjson.alibaba.commons.tools.security.enums.SecurityStatusEnum;
import com.zjson.alibaba.commons.tools.security.util.UserDetailRedisUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisConfigTest {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    @Test
    public void testValueOption() throws Exception {
        UserVo userVo = new UserVo();
        userVo.setAddress("上海");
        userVo.setName("jantent");
        userVo.setAge(23);
        redisTemplate.opsForValue().set("test", userVo);
        System.out.println(redisTemplate.opsForValue().get("test"));
    }

    @Test
    public void testSetOperation() throws Exception {
        UserVo userVo = new UserVo();
        userVo.setAddress("北京");
        userVo.setName("jantent");
        userVo.setAge(23);
        UserVo auserVo = new UserVo();
        auserVo.setAddress("n柜昂周");
        auserVo.setName("antent");
        auserVo.setAge(23);
        redisTemplate.opsForSet().add("user:test", userVo, auserVo);
        Set<Object> result = redisTemplate.opsForSet().members("user:test");
        System.out.println(result);
    }

    @Test
    public void HashOperations() throws Exception {
        UserVo userVo = new UserVo();
        userVo.setAddress("北京");
        userVo.setName("jantent");
        userVo.setAge(23);
        redisTemplate.opsForHash().put("hash:user", userVo.hashCode() + "", userVo);
        System.out.println(redisTemplate.opsForHash().get("hash:user", userVo.hashCode() + ""));
    }

    @Test
    public void HashOperationsPutAll() throws Exception {
        UserVo userVo = new UserVo();
        userVo.setId("111111111111111111");
        userVo.setAddress("北京");
        userVo.setName("jantent");
        userVo.setAge(23);
        Map<String, Object> map = BeanUtil.beanToMap(userVo, false, true);
        redisTemplate.opsForHash().putAll(RedisKeys.getSecurityUserPlatformKey(userVo.getId()), map);
        Map<Object, Object> entries = redisTemplate.opsForHash().entries(RedisKeys.getSecurityUserPlatformKey(userVo.getId()));
        UserVo userDetail = BeanUtil.mapToBean(map, UserVo.class, true);
        System.out.println(redisTemplate.opsForHash().get(RedisKeys.getSecurityUserPlatformKey(userVo.getId()), userVo.hashCode() + ""));
    }

    @Test
    public void ListOperations() throws Exception {
        UserVo userVo = new UserVo();
        userVo.setAddress("北京");
        userVo.setName("jantent");
        userVo.setAge(23);
//        listOperations.leftPush("list:user",userVo);
//        System.out.println(listOperations.leftPop("list:user"));
        // pop之后 值会消失
        System.out.println(redisTemplate.opsForList().leftPop("list:user"));
    }

    @Autowired
    private UserDetailRedisUtils userDetailRedisUtils;

    @Test
    public void test() {
        userDetailRedisUtils.updateSecurityStatus("111111111111",
                SecurityStatusEnum.AUTH_PERMISSION_CHANGED,
                TerminalClientEnum.MERCHANT_CLIENT);
    }

}