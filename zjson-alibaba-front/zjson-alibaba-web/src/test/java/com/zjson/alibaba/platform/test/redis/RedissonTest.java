package com.zjson.alibaba.platform.test.redis;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.*;
import org.redisson.api.condition.Conditions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedissonTest {

    @Resource
    private RedissonClient redisson;


    @Test
    public void test1() throws Exception {
        RBucket<UserVo> bucket = redisson.getBucket("anyObject");
        bucket.set(new UserVo("1", "1", "1", 11));
        UserVo obj = bucket.get();
        log.debug(obj.toString());

        bucket.set(new UserVo("2", "2", "2", 22));
        UserVo userVo = bucket.get();
        log.debug(userVo.toString());

        // bucket.trySet(new AnyObject(3));
        // bucket.compareAndSet(new AnyObject(4), new AnyObject(5));
        // bucket.getAndSet(new AnyObject(6));
    }

    @Test
    public void test2() throws Exception {
        RGeo<String> geo = redisson.getGeo("test");
        geo.add(new GeoEntry(13.361389, 38.115556, "Palermo"),
                new GeoEntry(15.087269, 37.502669, "Catania"));
        geo.addAsync(37.618423, 55.751244, "Moscow");

        Double distance = geo.dist("Palermo", "Catania", GeoUnit.METERS);
        geo.hashAsync("Palermo", "Catania");
        Map<String, GeoPosition> positions = geo.pos("test2", "Palermo", "test3", "Catania", "test1");
        List<String> cities = geo.radius(15, 37, 200, GeoUnit.KILOMETERS);
        Map<String, GeoPosition> citiesWithPositions = geo.radiusWithPosition(15, 37, 200, GeoUnit.KILOMETERS);

    }

    @Test
    public void test3() {
        RBloomFilter<SomeObject> bloomFilter = redisson.getBloomFilter("sample");
        // 初始化布隆过滤器，预计统计元素数量为55000000，期望误差率为0.03
        boolean b = bloomFilter.tryInit(55000000L, 0.03);
        System.out.println(b);
        boolean add = bloomFilter.add(new SomeObject("field1Value", "field2Value"));
        System.out.println(add);
        boolean add1 = bloomFilter.add(new SomeObject("field5Value", "field8Value"));
        System.out.println(add1);
        boolean contains = bloomFilter.contains(new SomeObject("field1Value", "field8Value"));
        System.out.println(contains);

    }

    @Test
    public void test4() {
        RMap<String, SomeObject> map = redisson.getMap("anyMap");
        SomeObject prevObject = map.put("123", new SomeObject());
        SomeObject currentObject = map.putIfAbsent("323", new SomeObject());
        SomeObject obj = map.remove("123");

        map.fastPut("321", new SomeObject());
        map.fastRemove("321");

        map.fastRemoveAsync("321");
    }

    @Test
    public void test5() {
        RLiveObjectService service = redisson.getLiveObjectService();
        MyLiveObject myObject = new MyLiveObject();
        myObject.setId("1");
        // 将myObject对象当前的状态持久化到Redis里并与之保持同步。
        myObject = service.persist(myObject);

        // 抛弃myObject对象当前的状态，并与Redis里的数据建立连接并保持同步。
        myObject = service.attach(new MyLiveObject("1"));

        MyLiveObject myObject1 = new MyLiveObject();
        myObject1.setId("1");
        // 将myObject对象当前的状态与Redis里的数据合并之后与之保持同步。
        myObject = service.merge(myObject1);
        myObject.setValue("somevalue");

        // 通过ID获取分布式实时对象
        MyLiveObject myObject2 = service.get(MyLiveObject.class, "1");

        // 通过索引查找分布式实时对象
        Collection<MyLiveObject> myObjects1 = service.find(MyLiveObject.class, Conditions.in("value", "somevalue", "somevalue2"));

        Collection<MyLiveObject> myObjects2 = service.find(MyLiveObject.class, Conditions.and(Conditions.in("value", "somevalue", "somevalue2"), Conditions.eq("secondfield", "test")));

    }

    @Test
    public void test6() {
        RLock lock = redisson.getLock("anyLock");
        // 最常见的使用方法
        lock.lock();
        lock.lock();
        long timeMillis = System.currentTimeMillis();
        // while (true) {
        //     long currentTimeMillis = System.currentTimeMillis();
        //     System.out.println(currentTimeMillis);
        //     if (currentTimeMillis - timeMillis > 60000L){
        //         System.out.println("breakbreakbreakbreakbreakbreakbreakbreakbreakbreakbreakbreakbreakbreakbreak");
        //         break;
        //     }
        // }

        lock.unlock();
    }

    @Test
    public void test7() throws InterruptedException {
        RLock fairLock = redisson.getFairLock("anyLock");
        // 最常见的使用方法
        fairLock.lock();
        fairLock.lock();
        long timeMillis = System.currentTimeMillis();
        while (true) {
            long currentTimeMillis = System.currentTimeMillis();
            System.out.println(currentTimeMillis);
            if (currentTimeMillis - timeMillis > 30000L) {
                System.out.println("breakbreakbreakbreakbreakbreakbreakbreakbreakbreakbreakbreakbreakbreakbreak");
                break;
            }
        }
        fairLock.unlock();
        RReadWriteLock rwlock = redisson.getReadWriteLock("anyRWLock");
        // 最常见的使用方法
        rwlock.readLock().lock();
        // 或
        rwlock.writeLock().lock();
    }

    @Test
    public void test8() {
        RSemaphore semaphore = redisson.getSemaphore("semaphore");
        semaphore.addPermits(10);
        //模拟100辆车进入停车场
        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(() -> {
                try {
                    System.out.println("====" + Thread.currentThread().getName() + "来到停车场");
                    if (semaphore.availablePermits() == 0) {
                        System.out.println("车位不足，请耐心等待");
                    }
                    semaphore.acquire();//获取令牌尝试进入停车场
                    System.out.println(Thread.currentThread().getName() + "成功进入停车场");
                    Thread.sleep(new Random().nextInt(10000));//模拟车辆在停车场停留的时间
                    System.out.println(Thread.currentThread().getName() + "驶出停车场");
                    semaphore.release();//释放令牌，腾出停车场车位
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, i + "号车");
            thread.start();
        }

    }

}