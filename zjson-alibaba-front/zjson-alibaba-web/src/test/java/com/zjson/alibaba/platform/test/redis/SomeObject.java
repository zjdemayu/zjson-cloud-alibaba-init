package com.zjson.alibaba.platform.test.redis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 测试实体类
 *
 * @author zengzhi
 * @version 1.0
 * @since 2021/6/7 14:29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SomeObject implements Serializable {

    private static final long serialVersionUID = 4738913279626696744L;

    private String value1;

    private String value2;

}
