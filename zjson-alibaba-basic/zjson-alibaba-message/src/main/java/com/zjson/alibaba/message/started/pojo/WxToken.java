package com.zjson.alibaba.message.started.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yaozou
 * @description: 微信token
 * @date 2019-11-01 16:01
 * @since 1.0.0
 */
@Data
public class WxToken implements Serializable {
    private String token;
    private Long   outTime;
}
