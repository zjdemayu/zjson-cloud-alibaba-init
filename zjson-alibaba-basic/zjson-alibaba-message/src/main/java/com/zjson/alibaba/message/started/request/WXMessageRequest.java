package com.zjson.alibaba.message.started.request;


import lombok.Data;

import java.util.List;

/**
 * 微信公众号 普通消息模型
 * @author pengh
 *2018-08-18
 */
@Data
public class WXMessageRequest  extends SendBaseRequest{
	private String id;

	   /**
		 * 接收用户openID
		 */
       private String[] touser;
       /** 群发
        * 是否发送所有
        */
       private boolean isToAll;
       /**
        * 群发到的标签的tag_id，参见用户管理中用户分组接口，若is_to_all值为true，可不填写tag_id
        */
       private String tagId;
       /**
        * 内容描述
        */
       private String description;
       /**
        * 内容标题
        */
       private String title;
       /**
        * 素材ID
        */
       private String mediaId;
       /**
        */
       private String cardId;
       /**
        * 消息内容 非必填  msgtype==text 时必填
        */
       private String content;
       /**
        * 消息类型
        */
       private String msgtype;
       /**
        * 图文消息内容  发送图文消息 穿度 size必须==1 用户也必须是1
        */
       private List<ArticlesRequest> articles;
       private String msgCode;

       

       
    
}
