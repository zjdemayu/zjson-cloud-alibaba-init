package com.zjson.alibaba.message.started.request;

import lombok.Data;

import java.util.Map;

/**
 * 极光推送请求对象
 * @author wh
 * 2019-08-17
 *
 */
@Data
public class JGPushRequest extends SendBaseRequest {
	private String id;

	/**
	 * 推送方式 alias 别名推送
	 * tag  标签推送
	 */
	private String type;
	private String msgTyte;//msg  消息/notice 通知
	/**
	 * 应用信息
	 */
	private Map<String,String> tarappMap;
	/**
	 * 推送类型对应的值 
	 */
    private String typeValue;
    /**
     * 推送内容
     */
    private String alert;
    /**
     * 推送标题
     */
    private String title;
    /**
     * 扩展字段
     */
    private Map<String, String> extras;
}
