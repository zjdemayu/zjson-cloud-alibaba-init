package com.zjson.alibaba.message.started.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yaozou
 * @description: 调用结果
 * @date 2019-10-26 16:22
 * @since 1.0.0
 */
@Data
public class Result implements Serializable {
    private String errorCode;
    private String message;
    private String messageCode;
}
