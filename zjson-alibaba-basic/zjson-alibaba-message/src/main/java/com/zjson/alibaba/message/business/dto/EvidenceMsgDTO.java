package com.zjson.alibaba.message.business.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 发券实体
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/6/5 13:39
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EvidenceMsgDTO {

	/**
	 * 发行商户
	 */
	private String issuedMerchantName;

	/**
	 * 手机号
	 */
	private String phone;

	/**
	 * 短信编码
	 */
	private String sendCode;




}
