package com.zjson.alibaba.message.started.request;



import lombok.Data;


/**
 * 模板 请求对象
 * @author pengh
 *2019-09-02
 */
@Data
public class McAppTemplateRequest {

	private Long id;
	 /**
	  * 沐模板ID或code 对应第三方平台
	  */
	private String templateId;

	/** 自定义code */
	private String code;
	
	/**
	 * 模板标题
	 */
	private String title;
	/**
	 * 对应应用ID
	 */
	private Long maAppId;

	/**
	 * 备注
	 */
	private String note;

	/**
	 * 模板内容 
	 */
	private String content;

}
