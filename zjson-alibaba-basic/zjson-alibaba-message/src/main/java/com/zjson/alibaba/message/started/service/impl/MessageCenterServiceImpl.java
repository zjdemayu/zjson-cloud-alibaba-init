package com.zjson.alibaba.message.started.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zjson.alibaba.message.started.config.TokenConfig;
import com.zjson.alibaba.message.started.properties.MessageProperties;
import com.zjson.alibaba.message.started.request.McAppRequest;
import com.zjson.alibaba.message.started.request.McAppTemplateRequest;
import com.zjson.alibaba.message.started.request.ProjectRequest;
import com.zjson.alibaba.message.started.service.BaseService;
import com.zjson.alibaba.message.started.service.IMessageCenterService;
import com.zjson.alibaba.message.started.util.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author yaozou
 * @description:
 * @date 2019-11-05 16:59
 * @since 1.0.0
 */
@Service("messageCenterService")
@Slf4j
public class MessageCenterServiceImpl extends BaseService implements IMessageCenterService {

    @Autowired
    private MessageProperties properties;

    @Autowired
    private TokenConfig tokenConfig;

    @Override
    public JSONObject addProject(ProjectRequest request) throws IOException {
        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(), properties.getMsgProjectUrl(),
                JSONObject.toJSONString(request), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        return toJsonObject(content);
    }

    @Override
    public JSONObject addApp(McAppRequest request) throws IOException {
        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(), properties.getMsgAddAppUrl(),
                JSONObject.toJSONString(request), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        return toJsonObject(content);
    }

    @Override
    public JSONObject updateApp(McAppRequest request) throws IOException {
        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                properties.getMsgUpdateAppUrl(), JSONObject.toJSONString(request), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        return toJsonObject(content);
    }

    @Override
    public JSONObject deleteApp(Long[] ids) throws IOException {
        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                properties.getMsgDelAppUrl(), JSONObject.toJSONString(ids), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        return toJsonObject(content);
    }

    @Override
    public JSONObject addTemplate(McAppTemplateRequest request) throws IOException {
        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                properties.getMsgAddTempUrl(), JSONObject.toJSONString(request), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        return toJsonObject(content);
    }

    @Override
    public JSONObject updateTemplate(McAppTemplateRequest request) throws IOException {
        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                properties.getMsgUpdateTempUrl(), JSONObject.toJSONString(request), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        return toJsonObject(content);
    }

    @Override
    public JSONObject delTemplate(Long[] ids) throws IOException {
        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                properties.getMsgDelTempUrl(), JSONObject.toJSONString(ids), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        return toJsonObject(content);
    }

    private JSONObject toJsonObject(String content) {
        return JSON.parseObject(content);
    }
}
