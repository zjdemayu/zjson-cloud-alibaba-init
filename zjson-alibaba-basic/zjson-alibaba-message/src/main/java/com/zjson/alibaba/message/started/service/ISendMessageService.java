package com.zjson.alibaba.message.started.service;

import com.zjson.alibaba.message.started.pojo.*;

import java.io.IOException;

/**
 * @author yaozou
 * @description:
 * @date 2019-10-24 14:24
 * @since 1.0.0
 */
public interface ISendMessageService {

    /**
     * 从阿里渠道发送短信消息
     *
     * @param shortMessage 短信信息
     * @return
     */
    Result sendAliShortMsg(ShortMessage shortMessage) throws Exception;

    /**
     * 从腾讯渠道发送短信消息
     *
     * @param shortMessage
     * @return
     * @throws Exception
     */
    Result sendTencentShortMsg(ShortMessage shortMessage) throws Exception;

    /**
     * 获取微信普通access_token
     *
     * @param appId
     * @return
     * @throws Exception
     */
    WxToken getWxToken(String appId) throws Exception;

    /**
     * 发送邮件
     *
     * @param mailMessage
     * @return
     * @throws Exception
     */
    Result sendWxMail(MailMessage mailMessage) throws Exception;

    /**
     * 发送微信公共号普通消息
     *
     * @param message
     * @return
     * @throws IOException
     */
    Result sendWxPublicPlainMsg(WxPublicPlainMessage message) throws IOException;

    /**
     * 发送微信公共号模板消息
     *
     * @param message
     * @return
     * @throws IOException
     */
    Result sendWxPublicTemplateMsg(WxPublicTemplateMessage message) throws IOException;

    /**
     * 发送微信小程序订阅消息
     *
     * @param message
     * @return
     * @throws IOException
     */
    Result sendWxMiniMsg(WxMiniMessage message) throws IOException;

    /**
     * 发送mqtt消息
     *
     * @param message
     * @return
     * @throws IOException
     */
    Result sendMqtt(MqttMessage message) throws IOException;

    /**
     * 批量发送mqtt消息（类似广播消息）
     *
     * @param message
     * @return
     */
    Result sendBatchMqtt(MqttBatchMessage message) throws IOException;

    /**
     * 查询短信发送结果
     *
     * @param outId 外部扩展字段（可为相关业务字段）
     * @return
     */
    Result queryShortMessageSendResult(String outId) throws IOException;
}
