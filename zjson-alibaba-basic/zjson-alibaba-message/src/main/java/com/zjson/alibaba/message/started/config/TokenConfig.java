package com.zjson.alibaba.message.started.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zjson.alibaba.message.started.properties.MessageProperties;
import com.zjson.alibaba.message.started.redis.MessageRedis;
import com.zjson.alibaba.message.started.request.LoginRequest;
import com.zjson.alibaba.message.started.util.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;

/**
 * @author yaozou
 * @description:
 * @date 2019-11-06 15:25
 * @since 1.0.0
 */
@Slf4j
@Service
public class TokenConfig {
    protected static final String CONTENT_TYPE = "application/json";
    @Autowired
    protected MessageProperties messageProperties;
    @Autowired
    private MessageRedis messageRedis;


    /**
     * 会超时，超时时间可配（不定）
     *
     * @return
     * @throws IOException
     */
    public String getToken() throws IOException {
        String token = messageRedis.getToken();
        if (!StringUtils.isEmpty(token)) {
            return token;
        }
        //设置参数
        LoginRequest request = new LoginRequest();
        request.setPassword(messageProperties.getPassword());
        request.setUserName(messageProperties.getUsername());
        //发送请求
        CloseableHttpResponse respose = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(), messageProperties.getLoginUrl(), JSONObject.toJSONString(request), CONTENT_TYPE, null);
        //解析返回值
        String content = HttpUtil.toString(respose);
        //输出返回格式
        log.debug("message login response:{}", content);
        JSONObject json = JSON.parseObject(content);
        String newToken = json.get("token").toString();
        System.out.println("----------token:" + newToken);
        messageRedis.setToken(newToken);
        return newToken;
    }

}
