package com.zjson.alibaba.message.started.pojo;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

/**
 * @author yaozou
 * @description:发送微信公众号模板消息
 * @date 2019-10-24 15:57
 * @since 1.0.0
 */
@Data
@Builder
public class WxPublicTemplateMessage extends BaseMessage {
    private String code;
    private String openId;
    private String url;

    /**
     * 模板参数键值对
     **/
    private Map<String, Object> data;
}
