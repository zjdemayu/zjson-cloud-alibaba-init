package com.zjson.alibaba.message.started.request;


import com.zjson.alibaba.message.started.menu.MsgTypeEnum;
import lombok.Data;

/**
 * 请求对象封装类
 * @author pengh
 * 2019-08-17
 */
@Data
public class SendMsgRequest<E> {
	//SHORTMESSAGE(短信),MAIL(邮件),WXGZH(微信公众号)，WXGZHTEL(微信公众号模板)，WXMINIAPP(微信小程序)，PUSHAPP(APP推送消息)
      /**
       * 消息类型
       */
      private MsgTypeEnum msgType;
      /**
       * 第三方接口应用ID
       */
      private String appId;

      /**
       * 请求参数
       */
      private E data;
      /**
       * 接口调用时间
       */
      private Long callTime;
      /**
       * 调用者ip
       */
      private String ip;

}
