package com.zjson.alibaba.message.started.request;

import lombok.Data;

import java.util.List;

/**
 * 短信发送 模型
 * 
 * @author pengh
 * 2018-08-18
 */
@Data
public class ShortMessageRequest  extends SendBaseRequest {
	private String id;

	/**
	 * 手机号 多个用逗号分割
	 */
	private String phoneNumbers;
	/**
	 * 参数集合
	 */
    private List<ParamRequest> params;
	// 可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
	private String OutId;
	

}
