package com.zjson.alibaba.message.business.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.LinkedHashMap;

/**
 * @author luohuan
 * @date 2020/4/17
 **/
@Data
@ApiModel("动态短信参数DTO")
public class VariableMsgDTO {

    @ApiModelProperty("短信模板code码")
    private String code;

    @ApiModelProperty("手机号（多个手机号以英文逗号分隔）")
    private String phone;

    @ApiModelProperty("短信模板参数")
    private LinkedHashMap<String, String> paramsMap = new LinkedHashMap<>(0);
}
