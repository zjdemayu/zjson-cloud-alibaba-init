package com.zjson.alibaba.message.started.menu;
/**
 * 消息接口类型枚举
 * @author pengh
 *2019-09-02
 */
public  enum MsgTypeEnum {

	
	//SHORTMESSAGE(短信),MAIL(邮件),WXGZH(微信公众号)，WXGZHTEL(微信公众号模板)，WXMINIAPP(微信小程序)，PUSHAPP(APP推送消息)
	//阿里短信消息
	ALI_SHORT_MESSAGE("ALISHORTMESSAGE","阿里短信消息",MsgSonTypeEnum.SM,"shortMessageMsgSendDomainServiceImpl","shortMessageConvertor","aliShortMessageUtil"),
	//腾讯短信消息
	TENCENT_SHORT_MESSAGE("TENCENTSHORTMESSAGE","腾讯短信消息",MsgSonTypeEnum.SM,"shortMessageMsgSendDomainServiceImpl","shortMessageConvertor","tencentShortMessageUtil"),
	//邮件消息
	MAIL("MAIL","邮件消息",MsgSonTypeEnum.OTHER,"mailMsgSendDomainServiceImpl","mailConvertor"),
	//微信公众号消息
	WXGZH("WXGZH","微信公众号消息",MsgSonTypeEnum.OTHER,"wxMsgSendDomainServiceImpl","wxMessageConvertor"),
	//微信公众号模板消息
	WXGZHTEL("WXGZHTEL","微信公众号模板消息",MsgSonTypeEnum.OTHER,"wxTemplatMsgSendDomainServiceImpl","wxTemplateConvertor"),
	//微信小程序
	WXMINIAPP("WXMINIAPP","微信小程序消息",MsgSonTypeEnum.OTHER,"wxMiniMseSendDomainServiceImpl","wxMiniAppConvertor"),
	//微信小程序订阅消息
	WXMINIAPPSUBMSG("WXMINIAPPSUBMSG","微信小程序订阅消息",MsgSonTypeEnum.OTHER,"wxMiniSubMseSendDomainServiceImpl","wxMiniAppConvertor"),
	//极光APP推送
	JG_PUSHAPP("JGPUSHAPP","极光APP推送",MsgSonTypeEnum.OTHER,"",""),
	//友盟APP推送
	YM_PUSHAPP("YMPUSHAPP","友盟APP推送",MsgSonTypeEnum.OTHER,"",""),
	//阿里APP推送
	ALI_PUSHAPP("ALIPUSHAPP","阿里APP推送",MsgSonTypeEnum.OTHER,"aliAppMsgSendDomainServiceImpl","aliPushAppConvertor"),
	//腾信APP推送
	TENCENT_PUSHAPP("TENCENTPUSHAPP","腾信APP推送",MsgSonTypeEnum.OTHER,"","")
	,MQTT("MQTT","MQTT消息推送",MsgSonTypeEnum.OTHER,"mqttMsgSendDomainServiceImpl","mqttAliConvertor")
	,MQTTBATCH("MQTTBATCH","MQTT消息推送BATCH",MsgSonTypeEnum.OTHER,"mqttBatchMsgSendDomainServiceImpl","mqttBatchAliConvertor");
	private String code;//编码
	private String desc;//描述
	private MsgSonTypeEnum type;//类型 SM 短信 OTHER 其他
	private String serverName;//服务名称
	private String convertorName;//转换工具名称
	//处理多通道
	private String sonServerName;//二级服务名
	MsgTypeEnum(String code,String desc,MsgSonTypeEnum type,String serverName,String convertorName){
		this.code=code;
		this.desc=desc;
		this.type=type;
		this.serverName=serverName;
		this.convertorName=convertorName;
	}
	MsgTypeEnum(String code,String desc,MsgSonTypeEnum type,String serverName,String convertorName,String sonServerName){
		this.code=code;
		this.desc=desc;
		this.type=type;
		this.serverName=serverName;
		this.convertorName=convertorName;
		this.sonServerName=sonServerName;
	}

	public MsgSonTypeEnum getType() {
		return type;
	}
	
	public String getSonServerName() {
		return sonServerName;
	}
	public String getConvertorName() {
		return convertorName;
	}
	public String getServerName() {
		return serverName;
	}
	public String getCode() {
		return code;
	}
	public String getDesc() {
		return desc;
	}
	


}
