package com.zjson.alibaba.message.business.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import lombok.Getter;

import java.util.Objects;

/**
 * 验证码类型枚举定义
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/8/28 16:03
 */
@Getter
public enum VerifyCodeTypeEnum {

    /**
     * 兑换凭证
     */
    CONSUME_EVIDENCE(0, "兑换凭证", "sys:message:consume_evidence:");

    @JsonValue
    private final Integer code;

    private final String desc;

    private final String keyPrefix;

    VerifyCodeTypeEnum(int code, String desc, String keyPrefix) {
        this.code = code;
        this.desc = desc;
        this.keyPrefix = keyPrefix;
    }

    public static VerifyCodeTypeEnum parse(Integer code) {
        if (Objects.isNull(code)) {
            return null;
        }
        for (VerifyCodeTypeEnum type : values()) {
            if (type.code.equals(code)) {
                return type;
            }
        }
        throw new CustomException("验证码类型code=" + code);
    }
}
