package com.zjson.alibaba.message.started.menu;
/**
 * 时间格式工具
 * @author pengh
 *2019-09-02
 */
public enum DateFormatEnum {
	YYYY("yyyy"),
	YYYYMM("yyyy-MM"),
	YYYYMMDD("yyyy-MM-dd"),
	YYYYMMDDHH("yyyy-MM-dd HH"),
	YYYYMMDDHHMM("yyyy-MM-dd HH:mm"),
	HHMMSS("HH:mm:ss"),
	YYYYMMDDHHMMSS("yyyy-MM-dd HH:mm:ss");

    private String paramString;
    DateFormatEnum(String paramString){
    	this.paramString=paramString;
    }
    
	public String getParamString() {
		return paramString;
	}

	public void setParamString(String paramString) {
		this.paramString = paramString;
	}
    
}
