package com.zjson.alibaba.message.started.request;

import lombok.Data;

@Data
public class ProjectRequest {
	//项目名称
    private String projectName;

	// 备注
	private String note;
    
}
