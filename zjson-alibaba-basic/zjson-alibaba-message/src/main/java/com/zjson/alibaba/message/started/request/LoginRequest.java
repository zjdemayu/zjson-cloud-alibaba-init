package com.zjson.alibaba.message.started.request;

import lombok.Data;

/**
 * 登录请求对象
 * @author pengh
 * 2019-08-17
 */
@Data
public class LoginRequest {
	/**
	 * 用户名
	 */
      private String userName;
      /**
       * 密码
       */
      private String password;
}
