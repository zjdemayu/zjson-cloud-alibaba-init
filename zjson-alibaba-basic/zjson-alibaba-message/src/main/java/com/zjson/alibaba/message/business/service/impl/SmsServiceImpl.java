package com.zjson.alibaba.message.business.service.impl;

import com.google.common.collect.Maps;
import com.zjson.alibaba.message.business.dto.EvidenceMsgDTO;
import com.zjson.alibaba.message.business.dto.VerifyCodeDTO;
import com.zjson.alibaba.message.started.pojo.Result;
import com.zjson.alibaba.message.started.pojo.ShortMessage;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.properties.RightsSmsProperties;
import com.zjson.alibaba.commons.tools.properties.SmsVerifyCode;
import com.zjson.alibaba.commons.tools.utils.JsonUtils;
import com.zjson.alibaba.message.business.service.SmsService;
import com.zjson.alibaba.message.business.utils.MsgRedisUtil;
import com.zjson.alibaba.message.started.request.ParamRequest;
import com.zjson.alibaba.message.started.service.ISendMessageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 短信相关ServiceImpl
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/8/28 15:54
 */
@Slf4j
@Service("smsService")
public class SmsServiceImpl implements SmsService {

    @Resource
    private ISendMessageService sendMessageService;

    @Resource
    private MsgRedisUtil msgRedisUtil;

    /**
     * OK
     */
    private static final String OK = "OK";

    @Resource
    private RightsSmsProperties smsProperties;

    @Override
    public String sendVerifyCode(VerifyCodeDTO verifyCodeDTO) {
        SmsVerifyCode smsVerifyCode = smsProperties.getSmsVerifyCode();
        String code = smsVerifyCode.getDebugSwitch() ?
                smsVerifyCode.getDefaultCode() : RandomStringUtils.randomNumeric(6);
        // 放入redis中
        msgRedisUtil.setVerifyCode(verifyCodeDTO, code, smsVerifyCode.getDefaultExpire());
        if (smsVerifyCode.getDebugSwitch()) {
            log.warn("短信接口调试模式已打开，跳过第三方短信验证码发送步骤");
        } else {
            Map<String, String> map = Maps.newLinkedHashMap();
            map.put("code", code);
            this.send(verifyCodeDTO.getPhone(), map, smsVerifyCode.getRightsVerifyCode());
        }
        return code;
    }

    @Override
    public Boolean validateVerifyCode(VerifyCodeDTO verifyCodeDTO) {
        if (msgRedisUtil.validateVerifyCode(verifyCodeDTO)) {
            return true;
        }
        throw new CustomException("验证码错误！");
    }

    @Override
    public void sendEvidences(String phone, List<EvidenceMsgDTO> params) {
        // 对象集合转化为List<Map>
        List<Map<String, Object>> listMap = JsonUtils.listObjectToListMap(params);
        // 将List<Map>转化为消息集合
        List<ShortMessage> shortMessages = listMap.stream().map(msgMap -> {
            List<ParamRequest> paramRequests = new ArrayList<>();
            msgMap.forEach((k, v) ->
                    paramRequests.add(ParamRequest.builder()
                            .key(k)
                            .value(String.valueOf(v))
                            .build())
            );
            ShortMessage shortMessage = ShortMessage.builder()
                    .code(msgMap.get("sendCode").toString())
                    .params(paramRequests)
                    .phone(phone)
                    .build();
            shortMessage.setIp("127.0.0.1");
            return shortMessage;
        }).collect(Collectors.toList());
        // 发送短信
        shortMessages.forEach(message -> {
            Result result;
            try {
                result = sendMessageService.sendAliShortMsg(message);
            } catch (Exception e) {
                log.error("send evidence message to msg center error.", e);
                throw new CustomException(e.getMessage());
            }

            // 发送结果处理
            if (!OK.equalsIgnoreCase(result.getErrorCode())) {
                log.error("send evidence message error:{},{},{}", result.getErrorCode(), result.getMessage(), result.getMessageCode());
                throw new CustomException(result.getMessage());
            }
        });


    }


    public void send(String mobile, Map<String, String> map, String code) {
        log.info("向用户[{}]发送[{}]消息，内容关键字：{}", mobile, code, map);
        // 请求消息中心发送短信  list 必须按模板顺序存放
        List<ParamRequest> params = new ArrayList<>();
        map.forEach((k, v) -> params.add(ParamRequest.builder()
                .key(k)
                .value(v)
                .build()));
        ShortMessage shortMessage = ShortMessage.builder()
                .code(code)
                .params(params)
                .phone(mobile)
                .build();
        shortMessage.setIp("127.0.0.1");


        Result result;
        try {
            result = sendMessageService.sendAliShortMsg(shortMessage);
        } catch (Exception e) {
            log.error("send sms message to msg center error.", e);
            throw new CustomException(e.getMessage());
        }

        // 发送结果处理
        if (!OK.equalsIgnoreCase(result.getErrorCode())) {
            log.error("send sms message error:{},{},{}", result.getErrorCode(), result.getMessage(), result.getMessageCode());
            throw new CustomException(result.getMessage());
        }

    }


}
