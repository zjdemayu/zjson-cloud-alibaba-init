package com.zjson.alibaba.message.started.request;


import com.zjson.alibaba.message.started.menu.AppPushTypeEnum;
import lombok.Data;

import java.util.Map;

/**
 * 阿里云app推送请求对象
 * @author wh
 * 2019-08-17
 */
@Data
public class AliPushAppRequset extends SendBaseRequest {

	private String target;//推送目标: DEVICE:推送给设备; ACCOUNT:推送给指定帐号,TAG:推送给自定义标签; ALL: 推送给全部
	private String targetValue;//根据Target来设定，如Target=DEVICE, 则对应的值为 设备id1,设备id2. 多个值使用逗号分隔.(帐号与设备有一次最多100个的限制)
	private String pushType;// 消息类型 MESSAGE NOTICE
	private AppPushTypeEnum deviceType;// 设备类型 ANDROID iOS ALL.
	private String title;//标题
	private String body;//内容
	private Map<String,String> extParameters;//扩展字段
	
	
    private Boolean iosRemind=true;
    private Integer iosBadge; // iOS应用图标右上角角标
    private String iosMusic; // iOS通知声音
    private String iosSubtitle;//iOS10通知副标题的内容
    private String iosNotificationCategory;//指定iOS10通知Category
    private Boolean iosMutableContent=true;//是否允许扩展iOS通知内容
    private String iostIOSApnsEnv;//iOS的通知是通过APNs中心来发送的，需要填写对应的环境信息。"DEV" : 表示开发环境 "PRODUCT" : 表示生产环境
    private String iosRemindBody;//iOS消息转通知时使用的iOS通知内容，仅当iOSApnsEnv=PRODUCT && iOSRemind为true时有效
    private String iosApnsEnv;//iOS的通知是通过APNs中心来发送的，需要填写对应的环境信息。"DEV" : 表示开发环境 "PRODUCT" : 表示生产环境
    
    
    
    private String androidNotifyType;//通知的提醒方式 "VIBRATE" : 震动 "SOUND" : 声音 "BOTH" : 声音和震动 NONE : 静音
    private Integer androidNotificationBarType;//通知栏自定义样式0-100
    private Integer androidNotificationBarPriority;//通知栏自定义样式0-100
    private String androidOpenType; //点击通知后动作 "APPLICATION" : 打开应用 "ACTIVITY" : 打开AndroidActivity "URL" : 打开URL "NONE" : 无跳转
    private String androidOpenUrl; //Android收到推送后打开对应的url,仅当AndroidOpenType="URL"有效
    private String androidActivity; // 设定通知打开的activity，仅当AndroidOpenType="Activity"有效
    private String androidMusic; // Android通知音乐
    private String androidPopupActivity;//设置该参数后启动辅助弹窗功能, 此处指定通知点击后跳转的Activity（辅助弹窗的前提条件：1. 集成第三方辅助通道；2. StoreOffline参数设为true）
    private String androidPopupTitle;//弹出标题
    private String androidPopupBody;//弹出内容	        
    private String androidNotificationChannel;//安卓系统8.0及以上版本，这里加入设置
    private Boolean storeOffline=true;

}
