package com.zjson.alibaba.message.started.pojo;

import lombok.Builder;
import lombok.Data;

/**
 * @author yaozou
 * @description:
 * @date 2019-10-24 18:00
 * @since 1.0.0
 */
@Data
@Builder
public class MqttMessage extends BaseMessage {
    private String clientId;
    /** 发送方ID */
    private String fromClientId;
    private String message;
    private Integer qos;
    private String topic;
    private Boolean retain;
}
