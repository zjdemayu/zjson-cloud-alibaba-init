package com.zjson.alibaba.message.started.request;

import lombok.Data;

/**
 * 附件file请求对象
 *
 * @author pengh
 * 2019-08-17
 */
@Data
public class MailFileRequest {
    /**
     * 附件地址
     */
    private String fileUrl;
    /**
     * 附件名称
     */
    private String fileName;


    public MailFileRequest() {
    }

    public MailFileRequest(String fileName, String fileUrl) {
        this.fileUrl = fileUrl;
        this.fileName = fileName;
    }
}
