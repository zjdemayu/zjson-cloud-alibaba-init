package com.zjson.alibaba.message.started.pojo;

import com.zjson.alibaba.message.started.request.MailFileRequest;
import lombok.Builder;
import lombok.Data;

/**
 * @author yaozou
 * @description: 电子邮件信息
 * @date 2019-10-24 15:30
 * @since 1.0.0
 */
@Data
@Builder
public class MailMessage extends BaseMessage {
    private String[] addresses;
    /**
     * 邮件密送接收者的地址
     */
    private String[] blindCarbonCopyAddress;
    /**
     * 邮件抄送接收者的地址
     */
    private String[] carbonCopyAddress;
    private String subject;
    private String content;
    private MailFileRequest[] files;
}
