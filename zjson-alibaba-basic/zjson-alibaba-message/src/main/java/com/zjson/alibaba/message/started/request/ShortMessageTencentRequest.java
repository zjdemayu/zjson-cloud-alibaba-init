package com.zjson.alibaba.message.started.request;




import lombok.Data;

/**
 * 腾讯云短信发送 模型
 * @author pengh
 *2018-08-18
 */
@Data
public class ShortMessageTencentRequest  extends SendBaseRequest {
	private String id;
	
	/**
	 * 手机号 多个用逗号分割
	 */
	private String phoneNumbers;

	/**
	 * 模板参数
	 */
	private String[] params;
	/**
	 * 短信内容
	 */
	private String content;
	
	

}
