package com.zjson.alibaba.message.started.service;

import com.alibaba.fastjson.JSONObject;
import com.zjson.alibaba.message.started.config.TokenConfig;
import com.zjson.alibaba.message.started.menu.MQTTAgreementEnum;
import com.zjson.alibaba.message.started.menu.MsgTypeEnum;
import com.zjson.alibaba.message.started.properties.MessageProperties;
import com.zjson.alibaba.message.started.util.HttpUtil;
import com.zjson.alibaba.message.started.pojo.*;
import com.zjson.alibaba.message.started.request.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yaozou
 * @description: 基础服务
 * @date 2019-10-24 15:15
 * @since 1.0.0
 */
@Slf4j
public class BaseService {
    protected static final String CONTENT_TYPE = "application/json";
    @Autowired
    protected MessageProperties messageProperties;

    @Autowired
    private TokenConfig tokenConfig;

    /**
     * 发送短信
     *
     * @param shortMessage 短信信息
     * @param msgTypeEnum  消息类型
     * @return
     * @throws IOException
     */
    protected Result sendShort(ShortMessage shortMessage, MsgTypeEnum msgTypeEnum) throws IOException {
        SendMsgRequest<ShortMessageRequest> smr = new SendMsgRequest<ShortMessageRequest>();
        //对应 后台APPID
        smr.setAppId(shortMessage.getAppId());
        //APPID对应的平台
        smr.setMsgType(msgTypeEnum);
        smr.setCallTime(System.currentTimeMillis());
        smr.setIp(shortMessage.getIp());

        ShortMessageRequest request = new ShortMessageRequest();
        //异步消息是 可用 发送成功后回调
        request.setCallBackUrl(messageProperties.getCallBackUrl());
        //模板code
        request.setCode(shortMessage.getCode());
        //参数 list 必须按模板顺序存放
        request.setParams(shortMessage.getParams());
        request.setPhoneNumbers(shortMessage.getPhone());
        request.setOutId(shortMessage.getOutId());
        smr.setData(request);
        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                messageProperties.getSendMsgUrl(), JSONObject.toJSONString(smr), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        log.info("sendShort request:{},response:{}", JSONObject.toJSONString(smr), content);
        return toResult(content);
    }

    public WxToken getWxToken(String appId) throws IOException {
        WXTokenRequest request = new WXTokenRequest();
        //管理端创建的 微信应用 ID
        request.setAppId(appId);

        CloseableHttpResponse respose = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                messageProperties.getWxTokenUrl(), JSONObject.toJSONString(request), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(respose);
	    /*{
		"outTime": 1571886470299,
		"token": "26_Zfy9-CfdNM-2v1PuIQ4NWNAYtb52DkYY-uxqzntb8sp8rV7vcTie94Jjc1He5Bt-sEmophj5yhUfBJXZpMCxWNz6f2XDZmb5viKR-LnOC78n3sNiCsGx8TBCSsn1PyYQ9x_2w0hrW6lV83HePAMeAAALWI"
		}*/
        log.info("getWxToken request:{},response:{}", JSONObject.toJSONString(request), content);
        return JSONObject.parseObject(content, WxToken.class);
    }

    /**
     * 发送微信公众号普通消息
     *
     * @return
     * @throws IOException
     */
    public Result sendWxPublicPlainMsg(WxPublicPlainMessage message) throws IOException {
        SendMsgRequest<WXMessageRequest> smr = new SendMsgRequest<WXMessageRequest>();
        //对应 后台APPID
        smr.setAppId(message.getAppId());
        //APPID对应的平台类型
        smr.setMsgType(MsgTypeEnum.WXGZH);
        smr.setCallTime(System.currentTimeMillis());
        smr.setIp(message.getIp());

        WXMessageRequest request = new WXMessageRequest();
        //异步消息是 可用 发送成功后回调
        request.setCallBackUrl(messageProperties.getCallBackUrl());
        //发送给所有人 true
        request.setToAll(message.getToAll());
        if (!message.getToAll()) {
            //发送给指定用户
            request.setTouser(message.getOpenIds());
        }
        request.setMsgtype(message.getMsgType());
        switch (message.getMsgType()) {
            case "image":
                request.setMediaId(message.getMediaId());
                break;
            case "voice":
                request.setMediaId(message.getMediaId());
                break;
            case "video":
                request.setMediaId(message.getMediaId());
                break;
            case "music":
                request.setMediaId(message.getMediaId());
                break;
            case "news":
                List<ArticlesRequest> articles = new ArrayList<>(1);
                articles.add(message.getArticlesRequest());
                request.setArticles(articles);
                break;
            case "mpnews":
                articles = new ArrayList<>(1);
                articles.add(message.getArticlesRequest());
                request.setArticles(articles);
                break;
            case "wxcard":
                request.setCardId(message.getCardId());
                break;
            case "miniprogrampage":
                request.setContent(message.getContent());
                break;
            default:
                //发送消息内容 msgType为text 此值必填
                request.setContent(message.getContent());
        }

        smr.setData(request);
        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                messageProperties.getSendMsgUrl(), JSONObject.toJSONString(smr), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        log.info("sendWxPublicPlainMsg request:{},response:{}", JSONObject.toJSONString(smr), content);
        return toResult(content);
    }

    /**
     * 发送微信公众号模板消息
     *
     * @return
     * @throws IOException
     */
    public Result sendWxPublicTemplateMsg(WxPublicTemplateMessage message) throws IOException {
        SendMsgRequest<JSONObject> smr = new SendMsgRequest<JSONObject>();
        //对应 后台APPID
        smr.setAppId(message.getAppId());
        //APPID对应的平台类型
        smr.setMsgType(MsgTypeEnum.WXGZHTEL);
        smr.setCallTime(System.currentTimeMillis());
        smr.setIp(message.getIp());

        WXTemplateMessageRequest request = new WXTemplateMessageRequest();
        //异步消息是 可用 发送成功后回调
        request.setCallBackUrl(messageProperties.getCallBackUrl());
        //模板code
        request.setCode(message.getCode());
        request.setTouser(message.getOpenId());
        if (!StringUtils.isEmpty(message.getUrl())) {
            request.setUrl(message.getUrl());
        }
        //设置模板参数
        JSONObject json = (JSONObject) JSONObject.toJSON(request);
        json.putAll(message.getData());
        smr.setData(json);
        System.out.println(JSONObject.toJSONString(smr));
        CloseableHttpResponse respose = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                messageProperties.getSendMsgUrl(), JSONObject.toJSONString(smr), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(respose);
        log.info("sendWxPublicTemplateMsg request:{},response:{}", JSONObject.toJSONString(smr), content);
        return toResult(content);
    }

    /**
     * 发送微信小程序模板消息
     *
     * @return
     * @throws IOException
     */
    public Result sendWxMiniMsg(WxMiniMessage message) throws IOException {
        SendMsgRequest<JSONObject> smr = new SendMsgRequest<JSONObject>();
        //对应 后台APPID
        smr.setAppId(message.getAppId());
        //APPID对应的平台类型
        smr.setMsgType(MsgTypeEnum.WXMINIAPPSUBMSG);
        smr.setCallTime(System.currentTimeMillis());
        smr.setIp(message.getIp());

        WXMIniAppMessageRequest request = new WXMIniAppMessageRequest();
        //异步消息是 可用 发送成功后回调
        request.setCallBackUrl(messageProperties.getCallBackUrl());
        //模板code
        request.setCode(message.getCode());
        request.setTouser(message.getOpenId());
        request.setPage(message.getPage());

        //必填项 没有值 设置为 空字符串
        request.setPage(messageProperties.getWxMiniDirectPageUrl());

        //设置模板参数
        JSONObject json = (JSONObject) JSONObject.toJSON(request);
        json.putAll(message.getData());
        smr.setData(json);

        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                messageProperties.getSendMsgUrl(), JSONObject.toJSONString(smr), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        log.info("sendWxMiniMsg request:{},response:{}", JSONObject.toJSONString(smr), content);
        return toResult(content);
    }

    /**
     * 发送邮件
     *
     * @param mailMessage 邮件信息
     * @return
     * @throws Exception
     */
    public Result sendWxMail(MailMessage mailMessage) throws Exception {
        SendMsgRequest<MailRequest> smr = new SendMsgRequest<MailRequest>();
        //对应 后台APPID
        smr.setAppId(mailMessage.getAppId());
        //APPID对应的平台类型
        smr.setMsgType(MsgTypeEnum.MAIL);
        smr.setCallTime(System.currentTimeMillis());
        smr.setIp(mailMessage.getIp());

        MailRequest request = new MailRequest();
        //异步消息是 可用 发送成功后回调
        request.setCallBackUrl(messageProperties.getCallBackUrl());
        // 两者二选一
        request.setToAddress(mailMessage.getAddresses());
        request.setUserName(messageProperties.getMailSenderName());
        request.setPassword(messageProperties.getMailSenderPwd());
        request.setSubject(mailMessage.getSubject());
        request.setContent(mailMessage.getContent());
        //内容类型 取值 HTML TEXT 默认值TEXT
        request.setContentType("TEXT");
        request.setAttachFileNames(mailMessage.getFiles());
        smr.setData(request);
        CloseableHttpResponse respose = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                messageProperties.getSendMsgUrl(), JSONObject.toJSONString(smr), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(respose);
        log.info("sendWxMail request:{},response:{}", JSONObject.toJSONString(smr), content);
        return toResult(content);
    }

    public Result sendMqtt(MqttMessage message) throws IOException {
        SendMsgRequest<MQTTAliRequest> smr = new SendMsgRequest<>();
        //对应 后台APPID
        smr.setAppId(message.getAppId());
        //APPID对应的平台类型
        smr.setMsgType(MsgTypeEnum.MQTT);
        smr.setCallTime(System.currentTimeMillis());
        smr.setIp(message.getIp());

        MQTTAliRequest request = new MQTTAliRequest();
        //异步消息是 可用 发送成功后回调
        request.setCallBackUrl(messageProperties.getCallBackUrl());
        request.setAgreement(MQTTAgreementEnum.TCP);

        request.setClientId(message.getClientId());
        request.setFromClientId(message.getFromClientId());
        request.setMessage(message.getMessage());
        request.setQoS(message.getQos());
        request.setSonTopic(message.getTopic());
        smr.setData(request);

        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                messageProperties.getSendMsgUrl(), JSONObject.toJSONString(smr), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        log.info("sendMqtt request:{},response:{}", JSONObject.toJSONString(smr), content);
        return toResult(content);
    }

    public Result sendBatchMqtt(MqttBatchMessage message) throws IOException {
        SendMsgRequest<MQTTBatchAliRequest> smr = new SendMsgRequest<>();
        //对应 后台APPID
        smr.setAppId(message.getAppId());
        //APPID对应的平台类型
        smr.setMsgType(MsgTypeEnum.MQTTBATCH);
        smr.setCallTime(System.currentTimeMillis());
        smr.setIp(message.getIp());

        MQTTBatchAliRequest request = new MQTTBatchAliRequest();
        request.setCallBackUrl(messageProperties.getCallBackUrl());
        request.setAgreement(MQTTAgreementEnum.TCP);
        request.setClientId(message.getClientIds());
        request.setFromClientId(message.getFromClientId());
        request.setMessage(message.getMessage());
        request.setQoS(message.getQos());
        smr.setData(request);

        CloseableHttpResponse response = HttpUtil.post(HttpUtil.getDefaultCloseableHttpClient(),
                messageProperties.getSendMsgUrl(), JSONObject.toJSONString(smr), CONTENT_TYPE, tokenConfig.getToken());
        String content = HttpUtil.toString(response);
        log.info("sendBatchMqtt request:{},response:{}", JSONObject.toJSONString(smr), content);
        return toResult(content);
    }

    protected Result toResult(String content) {
        return JSONObject.parseObject(content, Result.class);
    }
}
