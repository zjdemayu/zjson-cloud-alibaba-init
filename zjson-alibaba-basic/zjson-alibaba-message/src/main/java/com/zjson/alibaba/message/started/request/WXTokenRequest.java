package com.zjson.alibaba.message.started.request;

import lombok.Data;

/**
 * 微信公众号jstoken
 * 
 * @author pengh
 *2018-08-18
 */
@Data
public class WXTokenRequest  extends SendBaseRequest{
      /**
       * 微信jscode
       */
       private String jsCode;
       /**
        * 应用ID
        */
       private String appId;
       
    
       
}
