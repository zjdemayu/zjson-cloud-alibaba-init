package com.zjson.alibaba.message.started.pojo;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

/**
 * @author yaozou
 * @description:微信小程序消息
 * @date 2019-10-24 16:08
 * @since 1.0.0
 */
@Data
@Builder
public class WxMiniMessage extends BaseMessage {
    private String code;
    private String openId;
    private String page;
    /** 模板参数键值对 **/
    private Map<String,Object> data;
}
