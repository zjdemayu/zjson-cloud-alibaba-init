package com.zjson.alibaba.message.started.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Created by luohuan on 2019/11/16
 */
@Component
public class MessageRedis {

    private static final int ONE_HOUR = 60 * 60;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public static String getMessageTokenKey() {
        return "sys:message:token";
    }

    public String getToken() {
        String key = getMessageTokenKey();
        Object value = redisTemplate.opsForValue().get(key);
        return value == null ? null : (String) value;
    }

    public void setToken(String token) {
        String key = getMessageTokenKey();
        redisTemplate.opsForValue().set(key, token, ONE_HOUR, TimeUnit.SECONDS);
    }

}
