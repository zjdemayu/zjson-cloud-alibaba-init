package com.zjson.alibaba.message.started.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author yaozou
 * @description: 消息接口配置
 * @date 2019-10-24 14:39
 * @since 1.0.0
 */
@Data
@Component
@Configuration
@ConfigurationProperties(prefix = "message")
public class MessageProperties {
    /** token刷新时间 单位:小时 */
    private int refreshTime = 1;
    private String loginUrl;
    private String username;
    private String password;

    private String aliShortMsgAppId;
    private String tencentShortMsgAppId;

    private String mailSenderName;
    private String mailSenderPwd;

    private String sendMsgUrl;
    private String callBackUrl;
    private String queryShortMsgSendResultUrl;

    private String wxTokenUrl;

    /**	微信小程序模板消息 */
    /** 消息跳转到小程序对应页面 */
    private String wxMiniDirectPageUrl = "";

    private String msgProjectUrl;

    private String msgAddAppUrl;
    private String msgUpdateAppUrl;
    private String msgDelAppUrl;

    private String msgAddTempUrl;
    private String msgUpdateTempUrl;
    private String msgDelTempUrl;

    /** 追加短信调试接口开关，避免环境误发情况 */
    private Boolean debugSwitch;

    /**
     * 调试开关是否打开
     *
     * @return true：调试开关已打开；false：调试开关已关闭
     */
    public boolean isDebugSwitch() {
        return Boolean.TRUE.equals(debugSwitch);
    }

}
