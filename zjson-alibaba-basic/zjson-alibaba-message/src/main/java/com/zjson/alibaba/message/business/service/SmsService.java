package com.zjson.alibaba.message.business.service;


import com.zjson.alibaba.message.business.dto.EvidenceMsgDTO;
import com.zjson.alibaba.message.business.dto.VerifyCodeDTO;

import java.util.List;

/**
 * 短信相关Service
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/8/28 15:48
 */
public interface SmsService {

    /**
     * 发送短信验证码
     *
     * @param verifyCode 发送信息
     * @return 发送结果
     */
    String sendVerifyCode(VerifyCodeDTO verifyCode);

    /**
     * 校验验证码
     *
     * @param verifyCodeDTO verifyCodeDTO
     * @return Boolean
     */
    Boolean validateVerifyCode(VerifyCodeDTO verifyCodeDTO);

	/**
	 * 发送凭证信息
	 * @param phone 手机号
	 * @param params 参数
	 */
	void sendEvidences(String phone, List<EvidenceMsgDTO> params);
}
