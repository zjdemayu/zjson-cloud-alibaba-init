package com.zjson.alibaba.message.started.request;

import lombok.Data;

/**
 * 图文消息
 * @author pengh
 *2019-09-02
 */
@Data
public class ArticlesRequest {
	private String title; // 标题
	private String description; // 描述
	private String url;// 该图文的点击跳转链接
	private String picurl;// 图片的URL



}
