package com.zjson.alibaba.message.started.request;

import lombok.Data;

import java.util.Date;

/**
 * 邮件模型
 * @author pengh
 * 2019-08-17
 */
@Data
public class MailRequest  extends SendBaseRequest{  

	
  

    // 邮件接收者的地址      
    private String[] toAddress;  
    // 邮件密送接收者的地址  
    private String[] toBlindCarbonCopyAddress;  
    // 邮件抄送接收者的地址  
    private String[] toCarbonCopyAddress;  
    // 登陆邮件发送服务器的用户名  
    private String userName;  
    // 登陆邮件发送服务器的密码      
    private String password;      
    // 是否需要身份验证      
    private boolean validate = false;      
    // 邮件主题      
    private String subject;      
    // 邮件的文本内容      
    private String content;      
    // 邮件附件的文件名      
    private MailFileRequest[] attachFileNames;  
    private Date sendTime=new Date();
    /**
     * 内容类型
     */
    private String contentType;
    

 
      

   
      
} 