package com.zjson.alibaba.message.started.request;

import lombok.Data;

/**
 * 应用接口
 * 
 * @author ckq 2019-08-11
 */

@Data
public class McAppRequest {

	private Long id;

	/**
	 * 程序名称
	 */
	private String appName;
	/**
	 *  数据类型
	 *  ALI_SHORT_MESSAGE 阿里短信应用 
	 *        对应 parameData

	                "accessKeyId": "",
	                "accessKeySecret": "",
	                "connectTimeout": "",//毫秒
	                "readTimeout": "",//毫秒
	                "serverAddres": "cn-hangzhou",
	                "signName": ""

	 *     
	 *  TENCENT_SHORT_MESSAGE腾讯短信应用 
	 *       对应  parameData
	              "appId": "1400240549",
	              "appKey": "6a81017131d008f41cd67a5fad90b2bc",
	              "smsSign": "227061"

	 *  MAIL 邮件应用
	 *    对应  parameData
	           "mailServerHost": "smtpdm.aliyun.com",
	           "mailServerPort": "25"
	 *  WXGZH微信公众号应用 
	 *    对应  parameData
	                "appId": "wxb9844578ae0d683c",
	                "secret": "ba885395c1d4c1d08f50836aaa2391f8",
                  	"outTime": "5"  //秒

	 *  WXMINIAPP 微信小程序应用 
	  *    对应  parameData
	                "appId": "wxb9844578ae0d683c",
	                "secret": "ba885395c1d4c1d08f50836aaa2391f8",
                  	"outTime": "5"  //秒

	 *  MQTT MQTT应用 
	  *    对应  parameData
	             "instanceId": "post-cn-v641955sk0f",
	             "endPoint": "post-cn-v641955sk0f.mqtt.aliyuncs.com",
               	 "accessKey": "LTAIm9Np19u29LF7",
	             "secretKey": "CkCXB1lT13UPAtUB7DY8l2bPu2kG6m",
	             "parentTopic": "test",
	             "groupId": "GID_TEST"

	 *  ALI_PUSHAPP 阿里移动推送
	 *   对应  parameData

	          "accessKeyId": "LTAIm9Np19u29LF7",
	         "accessKeySecret": "CkCXB1lT13UPAtUB7DY8l2bPu2kG6m",
	          "androidMasterSecret": "8074783a22e2f9cc559ea9091bec1106",
	         "androidAppKey": "27745860",
	         "iosAppKey": "27756479",
	          "serverAddres": "cn-hangzhou",
	          "iosMasterSecret": "77d57b5cf320f1666b59784c55a4584c",
	         "PackageName": "cloud.aliyun.test",
	         "BundleId": "cloud.aliyun.test"

	 */
	private String appType;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 程序状态 :0 停用; 1 正常
	 */
	private String trappStatus;
	/**
	 * 参数内容
	 */
	private String parameData;
	/**
	 * 数据包大小
	 */
	private Long dataPacket;
	/**
	 * 项目ID
	 */
	private Long projectId;

}
