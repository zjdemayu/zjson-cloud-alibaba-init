package com.zjson.alibaba.message.business.utils;

import com.zjson.alibaba.message.business.dto.VerifyCodeDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 短信验证器
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/8/22 22:26
 */
@Slf4j
@Component
public class MsgRedisUtil {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 短信验证码验证
     *
     * @param verifyCodeDTO verifyCodeDTO
     * @return 验证结果
     */
    public boolean validateVerifyCode(VerifyCodeDTO verifyCodeDTO) {
        String msgRedisKey = getMsgRedisKey(verifyCodeDTO);
        String msgCode = redisTemplate.opsForValue().get(msgRedisKey);
        return verifyCodeDTO.getCode().equals(msgCode);
    }

    /**
     * 短信验证码验证，验证成功后删除
     *
     * @param verifyCodeDTO verifyCodeDTO
     * @return 验证结果
     */
    public boolean validateAndDeleteVerifyCode(VerifyCodeDTO verifyCodeDTO) {
        String msgRedisKey = getMsgRedisKey(verifyCodeDTO);
        String msgCode = redisTemplate.opsForValue().get(msgRedisKey);
        if (verifyCodeDTO.getCode().equals(msgCode)) {
            redisTemplate.delete(msgRedisKey);
            return true;
        }
        return false;
    }

    /**
     * 设置密码修改短信验证码
     *
     * @param verifyCodeDTO verifyCodeDTO
     * @param code          验证码
     * @param expire        过期时间
     */
    public void setVerifyCode(VerifyCodeDTO verifyCodeDTO, String code, long expire) {
        redisTemplate.opsForValue().set(getMsgRedisKey(verifyCodeDTO), code, expire, TimeUnit.SECONDS);
    }

    private String getMsgRedisKey(VerifyCodeDTO verifyCodeDTO) {
        return StringUtils.join(verifyCodeDTO.getVerifyCodeTypeEnum().getKeyPrefix(),
                verifyCodeDTO.getPhone());
    }

}
