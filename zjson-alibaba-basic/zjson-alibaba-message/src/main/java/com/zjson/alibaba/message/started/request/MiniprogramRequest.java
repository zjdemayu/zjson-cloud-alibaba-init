package com.zjson.alibaba.message.started.request;

import lombok.Data;

/**
 * 小程序请求对象
 * @author pengh
 * 2019-08-17
 */
@Data
public class MiniprogramRequest {
	/**
	 * 小程序ID
	 */
   private String appid;
   /**
    * 小程序页面地址
    */
   private String pagepath;
}
