package com.zjson.alibaba.message.started.service.impl;

import com.zjson.alibaba.message.started.pojo.Result;
import com.zjson.alibaba.message.started.pojo.ShortMessage;
import com.zjson.alibaba.message.started.menu.MsgTypeEnum;
import com.zjson.alibaba.message.started.service.BaseService;
import com.zjson.alibaba.message.started.service.ISendMessageService;
import com.zjson.alibaba.message.started.util.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author yaozou
 * @description: 发送消息
 * @date 2019-10-24 14:24
 * @since 1.0.0
 */
@Service(value = "sendMessageService")
@Slf4j
public class SendMessageServiceImpl extends BaseService implements ISendMessageService {

    @Override
    public Result sendAliShortMsg(ShortMessage shortMessage) throws Exception {
        String appId = messageProperties.getAliShortMsgAppId();
        shortMessage.setAppId(appId);
        return sendShort(shortMessage, MsgTypeEnum.ALI_SHORT_MESSAGE);
    }

    @Override
    public Result sendTencentShortMsg(ShortMessage shortMessage) throws Exception {
        String appId = messageProperties.getTencentShortMsgAppId();
        shortMessage.setAppId(appId);
        return sendShort(shortMessage, MsgTypeEnum.TENCENT_SHORT_MESSAGE);
    }

    @Override
    public Result queryShortMessageSendResult(String outId) throws IOException {
        String response = HttpUtil.getString(HttpUtil.getDefaultCloseableHttpClient(),
                messageProperties.getQueryShortMsgSendResultUrl() + "?outId=" + outId);
        return toResult(response);
    }
}
