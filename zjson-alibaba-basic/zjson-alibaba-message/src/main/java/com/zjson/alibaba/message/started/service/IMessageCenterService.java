package com.zjson.alibaba.message.started.service;

import com.alibaba.fastjson.JSONObject;
import com.zjson.alibaba.message.started.request.McAppRequest;
import com.zjson.alibaba.message.started.request.McAppTemplateRequest;
import com.zjson.alibaba.message.started.request.ProjectRequest;

import java.io.IOException;

/**
 * @author yaozou
 * @description: 消息中心基础服务
 * @date 2019-11-05 16:58
 * @since 1.0.0
 */
public interface IMessageCenterService {

    /**
     * 添加项目
     * @param request 项目数据
     * @return {"resultData":10015,"success":true}
     * @throws IOException
     */
    JSONObject addProject(ProjectRequest request) throws IOException;

    /**
     * 添加应用
     * @param request
     * @return
     * @throws IOException
     */
    JSONObject addApp(McAppRequest request) throws IOException;

    /**
     * 修改应用
     * @param request
     * @return
     * @throws IOException
     */
    JSONObject updateApp(McAppRequest request) throws IOException;

    /**
     * 批量删除应用
     * @param ids
     * @return
     * @throws IOException
     */
    JSONObject deleteApp(Long[] ids) throws  IOException;

    /**
     * 添加模板
     * @param request
     * @return
     * @throws IOException
     */
    JSONObject addTemplate(McAppTemplateRequest request) throws  IOException;

    /**
     * 更新模板
     * @param request
     * @return
     * @throws IOException
     */
    JSONObject updateTemplate(McAppTemplateRequest request) throws  IOException;

    /**
     * 删除模板
     * @param ids
     * @return
     * @throws IOException
     */
    JSONObject delTemplate(Long[] ids) throws  IOException;

}
