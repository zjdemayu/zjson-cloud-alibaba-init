package com.zjson.alibaba.message.started.request;

import lombok.Data;

import java.util.Map;

/**
 * 微信公众号模板消息领域模型 
 * @author pengh
 *2018-08-18
 */
@Data
public class WXMIniAppMessageRequest  extends SendBaseRequest {
	private String id;

       /**
        * 接收人openId
        */
       private String touser;
    
       private String page;
       /**
        * 数据
        */
       private Map<String,ContentRequest> data;
       
       /**
        * 表单ID
        */
       private String formId;

       
}
