package com.zjson.alibaba.message.started.request;

import lombok.Data;

/**
 * 微信消息内容
 * @author wh
 * 2019-08-17
 */
@Data
public class ContentRequest {
	/**
	 * 参数值
	 */
    private String value;
    /**
     * 显示颜色  保留字段
     */
    private String color;
	public ContentRequest(){}
	public ContentRequest(String value,String color){
		this.value=value;
		this.color=color;
	}
}
