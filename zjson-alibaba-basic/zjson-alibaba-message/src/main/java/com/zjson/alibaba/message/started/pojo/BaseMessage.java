package com.zjson.alibaba.message.started.pojo;

import lombok.Data;

/**
 * @author yaozou
 * @description:
 * @date 2019-10-24 15:57
 * @since 1.0.0
 */
@Data
public class BaseMessage {
    private String ip;
    // 消息中心返回的appId
    private String appId;
}
