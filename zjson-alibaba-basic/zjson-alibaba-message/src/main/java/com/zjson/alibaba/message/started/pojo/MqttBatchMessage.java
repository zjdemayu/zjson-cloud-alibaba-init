package com.zjson.alibaba.message.started.pojo;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author yaozou
 * @description:
 * @date 2019-10-24 18:00
 * @since 1.0.0
 */
@Data
@Builder
public class MqttBatchMessage extends BaseMessage {
    private List<String> clientIds;
    /** 发送方ID */
    private String fromClientId;
    private String message;
    private Integer qos;
    private String topic;
}
