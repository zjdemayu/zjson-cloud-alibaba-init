package com.zjson.alibaba.message.started.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.util.concurrent.TimeUnit;

public class DefaultApacheHttpClientBuilder {
	private int connectionRequestTimeout = 3000;//从连接池获取连接的超时时间
	private int connectionTimeout = 5000;//客户端和服务器建立连接的超时时间
	private int soTimeout = 10000;//从客户端从服务器读取数据的timeout，超出后会抛出SocketTimeOutException
	private int idleConnTimeout = 60000;//连接池连接空闲时间
	private int checkWaitTime = 60000;//连接池检查时间
	private int maxConnPerHost = 300;//每个host最大连接数
	private int maxTotalConn = 500;//总的最大连接数
	private String userAgent;//代理
	//重试handler
	private HttpRequestRetryHandler httpRequestRetryHandler = new DefaultHttpRequestRetryHandler(3, false);
	//ssl连接工厂
	private SSLConnectionSocketFactory sslConnectionSocketFactory = SSLConnectionSocketFactory.getSocketFactory();
	//连接工厂
	private PlainConnectionSocketFactory plainConnectionSocketFactory = PlainConnectionSocketFactory.getSocketFactory();

	private String httpProxyHost;//代理地址
	private int httpProxyPort;//代理端口
	private String httpProxyUsername;//代理用户
	private String httpProxyPassword;//代理用户密码

	/**
	 * 闲置连接监控线程
	 */
	private IdleConnectionMonitorThread idleConnectionMonitorThread;
    //客户端创建者
	private HttpClientBuilder httpClientBuilder;
   
	private boolean prepared = false;
	//默认构造方法，外部不需访问
	DefaultApacheHttpClientBuilder() {
	}

 
    /**
     * 设置代理地址
     * @param httpProxyHost
     * @return
     */
	public DefaultApacheHttpClientBuilder httpProxyHost(String httpProxyHost) {
		this.httpProxyHost = httpProxyHost;
		return this;
	}
	/**
	 * 设置代理端口
	 * @param httpProxyPort
	 * @return
	 */
	public DefaultApacheHttpClientBuilder httpProxyPort(int httpProxyPort) {
		this.httpProxyPort = httpProxyPort;
		return this;
	}
    /**
     * 设置代理用户
     * @param httpProxyUsername
     * @return
     */
	public DefaultApacheHttpClientBuilder httpProxyUsername(String httpProxyUsername) {
		this.httpProxyUsername = httpProxyUsername;
		return this;
	}
    /**
     * 设置代理用户密码
     * @param httpProxyPassword
     * @return
     */
	public DefaultApacheHttpClientBuilder httpProxyPassword(String httpProxyPassword) {
		this.httpProxyPassword = httpProxyPassword;
		return this;
	}
    /**
     * 设置ssl连接工厂
     * @param sslConnectionSocketFactory
     * @return
     */
	public DefaultApacheHttpClientBuilder sslConnectionSocketFactory(SSLConnectionSocketFactory sslConnectionSocketFactory) {
		this.sslConnectionSocketFactory = sslConnectionSocketFactory;
		return this;
	}

	public IdleConnectionMonitorThread getIdleConnectionMonitorThread() {
		return this.idleConnectionMonitorThread;
	}

	private void prepare() {
		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
				.register("http", this.plainConnectionSocketFactory).register("https", this.sslConnectionSocketFactory)
				.build();
        
		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
		connectionManager.setMaxTotal(this.maxTotalConn);
		connectionManager.setDefaultMaxPerRoute(this.maxConnPerHost);
		connectionManager
				.setDefaultSocketConfig(SocketConfig.copy(SocketConfig.DEFAULT).setSoTimeout(this.soTimeout).build());

		this.idleConnectionMonitorThread = new IdleConnectionMonitorThread(connectionManager, this.idleConnTimeout,
				this.checkWaitTime);
		this.idleConnectionMonitorThread.setDaemon(true);
		this.idleConnectionMonitorThread.start();

		this.httpClientBuilder = HttpClients.custom().setConnectionManager(connectionManager)
				.setConnectionManagerShared(true)
				.setDefaultRequestConfig(RequestConfig.custom().setSocketTimeout(this.soTimeout)
						.setConnectTimeout(this.connectionTimeout)
						.setConnectionRequestTimeout(this.connectionRequestTimeout).build())
				.setRetryHandler(this.httpRequestRetryHandler);

		if (StringUtils.isNotBlank(this.httpProxyHost) && StringUtils.isNotBlank(this.httpProxyUsername)) {
			// 使用代理服务器 需要用户认证的代理服务器
			CredentialsProvider provider = new BasicCredentialsProvider();
			provider.setCredentials(new AuthScope(this.httpProxyHost, this.httpProxyPort),
					new UsernamePasswordCredentials(this.httpProxyUsername, this.httpProxyPassword));
			this.httpClientBuilder.setDefaultCredentialsProvider(provider);
		}

		if (StringUtils.isNotBlank(this.userAgent)) {
			this.httpClientBuilder.setUserAgent(this.userAgent);
		}

	}
	/**
	 * 创建可关闭客户端
	 * @return
	 */
	public CloseableHttpClient build() {

		if (!this.prepared) {
			prepare();
			this.prepared = true;
		}

		return this.httpClientBuilder.build();
	}
    /**
     * 空闲连接监控
     * @author pengh
     * 2019-08-20
     */
	public static class IdleConnectionMonitorThread extends Thread {
		private final HttpClientConnectionManager connMgr;
		private final int idleConnTimeout;
		private final int checkWaitTime;
		private volatile boolean shutdown;

		public IdleConnectionMonitorThread(HttpClientConnectionManager connMgr, int idleConnTimeout,
				int checkWaitTime) {
			super("IdleConnectionMonitorThread");
			this.connMgr = connMgr;
			this.idleConnTimeout = idleConnTimeout;
			this.checkWaitTime = checkWaitTime;
		}

		@Override
		public void run() {
			try {
				while (!this.shutdown) {
					synchronized (this) {
						wait(this.checkWaitTime);
						this.connMgr.closeExpiredConnections();
						this.connMgr.closeIdleConnections(this.idleConnTimeout, TimeUnit.MILLISECONDS);
					}
				}
				this.connMgr.shutdown();
			} catch (InterruptedException ignore) {
			}
		}

		public void trigger() {
			synchronized (this) {
				notifyAll();
			}
		}

		public void shutdown() {
			this.shutdown = true;
			synchronized (this) {
				notifyAll();
			}
		}
	}
}
