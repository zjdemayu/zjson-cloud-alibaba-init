package com.zjson.alibaba.message.started.request;


import com.zjson.alibaba.message.started.menu.MQTTAgreementEnum;
import lombok.Data;

/**
 * 阿里mqtt请求对象
 * @author pengh
 * 2019-08-17
 */
@Data
public class MQTTAliRequest extends SendBaseRequest  {
	/**
	 * 非必填
	 * 客户端ID
	 */
	private String clientId;
	/**
	 * 必填
	 * 需要发送的消息体
	 */
	private String message;

	/**
	 * 二级Topic  非必填
	 * 格式 未  XXX 或 XXX/XXX 或 XXX/XXX/XXX
	 */
	private String sonTopic;
	/**
	 * 服务智联 0 代表最多分发一次 
	 * 1 代表至少达到一次
	 * 2 代表仅分发一次
	 */
	private Integer QoS;
	/**
	 * 
	 * 消息发送者客户端ID
	 */
	private String fromClientId;
	/**
	 * 使用的网络协议 非必填 默认使用tcp 协议
	 */
	private MQTTAgreementEnum agreement;

} 
