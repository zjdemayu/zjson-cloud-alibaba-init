package com.zjson.alibaba.message.started.request;

import lombok.Builder;

/**
 * 参数对象 键值对
 * @author pengh
 *
 */
@Builder
public class ParamRequest {
	private String key;
	private String value;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
      
}
