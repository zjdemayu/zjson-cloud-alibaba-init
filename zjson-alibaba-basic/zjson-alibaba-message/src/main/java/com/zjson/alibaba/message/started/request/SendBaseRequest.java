package com.zjson.alibaba.message.started.request;


/**
 * 请求对象父类
 * @author pengh
 * 2019-08-17
 */
public class SendBaseRequest  {


	/**
	 * 异步调用回调地址  请求方式为 get
	 */
    private String callBackUrl;
	/**
	 * 模板code
	 */
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}



    
}
