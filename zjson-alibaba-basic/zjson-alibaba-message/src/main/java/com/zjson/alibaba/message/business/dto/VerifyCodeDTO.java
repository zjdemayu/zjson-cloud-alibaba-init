package com.zjson.alibaba.message.business.dto;

import com.zjson.alibaba.message.business.enums.VerifyCodeTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 短信验证码DTO
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/8/28 15:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@ApiModel(value = "短信验证码DTO")
public class VerifyCodeDTO implements Serializable {

    private static final long serialVersionUID = -1794952168828925650L;

    @ApiModelProperty("验证码类型（0：兑换凭证）")
    private VerifyCodeTypeEnum verifyCodeTypeEnum;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty(value = "验证码，校验时传递此参数")
    private String code;

}
