package com.zjson.alibaba.message.started.pojo;

import com.zjson.alibaba.message.started.request.ParamRequest;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author yaozou
 * @description: 短信
 * @date 2019-10-24 14:35
 * @since 1.0.0
 */
@Data
@Builder
public class ShortMessage extends BaseMessage {
    private String code;
    private String phone;
    private List<ParamRequest> params;

    /**
     * 外部流水扩展字段
     */
    private String outId;
}
