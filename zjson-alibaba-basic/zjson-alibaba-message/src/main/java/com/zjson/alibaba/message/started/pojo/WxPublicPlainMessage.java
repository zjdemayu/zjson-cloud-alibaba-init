package com.zjson.alibaba.message.started.pojo;

import com.zjson.alibaba.message.started.request.ArticlesRequest;
import lombok.Builder;
import lombok.Data;

/**
 * @author yaozou
 * @description:微信公众号普通消息
 * @date 2019-10-24 15:43
 * @since 1.0.0
 */
@Data
@Builder
public class WxPublicPlainMessage extends BaseMessage {
    private Boolean toAll;
    private String[] openIds;

    //消息类型 文本为text，图片为image，语音为voice，视频消息为video，
    // 音乐消息为music，图文消息（点击跳转到外链）为news，
    // 图文消息（点击跳转到图文消息页面）为mpnews，
    // 卡券为wxcard，小程序为miniprogrampage
    private String msgType;

    /** 文本为text */
    private String content;
    /** 图文消息（点击跳转到外链）为news */
    private ArticlesRequest articlesRequest;

    /**
     * 内容描述
     */
    private String description;
    /**
     * 内容标题
     */
    private String title;
    /**
     * 素材ID
     */
    private String mediaId;
    /**
     * 卡券id
     */
    private String cardId;
}
