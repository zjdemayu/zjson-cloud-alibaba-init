package com.zjson.alibaba.message.started.menu;
/**
 * MQTT 协议枚举
 * @author pengh
 *2019-09-02
 */
public enum MQTTAgreementEnum {
	TCP("tcp","1883"),SSL("ssl","8883"),WEBSOCKET("ws","80")
	,WEBSOCKET_SSL("wss","443");
	private String agreement;
	private String port;
	MQTTAgreementEnum(String agreement,String port){
		this.agreement=agreement;
		this.port=port;
	}

	public String getAgreement() {
		return agreement;
	}
	public String getPort() {
		return port;
	}
	
}
