package com.zjson.alibaba.message.started.request;

import lombok.Data;

/**
 * 请求校验
 * @author wh
 * 2019-08-17
 *
 */
public interface IRequestValidation {
     ValidationResult validationData();
}

//校验结果
@Data
class ValidationResult{
	private String code="OK";
	private String msg;
}