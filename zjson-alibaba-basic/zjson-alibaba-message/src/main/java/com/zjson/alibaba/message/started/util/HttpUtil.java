package com.zjson.alibaba.message.started.util;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * http 工具
 *
 * @author pengh
 * 2019-08-20
 */
public class HttpUtil {
    public static final int SC_CONTINUE = 100;
    public static final int SC_SWITCHING_PROTOCOLS = 101;
    public static final int SC_OK = 200;
    public static final int SC_CREATED = 201;
    public static final int SC_ACCEPTED = 202;
    public static final int SC_NON_AUTHORITATIVE_INFORMATION = 203;
    public static final int SC_NO_CONTENT = 204;
    public static final int SC_RESET_CONTENT = 205;
    public static final int SC_PARTIAL_CONTENT = 206;
    public static final int SC_MULTIPLE_CHOICES = 300;
    public static final int SC_MOVED_PERMANENTLY = 301;
    public static final int SC_MOVED_TEMPORARILY = 302;
    public static final int SC_FOUND = 302;
    public static final int SC_SEE_OTHER = 303;
    public static final int SC_NOT_MODIFIED = 304;
    public static final int SC_USE_PROXY = 305;
    public static final int SC_TEMPORARY_REDIRECT = 307;
    public static final int SC_BAD_REQUEST = 400;
    public static final int SC_UNAUTHORIZED = 401;
    public static final int SC_PAYMENT_REQUIRED = 402;
    public static final int SC_FORBIDDEN = 403;
    public static final int SC_NOT_FOUND = 404;
    public static final int SC_METHOD_NOT_ALLOWED = 405;
    public static final int SC_NOT_ACCEPTABLE = 406;
    public static final int SC_PROXY_AUTHENTICATION_REQUIRED = 407;
    public static final int SC_REQUEST_TIMEOUT = 408;
    public static final int SC_CONFLICT = 409;
    public static final int SC_GONE = 410;
    public static final int SC_LENGTH_REQUIRED = 411;
    public static final int SC_PRECONDITION_FAILED = 412;
    public static final int SC_REQUEST_ENTITY_TOO_LARGE = 413;
    public static final int SC_REQUEST_URI_TOO_LONG = 414;
    public static final int SC_UNSUPPORTED_MEDIA_TYPE = 415;
    public static final int SC_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    public static final int SC_EXPECTATION_FAILED = 417;
    public static final int SC_INTERNAL_SERVER_ERROR = 500;
    public static final int SC_NOT_IMPLEMENTED = 501;
    public static final int SC_BAD_GATEWAY = 502;
    public static final int SC_SERVICE_UNAVAILABLE = 503;
    public static final int SC_GATEWAY_TIMEOUT = 504;
    public static final int SC_HTTP_VERSION_NOT_SUPPORTED = 505;

    public static CloseableHttpClient getDefaultCloseableHttpClient() {
        return CloseableHttpClientHolder.instance;

    }

    public static void CloseDefaultCloseableHttpClient() {
        try {
            CloseableHttpClientHolder.instance.close();
            CloseableHttpClientHolder.builder.getIdleConnectionMonitorThread().shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Header[] getDefaultHeader() {
        return new Header[]{
                new BasicHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"),
                new BasicHeader("Accept-Encoding", "gzip"),
                new BasicHeader("Accept-Language", "zh-CN, en-US"),
                new BasicHeader("Connection", "keep-alive"),
                new BasicHeader("Accept-Encoding", "gzip,deflate"),
                new BasicHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36")
        };
    }

    /**
     * get 请求
     *
     * @param httpClient 客户端 @see CloseableHttpClient
     * @param url        请求地址
     * @param encoding   编码
     * @return
     * @throws IOException
     */
    public static String getString(CloseableHttpClient httpClient, String url, String encoding) throws IOException {
        HttpGet get = new HttpGet(url);
        get.setHeaders(getDefaultHeader());
        CloseableHttpResponse response = httpClient.execute(get);
        //成功
        if (response.getStatusLine().getStatusCode() == 200) {
            String contentEncoding = encoding != null ? encoding : response.getEntity().getContentEncoding() == null ? null : response.getEntity().getContentEncoding().getValue();
            try (InputStream input = response.getEntity().getContent()) {
                if (contentEncoding != null && contentEncoding.toLowerCase().endsWith("gzip")) {
                    return IOUtils.toString(new GZIPInputStream(input), "utf-8");
                } else {
                    return IOUtils.toString(input, contentEncoding);
                }
            }
        }
        closeResponse(response);
        return null;
    }

    /**
     * 状态码
     *
     * @param response
     * @return
     */
    public static int statusCode(CloseableHttpResponse response) {
        return response.getStatusLine().getStatusCode();
    }

    public static boolean checkStatus(CloseableHttpResponse response, int status) {
        return statusCode(response) == status;
    }

    public static String toString(CloseableHttpResponse response) throws IOException {
        return toString(response, null);
    }

    public static String toString(CloseableHttpResponse response, String encoding) throws IOException {
        String contentEncoding = encoding != null ? encoding : response.getEntity().getContentEncoding() == null ? null : response.getEntity().getContentEncoding().getValue();
        try (InputStream input = response.getEntity().getContent()) {
            if (contentEncoding != null && contentEncoding.toLowerCase().endsWith("gzip")) {
                return IOUtils.toString(new GZIPInputStream(input), "utf-8");
            } else {
                return IOUtils.toString(input, contentEncoding);
            }
        }
    }

    public static String getString(CloseableHttpClient httpClient, String url) throws IOException {
        return getString(httpClient, url, null);
    }

    public static CloseableHttpResponse post(CloseableHttpClient httpClient, String url, String data) throws IOException {
        return post(httpClient, url, data, null, null);
    }

    public static CloseableHttpResponse post(CloseableHttpClient httpClient, String url, String data, String contentType, String token) throws IOException {
        HttpPost request = new HttpPost(url);
        StringEntity entity = new StringEntity(data, "utf-8");
        if (token != null) {
            request.setHeader("LOGIN_USER", token);
        }
        if (contentType != null) {
            entity.setContentType(contentType);
        }
        request.setEntity(entity);

        return httpClient.execute(request);
    }

    public static CloseableHttpResponse put(CloseableHttpClient httpClient, String url, String data) throws IOException {
        return put(httpClient, url, data, null);
    }

    public static CloseableHttpResponse put(CloseableHttpClient httpClient, String url, String data, String contentType) throws IOException {
        HttpPut request = new HttpPut(url);
        StringEntity entity = new StringEntity(data, "utf-8");
        if (contentType != null) {
            entity.setContentType(contentType);
        }
        request.setEntity(entity);

        return httpClient.execute(request);
    }

    public static CloseableHttpResponse delete(CloseableHttpClient httpClient, String url) throws IOException {
        HttpDelete request = new HttpDelete(url);
        request.setHeaders(getDefaultHeader());
        return httpClient.execute(request);
    }

    public static CloseableHttpResponse head(CloseableHttpClient httpClient, String url) throws IOException {
        HttpHead request = new HttpHead(url);
        request.setHeaders(getDefaultHeader());
        return httpClient.execute(request);
    }

    public static void closeResponse(CloseableHttpResponse response) {
        if (response != null) {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                response = null;
            }
        }

    }

    private static class CloseableHttpClientHolder {
        private static DefaultApacheHttpClientBuilder builder = new DefaultApacheHttpClientBuilder();
        private static CloseableHttpClient instance = builder.build();
    }
}
