package com.zjson.alibaba.message.started.request;

import lombok.Data;

import java.util.Map;

/**
 * 微信公众号模板消息领域模型
 * 
 * @author pengh
 *2018-08-18
 */
@Data
public class WXTemplateMessageRequest  extends SendBaseRequest {
	private String id;

	/**
	 * 接收人openId
	 */
	private String touser;
	/**
	 * 跳转地址
	 */
	private String url;
	/**
	 * 小程序
	 */
	private MiniprogramRequest miniprogram;
	/**
	 * 数据
	 */
	private Map<String, ContentRequest> data;
	private String msgCode;
   


}
