package com.zjson.alibaba.oss.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjson.alibaba.oss.entity.SysOssEntity;
import com.zjson.alibaba.oss.mapper.SysOssMapper;
import com.zjson.alibaba.oss.service.OssService;
import org.springframework.stereotype.Service;

@Service
public class OssServiceImpl extends ServiceImpl<SysOssMapper, SysOssEntity> implements OssService {

}
