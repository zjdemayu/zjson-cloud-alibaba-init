package com.zjson.alibaba.oss.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjson.alibaba.oss.entity.SysOssEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传
 */
@Mapper
public interface SysOssMapper extends BaseMapper<SysOssEntity> {

}
