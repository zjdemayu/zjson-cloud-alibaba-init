package com.zjson.alibaba.oss.cloud;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.auth.CredentialsProvider;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.aliyun.oss.common.auth.DefaultCredentials;
import com.aliyun.oss.common.comm.Protocol;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 创建OSSClient，交给spring管理
 *
 * @author zengzhi
 * @version 1.0
 * @since 2021/5/31 16:25
 */
@Configuration
@Getter
public class OssConfig {

    @Value("${spring.cloud.alicloud.access-key}")
    private String accessKey;

    @Value("${spring.cloud.alicloud.secret-key}")
    private String secretKey;

    @Value("${spring.cloud.alicloud.directory}")
    private String directory;

    @Value("${spring.cloud.alicloud.oss.bucket}")
    private String bucket;

    @Value("${spring.cloud.alicloud.oss.private-endpoint}")
    private String privateEndpoint;

    @Value("${spring.cloud.alicloud.oss.expiration-seconds}")
    private Long expirationSeconds;

    @Bean
    public OSS ossGetUrlClient() {
        DefaultCredentials defaultCredentials = new DefaultCredentials(accessKey,
                secretKey);
        CredentialsProvider provider = new DefaultCredentialProvider(defaultCredentials);
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setProtocol(Protocol.HTTPS);
        return new OSSClient(privateEndpoint, provider, clientConfiguration);
    }

}
