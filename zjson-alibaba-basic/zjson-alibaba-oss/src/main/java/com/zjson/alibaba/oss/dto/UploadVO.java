package com.zjson.alibaba.oss.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 上传信息
 *
 * @author zz
 * @version 1.0
 * @since 2020/9/10 17:23
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@ApiModel(value = "上传信息")
public class UploadVO {

    @ApiModelProperty(value = "文件key")
    private String key;

    @ApiModelProperty(value = "文件URL")
    private String url;

    @ApiModelProperty(value = "文件大小，单位字节")
    private Long size;

}
