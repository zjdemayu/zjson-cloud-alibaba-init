package com.zjson.alibaba.oss.cloud;

import com.zjson.alibaba.commons.tools.utils.DateUtils;
import com.zjson.alibaba.oss.dto.UploadVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.UUID;

/**
 * 云存储
 *
 * @author zz
 */
public abstract class AbstractAliYunOssService {

    /**
     * 文件路径
     *
     * @param prefix 前缀
     * @param suffix 后缀
     * @return 返回上传路径
     */
    public String getPath(String prefix, String suffix) {
        //生成uuid
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        //文件路径
        String path = DateUtils.format(new Date(), "yyyyMMdd") + "/" + uuid;

        if (StringUtils.isNotBlank(prefix)) {
            path = prefix + "/" + path;
        }

        return path + "." + suffix;
    }

    /**
     * 文件上传
     *
     * @param multipartFile multipartFile
     * @return 返回http地址
     */
    public abstract UploadVO upload(MultipartFile multipartFile);

    /**
     * 获取图片地址
     *
     * @param key key
     * @return 图片地址
     */
    public abstract String getOssUrl(String key);

    /**
     * 获取图片地址
     *
     * @param key key
     * @return 图片地址
     */
    public abstract String getOssUrl(String key, Long expirationHours);

}
