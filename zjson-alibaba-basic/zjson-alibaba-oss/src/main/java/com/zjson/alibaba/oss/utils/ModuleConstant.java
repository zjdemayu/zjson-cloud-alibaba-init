package com.zjson.alibaba.oss.utils;

import com.zjson.alibaba.commons.tools.constant.Constant;

/**
 * 模块常量
 */
public interface ModuleConstant extends Constant {
    /**
     * 云存储配置KEY
     */
    String CLOUD_STORAGE_CONFIG_KEY = "CLOUD_STORAGE_CONFIG_KEY";
}
