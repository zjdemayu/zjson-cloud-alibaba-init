package com.zjson.alibaba.oss.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.oss.entity.SysOssEntity;

/**
 * 文件上传
 */
public interface OssService extends IService<SysOssEntity> {

}
