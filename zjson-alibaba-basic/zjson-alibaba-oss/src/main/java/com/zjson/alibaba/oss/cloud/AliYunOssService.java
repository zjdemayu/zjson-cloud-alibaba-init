package com.zjson.alibaba.oss.cloud;

import com.aliyun.oss.OSS;
import com.aliyun.oss.common.comm.Protocol;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.oss.dto.UploadVO;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * 阿里云存储
 *
 * @author admin
 */
@Component("aliYunOssService")
public class AliYunOssService extends AbstractAliYunOssService {

    private final OSS ossClient;

    private final OSS ossGetUrlClient;

    private final OssConfig ossConfig;

    public AliYunOssService(OSS ossClient, OSS ossGetUrlClient, OssConfig ossConfig) {
        this.ossClient = ossClient;
        this.ossGetUrlClient = ossGetUrlClient;
        this.ossConfig = ossConfig;
    }

    @Override
    public UploadVO upload(MultipartFile multipartFile) {
        String url;
        String key = getPath(ossConfig.getDirectory(), FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
        try {
            ossClient.putObject(ossConfig.getBucket(), key, new ByteArrayInputStream(multipartFile.getBytes()));
            url = getOssUrl(key);
        } catch (Exception e) {
            throw new CustomException("上传文件失败{0}", e);
        }
        return UploadVO.of(key, url, multipartFile.getSize());
    }

    @Override
    public String getOssUrl(String key) {
        return getOssUrl(key, ossConfig.getExpirationSeconds());
    }

    @Override
    public String getOssUrl(String key, Long expirationHours) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        if (StringUtils.startsWith(key, Protocol.HTTPS.toString())
                || StringUtils.startsWith(key, Protocol.HTTP.toString())) {
            return key;
        }
        URL url = ossGetUrlClient.generatePresignedUrl(ossConfig.getBucket(), key,
                Date.from(LocalDateTime.now().plusSeconds(expirationHours).atZone(ZoneId.systemDefault()).toInstant()));
        return url.toString();
    }

}
