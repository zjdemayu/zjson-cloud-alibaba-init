package com.zjson.alibaba.sdk.service.impl;

import com.zjson.alibaba.sdk.domain.Result;
import com.zjson.alibaba.sdk.properties.RightsProperties;
import com.zjson.alibaba.sdk.constant.RequestConstant;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Map;

/**
 *  restTemplate-http请求服务
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/6/28 11:54
 */
@Service
public class RestSendService {
	@Resource
	private  RestTemplate restTemplate;

	@Resource
	private RightsProperties rightsProperties;

	/**
	 * post 请求
	 * @param uri uri
	 * @param map map
	 * @return Result
	 */
	public Result sendPost(String uri, Map<String, Object> map, String token) {
		ResponseEntity<Result> responseEntity = restTemplate.postForEntity(
				rightsProperties.getPreUri() + uri,
				generatePostJson(map, token),
				Result.class);
		return responseEntity.getBody();
	}

	/**
	 * get 请求
	 * @param uri uri
	 * @return Result
	 */
	public  Result sendGet(String uri,Map<String, Object> map, String token) {
		ResponseEntity<Result> responseEntity = restTemplate.exchange(
				rightsProperties.getPreUri() + uri,
				HttpMethod.GET,
				generatePostJson(null, token),
				Result.class,
				map);
		return responseEntity.getBody();
	}


	public static HttpEntity<Map<String, Object>> generatePostJson(Map<String, Object> jsonMap, String token) {
		//如果需要其它的请求头信息、都可以在这里追加
		HttpHeaders httpHeaders = new HttpHeaders();
		MediaType type = MediaType.parseMediaType("application/json;charset=UTF-8");
		httpHeaders.setContentType(type);
		httpHeaders.set("terminal_client", RequestConstant.TERMINAL_CLIENT);
		httpHeaders.set("token", token);
		return new HttpEntity<>(jsonMap, httpHeaders);
	}


}
