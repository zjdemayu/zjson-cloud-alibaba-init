package com.zjson.alibaba.sdk.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 确认发放请求参数
 *
 * @author zj
 * @date 2021-05-20 09:37:41
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EvidenceIssueSdkParam implements Serializable {


    private static final long serialVersionUID = 5946773768676757317L;

    /**
     * 凭证下发详情
     */
    List<EvidenceIssueSdkDetailParam> detailParams;


    /**
     * 凭证下发id
     */
    private String evidenceIssuedId;


}
