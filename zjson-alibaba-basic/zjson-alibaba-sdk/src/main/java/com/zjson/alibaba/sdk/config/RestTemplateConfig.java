package com.zjson.alibaba.sdk.config;

import com.zjson.alibaba.sdk.properties.RightsProperties;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * RestTemplateConfig 配置
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/6/28 14:58
 */
@Configuration
public class RestTemplateConfig {
	@Resource
	private RightsProperties rightsProperties;

	@Bean
	public RestTemplate restTemplate(@Qualifier("simpleClientHttpRequestFactory") ClientHttpRequestFactory factory){
		return new RestTemplate(factory);
	}

	@Bean
	public ClientHttpRequestFactory simpleClientHttpRequestFactory(){
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		//单位为ms
		factory.setReadTimeout(rightsProperties.getReadTimeout());
		//单位为ms
		factory.setConnectTimeout(rightsProperties.getConnectTimeout());
		return factory;
	}

}
