package com.zjson.alibaba.sdk.service.impl;

import com.zjson.alibaba.sdk.constant.RequestConstant;
import com.zjson.alibaba.sdk.domain.Result;
import com.zjson.alibaba.sdk.domain.dto.EvidenceInfoDTO;
import com.zjson.alibaba.sdk.domain.dto.IssuedDTO;
import com.zjson.alibaba.sdk.domain.vo.EvidenceIssueSdkParam;
import com.zjson.alibaba.sdk.domain.vo.ExternalEvidenceVO;
import com.zjson.alibaba.sdk.domain.vo.ListVo;
import com.zjson.alibaba.sdk.domain.vo.TokenParam;
import com.zjson.alibaba.sdk.service.RightsSdkService;
import com.zjson.alibaba.sdk.service.TokenService;
import com.zjson.alibaba.sdk.utils.JsonUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *  实现凭证相关服务
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/6/28 11:41
 */
@Service
public class RightsSdkServiceImpl implements RightsSdkService, TokenService {
	@Resource
	private RestSendService restSendService;

	@Override
	public Result<String> getToken(TokenParam param) {
		Map<String, Object> map = JsonUtils.objectToMap(param);
		return restSendService.sendPost(RequestConstant.GRT_TOKEN_URI, map, "");
	}

	@Override
	public Result<List<ListVo>> getCanUseList(Integer type, String token) {
		Map<String, Object> map = Collections.singletonMap("type", type);
		return restSendService.sendGet(RequestConstant.GET_CAN_USE_LIST_URI, map, token);
	}

	@Override
	public Result<EvidenceIssueSdkParam> issuedToCustomer(IssuedDTO param, String token) {
		Map<String, Object> map = JsonUtils.objectToMap(param);
		return restSendService.sendPost(RequestConstant.ISSUED_TO_CUSTOMER_URI, map, token);
	}

	@Override
	public Result<ExternalEvidenceVO> getEvidenceInfo(EvidenceInfoDTO param, String token) {
		Map<String, Object> map = JsonUtils.objectToMap(param);
		return restSendService.sendPost(RequestConstant.GET_EVIDENCE_INFO_URI, map, token);
	}

	@Override
	public Result<Boolean> noticeOver(EvidenceInfoDTO param, String token) {
		Map<String, Object> map = JsonUtils.objectToMap(param);
		return restSendService.sendPost(RequestConstant.NOTICE_OVER_URI, map, token);
	}

	@Override
	public Result<String> cancel(String evidenceIssuedId, String token) {
		Map<String, Object> map = Collections.singletonMap("evidenceIssuedId", evidenceIssuedId);
		return restSendService.sendGet(RequestConstant.CANCEL_URI, map, token);
	}


}
