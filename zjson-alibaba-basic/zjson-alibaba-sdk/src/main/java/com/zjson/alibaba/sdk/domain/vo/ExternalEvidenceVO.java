package com.zjson.alibaba.sdk.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 凭证(Evidence)表DTO
 *
 * @author zengzhi
 * @date 2021-05-20 09:37:41
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExternalEvidenceVO implements Serializable {
    private static final long serialVersionUID = -7356250190796865132L;


    /**
     * 发放的用户手机号
     */
    private String phone;

    /**
     * 凭证ID
     */
    private String evidenceId;

    /**
     * 凭证名称
     */
    private String evidenceName;

    /**
     * 凭证状态
     */
    private Integer status;
    /**
     * 发放时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime issuedTime;

    /**
     * 使用开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate usingStartTime;

    /**
     * 使用结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate usingEndTime;

    private Boolean canUseInStore;



}
