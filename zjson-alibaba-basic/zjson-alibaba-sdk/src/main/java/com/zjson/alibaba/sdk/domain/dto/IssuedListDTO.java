package com.zjson.alibaba.sdk.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 下发个数
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/6/28 17:42
 */
@Data
public class IssuedListDTO {
	/**
	 * 凭证ID
	 */
	@NotBlank(message = "凭证ID不能为空")
	private String evidenceId;

	/**
	 * 下发数量
	 */
	@NotNull(message = "下发数量不能为空")
	private Integer issuedQuantity;
}
