package com.zjson.alibaba.sdk.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 三方查询凭证信息模型
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/5/22 14:45
 */
@Data
public class EvidenceInfoDTO implements Serializable {
	private static final long serialVersionUID = 5437709524368607190L;

	/**
	 * 门店编号
	 */
	@NotBlank(message = "门店编号不能为空")
	private String storeNum;


	/**
	 * 凭证码
	 */
	@NotBlank(message = "凭证码不能为空")
	private String evidenceCode;
}
