package com.zjson.alibaba.sdk.domain.vo;

import lombok.Data;

/**
 * 商户列表显示模型
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/5/20 10:23
 */
@Data
public class ListVo {
	/**
	 * 主键id
	 */
	private String id;

	/**
	 * 名称
	 */
	private String name;
	/**
	 * 商户名称；查询发行凭证时：查询的是使用商户名称；查询使用凭证时：查询的是发行商户名称
	 */
	private String merchantName;

}
