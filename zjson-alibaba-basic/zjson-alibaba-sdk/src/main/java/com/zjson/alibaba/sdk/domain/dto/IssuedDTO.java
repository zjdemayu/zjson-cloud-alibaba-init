package com.zjson.alibaba.sdk.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 下发凭证给用户
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/5/22 14:45
 */
@Data
public class IssuedDTO implements Serializable {
	private static final long serialVersionUID = -6496892179932739334L;
	/**
	 * 门店编号:C端购买时为空
	 */
	private String storeNum;

	@NotBlank(message = "手机号不能为空")
	private String phone;

	/**
	 * 下发list
	 */
	private List<IssuedListDTO> issuedList;

	/**
	 * 业务编号
	 */
	@NotBlank(message = "业务编号不能为空")
	private String businessNum;

}
