package com.zjson.alibaba.sdk.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * sdk的配置
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/6/28 16:31
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "rights-displace.sdk")
public class RightsProperties {
	/**
	 * 调用环境uri
	 */
	private String preUri;
	/**
	 * appId
	 */
	private String appId;
	/**
	 * appKey
	 */
	private String appKey;

	/**
	 * 读取超时时间
	 */
	private Integer readTimeout = 10000;
	/**
	 *  链接超时时间
	 */
	private Integer connectTimeout = 15000;

}
