package com.zjson.alibaba.sdk.service;

import com.zjson.alibaba.sdk.domain.Result;
import com.zjson.alibaba.sdk.domain.vo.TokenParam;

/**
 * 获取token接口
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/7/12 15:45
 */
public interface TokenService {
	/**
	 * 获取token
	 * @param tokenParam param
	 * @return string
	 */
	Result<String> getToken(TokenParam tokenParam);
}
