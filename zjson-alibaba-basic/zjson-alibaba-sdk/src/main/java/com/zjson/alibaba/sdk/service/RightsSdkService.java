package com.zjson.alibaba.sdk.service;

import com.zjson.alibaba.sdk.domain.Result;
import com.zjson.alibaba.sdk.domain.dto.EvidenceInfoDTO;
import com.zjson.alibaba.sdk.domain.dto.IssuedDTO;
import com.zjson.alibaba.sdk.domain.vo.EvidenceIssueSdkParam;
import com.zjson.alibaba.sdk.domain.vo.ExternalEvidenceVO;
import com.zjson.alibaba.sdk.domain.vo.ListVo;

import java.util.List;

/**
 *  凭证相关服务
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/6/28 11:34
 */
public interface RightsSdkService {


	/**
	 * 查询可用的凭证列表
	 * @param @param type 类型，1：发行商户，2：使用商户
	 * @param token token
	 * @return list
	 */
	Result<List<ListVo>> getCanUseList(Integer type, String token);

	/**
	 * 将凭证下发给用户
	 * @param param param
	 * @param token token
	 * @return boolean
	 */
	Result<EvidenceIssueSdkParam> issuedToCustomer(IssuedDTO param, String token);

	/**
	 * 根据凭证码获取凭证信息
	 * @param param param
	 * @param token token
	 * @return vo
	 */
	Result<ExternalEvidenceVO> getEvidenceInfo(EvidenceInfoDTO param, String token);


	/**
	 * 完成兑换，通知权益置换中心
	 * @param param param
	 * @param token token
	 * @return boolean
	 */
	Result<Boolean> noticeOver(EvidenceInfoDTO param, String token);

	/**
	 * 撤销凭证
	 * @param evidenceIssuedId 凭证下发Id
	 *                         @param token token
	 * @return
	 */
	Result<String> cancel(String evidenceIssuedId, String token);

}
