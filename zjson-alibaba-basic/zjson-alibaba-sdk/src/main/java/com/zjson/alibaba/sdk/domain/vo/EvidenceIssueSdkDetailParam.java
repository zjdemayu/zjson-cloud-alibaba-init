package com.zjson.alibaba.sdk.domain.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 确认发放请求参数
 *
 * @author zj
 * @date 2021-05-20 09:37:41
 */
@Data
public class EvidenceIssueSdkDetailParam implements Serializable {

    private static final long serialVersionUID = -2228199583644776759L;

    /**
     * 凭证id
     */
    private String evidenceId;

    /**
     * 是否下发成功
     */
    private Boolean isSuccess;
    /**
     * 提示信息
     */
    private String msg;


}
