package com.zjson.alibaba.sdk.constant;

/**
 * 常量
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/6/28 12:22
 */
public interface RequestConstant {


	/**
	 *  客户端
	 */
	String TERMINAL_CLIENT = "2";

	/**
	 * 获取token的uri
	 */
	String GRT_TOKEN_URI = "/auth/v1/getToken";
	/**
	 * 获取凭证列表的uri
	 */
	String GET_CAN_USE_LIST_URI = "/merchant/v1/external/evidence/canUseList?type={type}";
	/**
	 * 获取下发凭证的uri
	 */
	String ISSUED_TO_CUSTOMER_URI = "/merchant/v1/external/issuedToCustomer";
	/**
	 * 获取凭证信息的uri
	 */
	String GET_EVIDENCE_INFO_URI = "/merchant/v1/external/evidence/info";
	/**
	 * 兑换成功，通知凭证的uri
	 */
	String NOTICE_OVER_URI = "/merchant/v1/external/notice/over";
	/**
	 * 撤销凭证uri
	 */
	String CANCEL_URI = "/merchant/v1/external/evidence/cancel?evidenceIssuedId={evidenceIssuedId}";

}
