package com.zjson.alibaba.sdk.domain.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 登录请求参数实体
 *
 * @author admin
 */
@Data
public class TokenParam implements Serializable {

    private static final long serialVersionUID = 8926743436318087324L;

    /**
     * appId
     */
    @NotBlank(message = "请填写appId！")
    private String appId;

    /**
     * appKey
     */
    @NotBlank(message = "请填写appKey！")
    private String appKey;


}
