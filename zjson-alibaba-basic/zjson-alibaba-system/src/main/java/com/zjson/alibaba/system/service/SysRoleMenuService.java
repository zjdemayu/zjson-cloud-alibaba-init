package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.system.dto.RoleAndMenuDTO;
import com.zjson.alibaba.system.dto.RoleMenuVO;
import com.zjson.alibaba.system.entity.SysRoleMenuEntity;

import java.util.Set;

/**
 * 角色功能菜单权限(SysRoleMenu)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:19
 */
public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {


	/**
	 * 权限设置
	 * @param param
	 */
	void save(RoleAndMenuDTO param);

	/**
	 * 根据角色ID查询所有的菜单
	 * @param roleId 角色ID
	 * @return vo
	 */
	RoleMenuVO getMenuIdsByRoleIds(String roleId);

	/**
	 *  根据角色Id查询权限集合
	 * @param roleId 角色Id
	 * @param isAdmin 是否是管理员
	 * @return 权限集合
	 */
	Set<String> getPermissionsByRoleId(String roleId, boolean isAdmin);
}
