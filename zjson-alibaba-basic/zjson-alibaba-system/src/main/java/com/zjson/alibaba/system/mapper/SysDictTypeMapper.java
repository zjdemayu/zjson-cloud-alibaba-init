package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjson.alibaba.system.entity.SysDictTypeEntity;

/**
 * 字典类型(SysDictType)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:19
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictTypeEntity> {

}
