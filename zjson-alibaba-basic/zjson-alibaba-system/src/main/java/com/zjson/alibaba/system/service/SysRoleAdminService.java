package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.system.entity.SysRoleAdminEntity;

/**
 * 角色管理员关系(SysRoleAdmin)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:24
 */
public interface SysRoleAdminService extends IService<SysRoleAdminEntity> {

}
