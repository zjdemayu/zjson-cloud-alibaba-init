package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.system.entity.SysDictTypeEntity;

/**
 * 字典类型(SysDictType)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:18
 */
public interface SysDictTypeService extends IService<SysDictTypeEntity> {

}
