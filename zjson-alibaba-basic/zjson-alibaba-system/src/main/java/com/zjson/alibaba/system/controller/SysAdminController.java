package com.zjson.alibaba.system.controller;

import com.zjson.alibaba.commons.tools.utils.Result;
import com.zjson.alibaba.system.dto.AdminDTO;
import com.zjson.alibaba.system.service.SysAdminService;
import com.zjson.alibaba.system.service.SysMenuService;
import com.zjson.alibaba.system.vo.SysMenuTreeVO;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统用户
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/7/19 16:50
 */
@RestController
@RequestMapping("sysAdmin")
@Api(value = "系统用户", tags = "系统用户")
public class SysAdminController {

	@Resource
	private SysAdminService sysAdminService;
	@Resource
	private SysMenuService sysMenuService;

	@GetMapping("/findAdminByUsername")
	public Result<AdminDTO> resource(@RequestParam(value = "username") String username) {
		return Result.succ(sysAdminService.findAdminByUsername(username));
	}

	@GetMapping("/getRouterUrls")
	Result<List<SysMenuTreeVO>> getRouterUrls(@RequestParam(value = "roleId", required = false) String roleId,
											  @RequestParam("admin")boolean admin) {
		return Result.succ(sysMenuService.getRouterUrls(roleId, admin));
	}

}
