package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zjson.alibaba.commons.mybatis.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 菜单(SysMenu)表实体类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:00
 */
@Data
@TableName("sys_menu")
@EqualsAndHashCode(callSuper = true)
public class SysMenuEntity extends BaseEntity {

    private static final long serialVersionUID = 624264589631864312L;

    /**
     * 上级ID，一级菜单为0
     */
    private String parentId;

    /**
     * 菜单名
     */
    private String name;

    /**
     * 菜单URL
     */
    private String url;

    /**
     * 类型   0：菜单   1：按钮
     */
    private Integer type;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 权限标识，如：sys:menu:save
     */
    private String permissions;

    /**
     * 排序
     */
    private Integer sort;

}
