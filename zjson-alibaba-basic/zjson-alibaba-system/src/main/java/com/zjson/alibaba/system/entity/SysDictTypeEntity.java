package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zjson.alibaba.commons.mybatis.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典类型(SysDictType)表实体类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:18
 */
@Data
@TableName("sys_dict_type")
@EqualsAndHashCode(callSuper = true)
public class SysDictTypeEntity extends BaseEntity {

    private static final long serialVersionUID = 441228204901973779L;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 备注
     */
    private String remark;

    /**
     * 排序
     */
    private Integer sort;

}
