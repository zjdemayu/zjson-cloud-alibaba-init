package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjson.alibaba.system.entity.SysMenuEntity;
import com.zjson.alibaba.system.mapper.SysMenuMapper;
import com.zjson.alibaba.system.service.SysMenuService;
import com.zjson.alibaba.system.vo.SysMenuTreeVO;
import com.zjson.alibaba.commons.tools.tree.TreeUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 菜单(SysMenu)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:00
 */
@Service("sysMenuService")
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenuEntity> implements SysMenuService {

    @Resource
    private SysMenuMapper sysMenuMapper;

    @Override
    public List<SysMenuTreeVO> getRouterUrls(String roleId, boolean admin) {
        List<SysMenuTreeVO> menuTrees;
        if (admin) {
            menuTrees = sysMenuMapper.getAllSysMenu();
        }else {
            menuTrees = sysMenuMapper.getSysMenuByRoleId(roleId);
        }
        return TreeUtil.build(menuTrees, "0");
    }
}
