package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.system.entity.SysLogLoginEntity;

/**
 * 登录日志(SysLogLogin)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:17
 */
public interface SysLogLoginService extends IService<SysLogLoginEntity> {

}
