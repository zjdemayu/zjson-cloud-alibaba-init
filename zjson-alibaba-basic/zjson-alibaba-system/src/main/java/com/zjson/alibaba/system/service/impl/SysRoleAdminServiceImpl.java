package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjson.alibaba.system.entity.SysRoleAdminEntity;
import com.zjson.alibaba.system.mapper.SysRoleAdminMapper;
import com.zjson.alibaba.system.service.SysRoleAdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 角色管理员关系(SysRoleAdmin)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:25
 */
@Service("sysRoleAdminService")
public class SysRoleAdminServiceImpl extends ServiceImpl<SysRoleAdminMapper, SysRoleAdminEntity> implements SysRoleAdminService {

    @Resource
    private SysRoleAdminMapper sysRoleAdminMapper;

}
