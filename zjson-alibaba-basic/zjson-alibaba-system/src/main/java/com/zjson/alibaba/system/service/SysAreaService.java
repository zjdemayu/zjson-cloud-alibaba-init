package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.commons.tools.utils.Result;
import com.zjson.alibaba.system.dto.AreaRspVO;
import com.zjson.alibaba.system.entity.SysAreaEntity;

import java.util.List;

/**
 * 地区码表(SysArea)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:37
 */
public interface SysAreaService extends IService<SysAreaEntity> {

	/**
	 * 获取省市区三级下拉
	 * @return
	 */
	Result<List<AreaRspVO>> getArea();

}
