package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.zjson.alibaba.commons.tools.redis.RedisKeys;
import com.zjson.alibaba.commons.tools.utils.ConvertUtils;
import com.zjson.alibaba.commons.tools.utils.Result;
import com.zjson.alibaba.system.dto.AreaRspVO;
import com.zjson.alibaba.system.entity.SysAreaEntity;
import com.zjson.alibaba.system.mapper.SysAreaMapper;
import com.zjson.alibaba.system.service.SysAreaService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 地区码表(SysArea)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:38
 */
@Service("sysAreaService")
public class SysAreaServiceImpl extends ServiceImpl<SysAreaMapper, SysAreaEntity> implements SysAreaService {

	@Resource
	private RedisTemplate<String, List<AreaRspVO>> redisTemplate;

    @Resource
    private SysAreaMapper sysAreaMapper;

	@Override
	public Result<List<AreaRspVO>> getArea() {
		List<AreaRspVO> list = redisTemplate.opsForValue().get(RedisKeys.getAreaKey());
		if (CollectionUtils.isEmpty(list)) {
			List<AreaRspVO> areaList = getAreaByData();
			if (CollectionUtils.isEmpty(areaList)) {
				return Result.succ(areaList);
			}
			redisTemplate.opsForValue().set(RedisKeys.getAreaKey(), areaList);
			return Result.succ(areaList) ;
		}
		return Result.succ(list);
	}

	/**
	 *  获取所有的区域
	 * @return
	 */
	private List<AreaRspVO> getAreaByData() {
		List<SysAreaEntity> listArea = sysAreaMapper.selectList(new QueryWrapper<>());
		if (listArea.isEmpty()) {
			return Lists.newArrayList();
		}
		List<AreaRspVO> areaList = ConvertUtils.sourceToTarget(listArea, AreaRspVO.class);
		List<AreaRspVO> parent = areaList.stream().filter(x -> x.getParentId() == -1).collect(Collectors.toList());
		parent.forEach(x -> x.setChildren(getChild(x.getId(), areaList)));
		return parent;
	}

	/**
	 * 获取子区域
	 * @param parentId 父ID
	 * @param list 所有list
	 * @return
	 */
	private List<AreaRspVO> getChild(Integer parentId, List<AreaRspVO> list) {
		List<AreaRspVO> collect = list.stream()
				.filter(x -> Objects.equals(parentId, x.getParentId()))
				.collect(Collectors.toList());
		if (!collect.isEmpty()) {
			collect.forEach(x -> x.setChildren(getChild(x.getId(), list)));
		}
		return collect;
	}
}
