package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 地区码表(SysArea)表实体类
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:36
 */
@Data
@TableName("sys_area")
public class SysAreaEntity implements Serializable {

    private static final long serialVersionUID = 721667674784778686L;

    @TableId
    private Integer id;

    /**
     * 地区编码
     */
    private String areacode;

    /**
     * 地区名
     */
    private String areaname;

    /**
     * 地区级别（1:省份province,2:市city,3:区县district）
     */
    private String level;

    /**
     * 城市编码
     */
    private String citycode;

    /**
     * 城市中心点（即：经纬度坐标）
     */
    private String center;

    /**
     * 地区父节点
     */
    private Integer parentId;

}
