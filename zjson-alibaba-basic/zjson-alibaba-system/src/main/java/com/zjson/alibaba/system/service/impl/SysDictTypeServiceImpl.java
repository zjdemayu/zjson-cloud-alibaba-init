package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjson.alibaba.system.entity.SysDictTypeEntity;
import com.zjson.alibaba.system.mapper.SysDictTypeMapper;
import com.zjson.alibaba.system.service.SysDictTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 字典类型(SysDictType)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:18
 */
@Service("sysDictTypeService")
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictTypeEntity> implements SysDictTypeService {

    @Resource
    private SysDictTypeMapper sysDictTypeMapper;

}
