package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.system.entity.SysLogOperationEntity;

/**
 * 操作日志(SysLogOperation)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:31
 */
public interface SysLogOperationService extends IService<SysLogOperationEntity> {

}
