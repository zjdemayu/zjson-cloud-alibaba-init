package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.commons.tools.page.PageData;
import com.zjson.alibaba.system.dto.AdminDTO;
import com.zjson.alibaba.system.entity.SysAdminEntity;

/**
 * 后台管理员信息表(Admin)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-13 11:52:05
 */
public interface SysAdminService extends IService<SysAdminEntity> {

    /**
     * 账户列表
     * @param adminPage 分页信息
     * @param roleId    角色id
     * @param keyword   查询条件
     * @return 账户列表
     */
    PageData<AdminDTO> findPageList(IPage<AdminDTO> adminPage, String roleId, String keyword);

    /** 通过用户名查询
     *
     * @param username 用户名
     * @return AdminDTO
     */
    AdminDTO findAdminByUsername(String username);

    /**
     * 删除管理员
     *
     * @param adminId adminId
     */
    void deleteAdministratorAuthById(String adminId);

    /**
     * 启用禁用账号
     *
     * @param adminId  adminId
     * @param isEnable true 启用，false 禁用
     */
    void enable(String adminId, Boolean isEnable);

    /**
     * 新增/编辑账户
     *
     * @param adminDTO adminDTO
     * @return String
     */
    String saveAdmin(AdminDTO adminDTO);

    /**
     * 根据id查询管理员
     *
     * @param adminId 管理员id
     * @return 管理员信息
     */
    AdminDTO findAdminById(String adminId);

}
