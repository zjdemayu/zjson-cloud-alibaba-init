package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjson.alibaba.system.entity.SysAreaEntity;

/**
 * 地区码表(SysArea)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:39
 */
public interface SysAreaMapper extends BaseMapper<SysAreaEntity> {

}
