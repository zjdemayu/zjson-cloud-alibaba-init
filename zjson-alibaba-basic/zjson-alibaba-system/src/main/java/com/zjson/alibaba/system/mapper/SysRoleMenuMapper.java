package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjson.alibaba.system.entity.SysRoleMenuEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * 角色功能菜单权限(SysRoleMenu)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:20
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenuEntity> {

	/**
	 * 根据角色ID查询所有的菜单
	 * @param roleId 角色ID
	 * @return list
	 */
	List<String> getMenuIdsByRoles(@Param("roleId") String roleId);

	/**
	 *  根据角色Id查询权限集合
	 * @param roleId 角色Id
	 * @return 权限集合
	 */
	Set<String> getPermissionsByRoleId(@Param("roleId") String roleId);
}
