package com.zjson.alibaba;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 系统服务
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/7/19 15:02
 */
@SpringCloudApplication
@EnableFeignClients(basePackages = {"com.zjson"})
@MapperScan("com.zjson.alibaba.*.mapper")
public class SystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemApplication.class);
	}
}
