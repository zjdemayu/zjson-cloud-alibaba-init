package com.zjson.alibaba.system.config;

import com.zjson.alibaba.commons.tools.config.ModuleConfig;
import com.zjson.alibaba.commons.tools.constant.ServerConstant;
import org.springframework.stereotype.Service;

/**
 * 模块配置信息
 */
@Service
public class ModuleConfigImpl implements ModuleConfig {
    @Override
    public String getName() {
        return "system";
    }
}
