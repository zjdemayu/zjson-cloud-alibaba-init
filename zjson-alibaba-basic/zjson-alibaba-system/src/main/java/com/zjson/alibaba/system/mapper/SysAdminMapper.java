package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zjson.alibaba.system.dto.AdminDTO;
import com.zjson.alibaba.system.entity.SysAdminEntity;
import org.apache.ibatis.annotations.Param;

/**
 * 后台管理员信息表(Admin)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-13 11:52:08
 */
public interface SysAdminMapper extends BaseMapper<SysAdminEntity> {

    /**
     * 管理员列表
     *
     * @param adminPage 分页信息
     * @param roleId    角色Id
     * @param keyword   查询条件
     * @return 管理员列表
     */
    IPage<AdminDTO> findPageList(IPage<AdminDTO> adminPage,
								 @Param("roleId") String roleId,
								 @Param("keyword") String keyword);

    /**
     * 通过用户名查询管理员
     *
     * @param username 用户名
     * @return AdminDTO
     */
    AdminDTO findAdminByUsername(@Param("username") String username);

    /**
     * 根据id查询管理员
     *
     * @param adminId 管理员id
     * @return 管理员信息
     */
    AdminDTO findAdminById(@Param("adminId") String adminId);

}
