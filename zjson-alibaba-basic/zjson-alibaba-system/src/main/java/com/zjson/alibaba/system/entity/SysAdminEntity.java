package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zjson.alibaba.commons.mybatis.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 后台管理员信息表(Admin)表实体类
 *
 * @author zengzhi
 * @date 2021-05-13 11:52:03
 */
@Data
@TableName("sys_admin")
@EqualsAndHashCode(callSuper = true)
public class SysAdminEntity extends BaseEntity {

    private static final long serialVersionUID = -30122111565405241L;

    /**
     * 管理员账号
     */
    private String username;

    /**
     * 管理员密码
     */
    private String password;

    /**
     * 管理员姓名
     */
    private String realName;

    /**
     * 管理员联系电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String mail;

    /**
     * 管理员联系电话
     */
    private String headPortrait;

    /**
     * 最后登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastLoginTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 管理员账号状态（0：禁用；1：正常【默认】）
     */
    private Boolean isEnable;


}
