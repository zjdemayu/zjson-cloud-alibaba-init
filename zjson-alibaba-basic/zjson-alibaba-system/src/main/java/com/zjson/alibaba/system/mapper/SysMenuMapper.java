package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjson.alibaba.system.dto.RoleMenuTreeVO;
import com.zjson.alibaba.system.entity.SysMenuEntity;
import com.zjson.alibaba.system.vo.SysMenuTreeVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * 菜单(SysMenu)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:02
 */
public interface SysMenuMapper extends BaseMapper<SysMenuEntity> {

	/**
	 * 取all路由
	 * @return list
	 */
	List<SysMenuTreeVO> getAllSysMenu();

	/**
	 *  根据角色获取路由
	 * @param roleId 角色Id
	 *                @return list
	 */
	List<SysMenuTreeVO> getSysMenuByRoleId(@Param("roleId") String roleId);

	/**
	 * 查询所有菜单
	 * @return list
	 */
	List<RoleMenuTreeVO> selectAllMenu();


	/**
	 * 根據菜單ID集合查询其父id集合
	 * @param menuIds 菜單IDs
	 * @return 父Id集合
	 */
	Set<String> getParentIdsByIds(@Param("menuIds")List<String> menuIds);
}
