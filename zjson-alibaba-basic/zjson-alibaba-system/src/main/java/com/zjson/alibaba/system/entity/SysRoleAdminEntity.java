package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 角色管理员关系(SysRoleAdmin)表实体类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role_admin")
public class SysRoleAdminEntity implements Serializable {

    private static final long serialVersionUID = 690417907758669269L;

    /**
     * id
     */
    @TableId
    private String id;

    /**
     * 登陆账号id
     */
    private String adminId;

    /**
     * 角色id
     */
    private String roleId;

    public SysRoleAdminEntity(String adminId, String roleId) {
        this.adminId = adminId;
        this.roleId = roleId;
    }

}
