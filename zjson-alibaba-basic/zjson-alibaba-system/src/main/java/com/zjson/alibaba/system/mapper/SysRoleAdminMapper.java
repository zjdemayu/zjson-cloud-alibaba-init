package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjson.alibaba.system.entity.SysRoleAdminEntity;

/**
 * 角色管理员关系(SysRoleAdmin)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:20
 */
public interface SysRoleAdminMapper extends BaseMapper<SysRoleAdminEntity> {

}
