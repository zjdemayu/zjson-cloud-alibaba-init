package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjson.alibaba.system.entity.SysDictDataEntity;

/**
 * 字典数据(SysDictData)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:04
 */
public interface SysDictDataMapper extends BaseMapper<SysDictDataEntity> {

}
