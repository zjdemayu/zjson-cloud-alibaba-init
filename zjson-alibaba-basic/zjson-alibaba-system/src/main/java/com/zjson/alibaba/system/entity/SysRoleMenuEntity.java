package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zjson.alibaba.commons.mybatis.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 角色功能菜单权限(SysRoleMenu)表实体类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:19
 */
@Data
@TableName("sys_role_menu")
@NoArgsConstructor
@AllArgsConstructor
public class SysRoleMenuEntity  {

    private static final long serialVersionUID = 932677900587272429L;

    /**
     * id
     */
    @TableId
    private String id;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 菜单id
     */
    private String menuId;

    public SysRoleMenuEntity(String roleId, String menuId) {
        this.roleId = roleId;
        this.menuId = menuId;
    }
}
