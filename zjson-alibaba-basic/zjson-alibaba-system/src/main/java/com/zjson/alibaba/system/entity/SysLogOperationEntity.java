package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作日志(SysLogOperation)表实体类
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:30
 */
@Data
@TableName("sys_log_operation")
public class SysLogOperationEntity implements Serializable {

    private static final long serialVersionUID = -22154686397890928L;

    /**
     * id
     */
    @TableId
    private String id;

    /**
     * 模块名称，如：sys
     */
    private String module;

    /**
     * 用户操作
     */
    private String operation;

    /**
     * 请求URI
     */
    private String requestUri;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 请求Headers
     */
    private String requestHeaders;

    /**
     * 请求参数
     */
    private String requestParams;

    /**
     * 请求时长(毫秒)
     */
    private Integer requestTime;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 操作IP
     */
    private String ip;

    /**
     * 状态  0：失败   1：成功
     */
    private Integer status;

    /**
     * 操作终端类型
     */
    private Integer terminalClient;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

}
