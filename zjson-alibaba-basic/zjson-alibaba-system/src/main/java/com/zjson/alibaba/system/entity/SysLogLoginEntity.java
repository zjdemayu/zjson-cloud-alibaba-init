package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 登录日志(SysLogLogin)表实体类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:16
 */
@Data
@TableName("sys_log_login")
public class SysLogLoginEntity implements Serializable {

    private static final long serialVersionUID = 761309009877170627L;

    /**
     * id
     */
    @TableId
    private String id;

    /**
     * 用户操作   0：用户登录   1：用户退出
     */
    private Integer operation;

    /**
     * 状态  0：失败    1：成功    2：账号已锁定
     */
    private Integer status;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 操作IP
     */
    private String ip;

    /**
     * 操作终端类型
     */
    private Integer terminalClient;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

}
