package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.system.entity.SysDictDataEntity;

/**
 * 字典数据(SysDictData)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:03
 */
public interface SysDictDataService extends IService<SysDictDataEntity> {

}
