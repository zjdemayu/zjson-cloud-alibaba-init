package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.system.vo.SysMenuTreeVO;
import com.zjson.alibaba.system.entity.SysMenuEntity;

import java.util.List;

/**
 * 菜单(SysMenu)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:00
 */
public interface SysMenuService extends IService<SysMenuEntity> {


	/**
	 *  根据角色获取路由
	 * @param roleId 角色Id
	 * @param admin 是否是超级管理员
	 * @return list
	 */
	List<SysMenuTreeVO> getRouterUrls(String roleId, boolean admin);
}
