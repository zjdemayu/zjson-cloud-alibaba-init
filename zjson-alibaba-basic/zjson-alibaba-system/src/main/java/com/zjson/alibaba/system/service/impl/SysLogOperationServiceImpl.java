package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjson.alibaba.system.entity.SysLogOperationEntity;
import com.zjson.alibaba.system.mapper.SysLogOperationMapper;
import com.zjson.alibaba.system.service.SysLogOperationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 操作日志(SysLogOperation)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:33
 */
@Service("sysLogOperationService")
public class SysLogOperationServiceImpl extends ServiceImpl<SysLogOperationMapper, SysLogOperationEntity> implements SysLogOperationService {

    @Resource
    private SysLogOperationMapper sysLogOperationMapper;

}
