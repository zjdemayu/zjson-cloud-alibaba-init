package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.system.entity.SysLogErrorEntity;

/**
 * 异常日志(SysLogError)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:24
 */
public interface SysLogErrorService extends IService<SysLogErrorEntity> {

}
