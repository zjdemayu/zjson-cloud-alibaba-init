package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjson.alibaba.system.dto.RoleDTO;
import com.zjson.alibaba.system.entity.SysRoleAdminEntity;
import com.zjson.alibaba.system.entity.SysRoleEntity;
import com.zjson.alibaba.system.mapper.SysRoleAdminMapper;
import com.zjson.alibaba.system.mapper.SysRoleMapper;
import com.zjson.alibaba.system.service.SysRoleService;
import com.zjson.alibaba.system.vo.RoleVO;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.page.PageData;
import com.zjson.alibaba.commons.tools.utils.ConvertUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * 系统角色(SysRole)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-21 10:08:23
 */
@Service("sysRoleService")
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRoleEntity> implements SysRoleService {

    @Resource
    private SysRoleMapper sysRoleMapper;
    @Resource
	private SysRoleAdminMapper sysRoleAdminMapper;

	@Override
	public PageData<RoleVO> findPage(Page<RoleVO> rolePage, String keyword) {
		rolePage = sysRoleMapper.findPage(rolePage, keyword);
		return PageData.of(rolePage.getTotal(), rolePage.getRecords());
	}

	@Override
	public String save(RoleDTO roleDTO) {
		SysRoleEntity sysRoleEntity = sysRoleMapper.selectOne(Wrappers.<SysRoleEntity>lambdaQuery()
				.ne(StringUtils.isNotBlank(roleDTO.getId()), SysRoleEntity::getId, roleDTO.getId())
				.eq(SysRoleEntity::getName, roleDTO.getName()));
		if (Objects.nonNull(sysRoleEntity)) {
			throw new CustomException("角色名称不能重复");
		}
		SysRoleEntity addRole = ConvertUtils.sourceToTarget(roleDTO, SysRoleEntity.class);
		this.saveOrUpdate(addRole);
		return addRole.getId();
	}

	@Override
	public void deleteById(String roleId) {
		// 角色下有用户，不能删除
		int roleCount = sysRoleAdminMapper.selectCount(Wrappers.<SysRoleAdminEntity>lambdaQuery()
				.eq(SysRoleAdminEntity::getRoleId, roleId));
		if (roleCount>0) {
			throw new CustomException("此角色关联了成员，暂无法删除");
		}
		sysRoleMapper.deleteById(roleId);
	}

	@Override
	public void enable(String roleId, Boolean isEnable) {
		// 禁用时判断
		if (Boolean.FALSE.equals(isEnable)) {
			int roleCount = sysRoleAdminMapper.selectCount(Wrappers.<SysRoleAdminEntity>lambdaQuery()
					.eq(SysRoleAdminEntity::getRoleId, roleId));
			if (roleCount > 0 ) {
				throw new CustomException("此角色关联了成员，暂无法删除");
			}
		}
		// 修改禁用、启用
		sysRoleMapper.update(null, Wrappers.<SysRoleEntity>lambdaUpdate()
				.set(SysRoleEntity::getIsEnable, isEnable)
				.eq(SysRoleEntity::getId, roleId));

	}

}
