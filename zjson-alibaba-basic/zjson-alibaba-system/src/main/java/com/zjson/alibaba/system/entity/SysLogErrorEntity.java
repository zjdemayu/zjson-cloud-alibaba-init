package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 异常日志(SysLogError)表实体类
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:24
 */
@Data
@TableName("sys_log_error")
public class SysLogErrorEntity implements Serializable {

    private static final long serialVersionUID = -28357055963969787L;

    /**
     * id
     */
    @TableId
    private String id;

    /**
     * 模块名称，如：sys
     */
    private String module;

    /**
     * 请求URI
     */
    private String requestUri;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 请求参数
     */
    private String requestParams;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 操作IP
     */
    private String ip;

    /**
     * 异常信息
     */
    private String errorInfo;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

}
