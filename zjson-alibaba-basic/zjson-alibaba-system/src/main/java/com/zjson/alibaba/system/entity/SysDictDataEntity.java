package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zjson.alibaba.commons.mybatis.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典数据(SysDictData)表实体类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:03
 */
@Data
@TableName("sys_dict_data")
@EqualsAndHashCode(callSuper = true)
public class SysDictDataEntity extends BaseEntity {

    private static final long serialVersionUID = 614889370474764421L;

    /**
     * 字典类型ID
     */
    private String dictTypeId;

    /**
     * 字典标签
     */
    private String dictLabel;

    /**
     * 字典值
     */
    private String dictValue;

    /**
     * 备注
     */
    private String remark;

    /**
     * 排序
     */
    private Integer sort;

}
