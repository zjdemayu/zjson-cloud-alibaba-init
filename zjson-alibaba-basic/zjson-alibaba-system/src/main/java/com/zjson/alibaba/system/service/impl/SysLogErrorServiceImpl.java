package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjson.alibaba.system.entity.SysLogErrorEntity;
import com.zjson.alibaba.system.mapper.SysLogErrorMapper;
import com.zjson.alibaba.system.service.SysLogErrorService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 异常日志(SysLogError)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:25
 */
@Service("sysLogErrorService")
public class SysLogErrorServiceImpl extends ServiceImpl<SysLogErrorMapper, SysLogErrorEntity> implements SysLogErrorService {

    @Resource
    private SysLogErrorMapper sysLogErrorMapper;

}
