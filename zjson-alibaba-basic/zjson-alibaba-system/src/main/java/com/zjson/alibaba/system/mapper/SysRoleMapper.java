package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjson.alibaba.system.entity.SysRoleEntity;
import com.zjson.alibaba.system.vo.RoleVO;
import org.apache.ibatis.annotations.Param;

/**
 * 系统角色(SysRole)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-21 10:08:23
 */
public interface SysRoleMapper extends BaseMapper<SysRoleEntity> {

	/**
	 * 分页查询角色列表
	 * @param rolePage page
	 * @param keyword keyword
	 * @return  PageData<RoleVO>
	 */
	Page<RoleVO> findPage(Page<RoleVO> rolePage, @Param("keyword") String keyword);
}
