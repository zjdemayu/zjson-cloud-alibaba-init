package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjson.alibaba.system.entity.SysDictDataEntity;
import com.zjson.alibaba.system.mapper.SysDictDataMapper;
import com.zjson.alibaba.system.service.SysDictDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 字典数据(SysDictData)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:03
 */
@Service("sysDictDataService")
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictDataEntity> implements SysDictDataService {

    @Resource
    private SysDictDataMapper sysDictDataMapper;

}
