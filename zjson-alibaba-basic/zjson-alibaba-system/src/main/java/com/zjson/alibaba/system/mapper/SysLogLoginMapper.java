package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjson.alibaba.system.dto.LogLoginStatisticDTO;
import com.zjson.alibaba.system.entity.SysLogLoginEntity;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * 登录日志(SysLogLogin)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:17
 */
public interface SysLogLoginMapper extends BaseMapper<SysLogLoginEntity> {

    /**
     * 统计门店登录次数
     *
     * @param storeIds       门店id集合
     * @param startDate      开始时间
     * @param endDate        结束时间
     * @return List<LogLoginStatisticDTO>
     */
    List<LogLoginStatisticDTO> findLogLoginStatistics(@Param("storeIds") List<String> storeIds,
													  @Param("startDate") LocalDate startDate,
													  @Param("endDate") LocalDate endDate);

}
