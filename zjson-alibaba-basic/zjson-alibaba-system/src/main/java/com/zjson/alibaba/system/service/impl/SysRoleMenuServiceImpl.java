package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Sets;
import com.zjson.alibaba.system.dto.RoleAndMenuDTO;
import com.zjson.alibaba.system.dto.RoleMenuTreeVO;
import com.zjson.alibaba.system.dto.RoleMenuVO;
import com.zjson.alibaba.system.entity.SysRoleAdminEntity;
import com.zjson.alibaba.system.entity.SysRoleMenuEntity;
import com.zjson.alibaba.system.mapper.SysMenuMapper;
import com.zjson.alibaba.system.mapper.SysRoleAdminMapper;
import com.zjson.alibaba.system.mapper.SysRoleMenuMapper;
import com.zjson.alibaba.system.service.SysRoleMenuService;
import com.zjson.alibaba.commons.tools.constant.Constant;
import com.zjson.alibaba.commons.tools.security.enums.SecurityStatusEnum;
import com.zjson.alibaba.commons.tools.security.util.UserDetailRedisUtils;
import com.zjson.alibaba.commons.tools.tree.TreeUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 角色功能菜单权限(SysRoleMenu)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:19
 */
@Service("sysRoleMenuService")
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenuEntity> implements SysRoleMenuService {

    @Resource
    private SysRoleMenuMapper sysRoleMenuMapper;
    @Resource
	private SysMenuMapper sysMenuMapper;
    @Resource
	private SysRoleAdminMapper sysRoleAdminMapper;
    @Resource
	private UserDetailRedisUtils userDetailRedisUtils;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(RoleAndMenuDTO param) {
		// 删除角色对应的菜单
		sysRoleMenuMapper.delete(Wrappers.<SysRoleMenuEntity>lambdaQuery()
				.eq(SysRoleMenuEntity::getRoleId, param.getRoleId()));
		// 批量添加关联
		if (!CollectionUtils.isEmpty(param.getMenuIds())) {
			// 根據菜單ID集合查询其父id集合
			Set<String> parentIds = sysMenuMapper.getParentIdsByIds(param.getMenuIds());
			parentIds.addAll(param.getMenuIds());
			Set<SysRoleMenuEntity> sysRoleMenus = parentIds.stream()
					.map(menuId -> new SysRoleMenuEntity(param.getRoleId(), menuId))
					.collect(Collectors.toSet());
			this.saveBatch(sysRoleMenus);
		}
		//  角色发生改变，重新登录
		List<SysRoleAdminEntity> roleAdminEntities = sysRoleAdminMapper.selectList(Wrappers.<SysRoleAdminEntity>
				lambdaQuery().eq(SysRoleAdminEntity::getRoleId, param.getRoleId()));
		if (CollectionUtils.isEmpty(roleAdminEntities)) {
			return;
		}
		roleAdminEntities.forEach(entity ->
				userDetailRedisUtils.updateSecurityStatus(entity.getAdminId(), SecurityStatusEnum.AUTH_ROLE_CHANGED));
	}

	@Override
	public RoleMenuVO getMenuIdsByRoleIds(String roleId) {
		// 查询角色所有的菜单
		List<RoleMenuTreeVO> menus = sysMenuMapper.selectAllMenu();
		// 查询角色选中的key
		List<String> keys =sysRoleMenuMapper.getMenuIdsByRoles(roleId);
		RoleMenuVO vo = new RoleMenuVO();
		vo.setMenuTree(TreeUtil.build(menus, "0"));
		vo.setKeys(keys);
		return vo;
	}

	@Override
	public Set<String> getPermissionsByRoleId(String roleId, boolean isAdmin) {
		if (isAdmin) {
			return Sets.newHashSet(Constant.ALL_PERMISSION);
		}
		return sysRoleMenuMapper.getPermissionsByRoleId(roleId);
	}
}
