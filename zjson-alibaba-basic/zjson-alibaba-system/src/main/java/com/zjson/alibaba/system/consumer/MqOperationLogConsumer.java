package com.zjson.alibaba.system.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zjson.alibaba.commons.tools.log.BaseLog;
import com.zjson.alibaba.commons.tools.log.SysLogError;
import com.zjson.alibaba.commons.tools.log.SysLogLogin;
import com.zjson.alibaba.commons.tools.log.SysLogOperation;
import com.zjson.alibaba.commons.tools.log.enums.LogTypeEnum;
import com.zjson.alibaba.commons.tools.utils.ConvertUtils;
import com.zjson.alibaba.commons.tools.utils.JsonUtils;
import com.zjson.alibaba.log.mq.config.MqOperationLogConfig;
import com.zjson.alibaba.system.entity.SysLogErrorEntity;
import com.zjson.alibaba.system.entity.SysLogLoginEntity;
import com.zjson.alibaba.system.entity.SysLogOperationEntity;
import com.zjson.alibaba.system.service.SysLogErrorService;
import com.zjson.alibaba.system.service.SysLogLoginService;
import com.zjson.alibaba.system.service.SysLogOperationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 从MQ队列中获取Log，保存到DB
 *
 * @author wangbing
 * @version v1.0.0
 * @date 2020/6/10 16:53
 */
@Slf4j
@Component
@RabbitListener(queues = MqOperationLogConfig.OPERATION_LOG_QUEUE)
public class MqOperationLogConsumer {

    @Resource
    private SysLogErrorService sysLogErrorService;

    @Resource
    private SysLogLoginService sysLogLoginService;

    @Resource
    private SysLogOperationService sysLogOperationService;

    @RabbitHandler
    public void handle(String data) {
        try {
            JSONObject jsonObject = JSON.parseObject(data);
            Integer type = jsonObject.getInteger("type");
            BaseLog operationLog = null;
            if (Integer.valueOf(LogTypeEnum.LOGIN.value()).equals(type)) {
                operationLog = JsonUtils.jsonToObject(data, SysLogLogin.class);
            } else if (Integer.valueOf(LogTypeEnum.OPERATION.value()).equals(type)) {
                operationLog = JsonUtils.jsonToObject(data, SysLogOperation.class);
            } else if (Integer.valueOf(LogTypeEnum.ERROR.value()).equals(type)) {
                operationLog = JsonUtils.jsonToObject(data, SysLogError.class);
            } else {
                log.warn("无对应的日志处理操作。data={}", data);
            }
            if (operationLog != null) {
                handle(operationLog);
            }
        } catch (Exception e) {
            log.error("操作日志处理失败。data={}", data);
            log.error("操作日志处理失败", e);
        }
    }

    /**
     * 保存操作日志
     *
     * @param operationLog 操作日志信息
     */
    private void handle(BaseLog operationLog) {
        if (operationLog.getType() == LogTypeEnum.LOGIN.value()) {
            //登录日志
            SysLogLoginEntity entity = ConvertUtils.sourceToTarget(operationLog, SysLogLoginEntity.class);
            sysLogLoginService.save(entity);
        } else if (operationLog.getType() == LogTypeEnum.OPERATION.value()) {
            //操作日志
            SysLogOperationEntity entity = ConvertUtils.sourceToTarget(operationLog, SysLogOperationEntity.class);
            sysLogOperationService.save(entity);
        } else if (operationLog.getType() == LogTypeEnum.ERROR.value()) {
            //异常日志
            SysLogErrorEntity entity = ConvertUtils.sourceToTarget(operationLog, SysLogErrorEntity.class);
            sysLogErrorService.save(entity);
        } else {
            log.warn("无对应的日志处理操作。operationLog={}", operationLog);
        }
    }

}
