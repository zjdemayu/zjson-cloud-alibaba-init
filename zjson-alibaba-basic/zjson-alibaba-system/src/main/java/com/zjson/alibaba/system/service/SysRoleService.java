package com.zjson.alibaba.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zjson.alibaba.system.dto.RoleDTO;
import com.zjson.alibaba.system.vo.RoleVO;
import com.zjson.alibaba.commons.tools.page.PageData;
import com.zjson.alibaba.system.entity.SysRoleEntity;

/**
 * 系统角色(SysRole)表服务接口
 *
 * @author zengzhi
 * @date 2021-05-21 10:08:23
 */
public interface SysRoleService extends IService<SysRoleEntity> {

	/**
	 * 分页查询角色列表
	 * @param rolePage page
	 * @param keyword keyword
	 * @return  PageData<RoleVO>
	 */
	PageData<RoleVO> findPage(Page<RoleVO> rolePage, String keyword);

	/**
	 *  编辑修改角色
	 * @param roleDTO param
	 * @return roleId
	 */
	String save(RoleDTO roleDTO);

	/**
	 *   逻辑删除角色信息
	 * @param roleId roleId
	 */
	void deleteById(String roleId);

	/**
	 *  启用、禁用角色
	 * @param roleId 角色ID
	 * @param isEnable 是否启用
	 */
	void enable(String roleId, Boolean isEnable);
}
