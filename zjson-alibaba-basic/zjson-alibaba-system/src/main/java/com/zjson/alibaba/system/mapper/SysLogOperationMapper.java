package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjson.alibaba.system.entity.SysLogOperationEntity;

/**
 * 操作日志(SysLogOperation)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:34
 */
public interface SysLogOperationMapper extends BaseMapper<SysLogOperationEntity> {

}
