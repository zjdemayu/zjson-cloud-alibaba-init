package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjson.alibaba.system.entity.SysLogLoginEntity;
import com.zjson.alibaba.system.mapper.SysLogLoginMapper;
import com.zjson.alibaba.system.service.SysLogLoginService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 登录日志(SysLogLogin)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-20 09:42:17
 */
@Service("sysLogLoginService")
public class SysLogLoginServiceImpl extends ServiceImpl<SysLogLoginMapper, SysLogLoginEntity> implements SysLogLoginService {

    @Resource
    private SysLogLoginMapper sysLogLoginMapper;

}
