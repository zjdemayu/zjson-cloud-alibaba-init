package com.zjson.alibaba.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zjson.alibaba.commons.mybatis.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统角色(SysRole)表实体类
 *
 * @author zengzhi
 * @date 2021-05-21 10:08:22
 */
@Data
@TableName("sys_role")
@EqualsAndHashCode(callSuper = true)
public class SysRoleEntity extends BaseEntity {

    private static final long serialVersionUID = 575387180313894690L;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否是系统默认角色 0：否，1：是'
     */
    private Integer isDefault;

    /**
     * 号状态（0：禁用；1：正常【默认】）
     */
    private Boolean isEnable;

}
