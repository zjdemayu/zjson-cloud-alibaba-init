package com.zjson.alibaba.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.page.PageData;
import com.zjson.alibaba.commons.tools.utils.ConvertUtils;
import com.zjson.alibaba.system.dto.AdminDTO;
import com.zjson.alibaba.system.entity.SysAdminEntity;
import com.zjson.alibaba.system.entity.SysLogOperationEntity;
import com.zjson.alibaba.system.entity.SysRoleAdminEntity;
import com.zjson.alibaba.system.entity.SysRoleEntity;
import com.zjson.alibaba.system.mapper.SysAdminMapper;
import com.zjson.alibaba.system.mapper.SysLogOperationMapper;
import com.zjson.alibaba.system.mapper.SysRoleAdminMapper;
import com.zjson.alibaba.system.mapper.SysRoleMapper;
import com.zjson.alibaba.system.service.SysAdminService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * 后台管理员信息表(Admin)表服务实现类
 *
 * @author zengzhi
 * @date 2021-05-13 11:52:06
 */
@Service("adminService")
public class SysAdminServiceImpl extends ServiceImpl<SysAdminMapper, SysAdminEntity> implements SysAdminService {

    private static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    @Resource
    private SysAdminMapper adminMapper;

    @Resource
    private SysRoleAdminMapper roleAdminMapper;

    @Resource
    private SysRoleMapper roleMapper;

    @Resource
    private SysLogOperationMapper logOperationMapper;

    @Override
    public PageData<AdminDTO> findPageList(IPage<AdminDTO> adminPage, String roleId, String keyword) {
        adminPage = adminMapper.findPageList(adminPage, roleId, keyword);
        return PageData.of(adminPage.getTotal(), adminPage.getRecords());
    }

    @Override
    public AdminDTO findAdminByUsername(String username) {
        return adminMapper.findAdminByUsername(username);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAdministratorAuthById(String adminId) {
        // 校验
        SysAdminEntity adminEntity = adminMapper.selectById(adminId);
        if (Objects.isNull(adminEntity)) {
            throw new CustomException("不存在该管理员！");
        }
        if (adminEntity.getIsEnable()) {
            throw new CustomException("状态为启用，暂无法删除！");
        }
        Integer count = logOperationMapper.selectCount(Wrappers.<SysLogOperationEntity>lambdaQuery()
                .eq(SysLogOperationEntity::getCreateBy, adminId));
        if (count > 0) {
            throw new CustomException("此用户已经操作记录，暂无法删除！");
        }
        adminMapper.deleteById(adminId);
        roleAdminMapper.delete(Wrappers.<SysRoleAdminEntity>lambdaQuery()
                .eq(SysRoleAdminEntity::getAdminId, adminId));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void enable(String adminId, Boolean isEnable) {
        SysAdminEntity adminEntity = new SysAdminEntity();
        adminEntity.setId(adminId);
        adminEntity.setIsEnable(isEnable);
        adminMapper.updateById(adminEntity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String saveAdmin(AdminDTO adminDTO) {
        SysAdminEntity existAdminEntity = adminMapper.selectOne(Wrappers.<SysAdminEntity>lambdaQuery()
                .ne(StringUtils.isNotBlank(adminDTO.getId()), SysAdminEntity::getId, adminDTO.getId())
                .eq(SysAdminEntity::getUsername, adminDTO.getUsername()));
        if (Objects.nonNull(existAdminEntity)) {
            throw new CustomException("账号不可重复！");
        }
        SysRoleEntity roleEntity = roleMapper.selectById(adminDTO.getRoleId());
        if (Objects.isNull(roleEntity)) {
            throw new CustomException("角色不存在！");
        }
        SysAdminEntity adminEntity = ConvertUtils.sourceToTarget(adminDTO, SysAdminEntity.class);
        adminEntity.setPassword(StringUtils.isBlank(adminDTO.getPassword()) ?
                null : PASSWORD_ENCODER.encode(adminDTO.getPassword()));
        this.saveOrUpdate(adminEntity);
        if (StringUtils.isNotBlank(adminDTO.getId())) {
            roleAdminMapper.delete(Wrappers.<SysRoleAdminEntity>lambdaQuery()
                    .eq(SysRoleAdminEntity::getAdminId, adminEntity.getId()));
        }
        roleAdminMapper.insert(new SysRoleAdminEntity(adminEntity.getId(), adminDTO.getRoleId()));

        return adminEntity.getId();
    }

    @Override
    public AdminDTO findAdminById(String adminId) {
        return adminMapper.findAdminById(adminId);
    }

}
