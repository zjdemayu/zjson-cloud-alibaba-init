package com.zjson.alibaba.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjson.alibaba.system.entity.SysLogErrorEntity;

/**
 * 异常日志(SysLogError)表数据库访问层
 *
 * @author zengzhi
 * @date 2021-05-20 09:47:27
 */
public interface SysLogErrorMapper extends BaseMapper<SysLogErrorEntity> {

}
