package com.kafaka.zjsonkafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class ZjsonAlibabaKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZjsonAlibabaKafkaApplication.class, args);
    }
    /**
     * 健康检查
     * @return
     */
    @GetMapping(value = "/health")
    public int healthCheck() {
        return HttpStatus.OK.value();
    }
}
