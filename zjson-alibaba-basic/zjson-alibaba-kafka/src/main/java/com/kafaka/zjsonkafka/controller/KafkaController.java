package com.kafaka.zjsonkafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zj
 * @Date: 2021/10/13 0013 16:14
 * @Description:
 */
@RestController
@RequestMapping("/kafka")
public class KafkaController {

    @Autowired
    private KafkaTemplate<String,Object> kafkaTemplate;

    private static final String TEST_TOPIC = "testTopic";

    @GetMapping("/message/send")
    public boolean send(@RequestParam(defaultValue = "test") String message){
        System.out.println("send_message=" + message);
        kafkaTemplate.send(TEST_TOPIC,message);
        return true;
    }

    @KafkaListener(topics = TEST_TOPIC)
    public void onMessage(String message){
        System.out.println("listener_message=" + message);
    }
}
