package com.zjson.alibaba.auth.service;


import com.zjson.alibaba.auth.domain.dto.LoginParam;
import com.zjson.alibaba.auth.domain.vo.MiniAppLoginVO;
import com.zjson.alibaba.auth.domain.vo.PlatformLoginVO;

/**
 * 功能描述：登录接口
 *
 * @author zz.
 * @date 2021/5/13 16:16
 */
public interface AuthService {

    /**
     * 平台登录
     *
     * @param loginParam loginParam
     * @return PlatformLoginVO
     */
    PlatformLoginVO platformLogin(LoginParam loginParam);


    /**
     * 小程序登录
     *
     * @param loginParam loginParam
     * @return MiniAppLoginVO
     */
    MiniAppLoginVO minAppLogin(LoginParam loginParam);

    /**
     * 退出登录
     *
     * @param userId userId
     */
    void logout(String userId);

}