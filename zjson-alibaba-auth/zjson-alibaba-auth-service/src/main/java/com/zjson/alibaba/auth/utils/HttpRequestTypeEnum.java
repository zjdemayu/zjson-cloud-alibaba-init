package com.zjson.alibaba.auth.utils;

import org.apache.http.client.methods.*;

/**
 * HTTP请求枚举类
 *
 * @author tokyoliu
 * @version 1.0
 * @since 2020/9/10 9:23
 */
public enum HttpRequestTypeEnum {

    /**
     * POST请求
     */
    POST {
        @Override
        public HttpRequestBase getHttpType(String url) {
            return new HttpPost(url);
        }
    },
    /**
     * GET请求
     */
    GET {
        @Override
        public HttpRequestBase getHttpType(String url) {
            return new HttpGet(url);
        }
    },
    /**
     * GET请求
     */
    DELETE {
        @Override
        public HttpRequestBase getHttpType(String url) {
            return new HttpDelete(url);
        }
    },
    /**
     * PUT请求
     */
    PUT {
        @Override
        public HttpRequestBase getHttpType(String url) {
            return new HttpPut(url);
        }
    },
    ;

    public abstract HttpRequestBase getHttpType(String url);
}
