package com.zjson.alibaba.auth.redis;

import com.zjson.alibaba.commons.tools.redis.RedisKeys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 验证码Redis
 *
 * @author yaozou
 */
@Component
public class CaptchaRedis {

    /**
     * 验证码5分钟过期
     */
    private final static long EXPIRE = 60 * 5L;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public void set(String uuid, String captcha) {
        String key = RedisKeys.getLoginCaptchaKey(uuid);
        redisTemplate.opsForValue().set(key, captcha, EXPIRE, TimeUnit.SECONDS);
    }

    public String get(String uuid) {
        String key = RedisKeys.getLoginCaptchaKey(uuid);
        String captcha = (String) redisTemplate.opsForValue().get(key);

        //删除验证码
        if (captcha != null) {
            redisTemplate.delete(key);
        }

        return captcha;
    }

}
