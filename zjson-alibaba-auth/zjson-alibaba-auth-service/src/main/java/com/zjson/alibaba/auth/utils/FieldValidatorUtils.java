package com.zjson.alibaba.auth.utils;

import com.zjson.alibaba.commons.tools.exception.CustomException;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 功能描述: 字段校验规则
 * @author WeiGang.
 * @date 2019/11/29 15:01
 * @version v1.0       
 */
public class FieldValidatorUtils {
    
    /**
     * 功能描述: 支持英文、数字，不支持中文、表情、特殊符号
     * @param jobNumber 
     * @return {@link boolean}
     * @author WeiGang.
     * @date 2019/11/29 15:02
     * @version v1.0       
     */
    public static boolean  jobNumberValidator(String jobNumber){
        String patternStr = "[A-Za-z0-9\\-]*";
        Matcher meq = Pattern.compile(patternStr).matcher(jobNumber);
        if(!meq.matches()){
            throw new CustomException("员工工号仅支持英文、数字，不支持中文、表情、特殊符号");
        }
        return true;
    }

    /**
     * 功能描述: 请输入6-16位数字或字母
     * @param password
     * @return {@link boolean}
     * @author WeiGang.
     * @date 2019/12/5 11:30
     * @version v1.0
     */
    public static boolean  passwordValidator(String password){
        if(StringUtils.isBlank(password)){
            throw new CustomException("密码不能为空,请输入密码");
        }
        String patternStr = "^[0-9a-zA-Z]{6,16}$";
        Matcher meq = Pattern.compile(patternStr).matcher(password);
        if(!meq.matches()){
            throw new CustomException("请输入6-16位数字或字母");
        }
        return true;
    }
}
