package com.zjson.alibaba.auth.service.impl;

import com.zjson.alibaba.auth.domain.dto.LoginParam;
import com.zjson.alibaba.auth.domain.vo.MiniAppLoginVO;
import com.zjson.alibaba.auth.domain.vo.PlatformLoginVO;
import com.zjson.alibaba.auth.enums.AuthErrorCodeEnum;
import com.zjson.alibaba.auth.jwt.JwtProperties;
import com.zjson.alibaba.auth.jwt.JwtUtils;
import com.zjson.alibaba.auth.service.AuthService;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.log.SysLogLogin;
import com.zjson.alibaba.commons.tools.log.enums.LogTypeEnum;
import com.zjson.alibaba.commons.tools.log.enums.LoginOperationEnum;
import com.zjson.alibaba.commons.tools.log.enums.LoginStatusEnum;
import com.zjson.alibaba.commons.tools.log.producer.LogProducer;
import com.zjson.alibaba.commons.tools.security.password.PasswordUtils;
import com.zjson.alibaba.commons.tools.security.user.SecurityUser;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import com.zjson.alibaba.commons.tools.security.util.UserDetailRedisUtils;
import com.zjson.alibaba.commons.tools.utils.HttpContextUtils;
import com.zjson.alibaba.commons.tools.utils.IpUtils;
import com.zjson.alibaba.system.dto.AdminDTO;
import com.zjson.alibaba.system.feign.SysAdminFeignClient;
import com.zjson.alibaba.system.vo.SysMenuTreeVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * 功能描述：认证服务
 *
 * @author zz
 * @date 2021/5/13 9:41
 */
@Slf4j
@Service("authService")
public class AuthServiceImpl implements AuthService {

    @Resource
    private UserDetailRedisUtils userDetailRedisUtils;

    @Resource
    private SysAdminFeignClient sysAdminFeignClient;

    @Resource
    private LogProducer logProducer;

    @Resource
    private JwtUtils jwtUtils;

    @Resource
    private JwtProperties jwtProperties;



    @Override
    public PlatformLoginVO platformLogin(LoginParam loginParam) {
        AdminDTO adminDTO = sysAdminFeignClient.findAdminByUsername(loginParam.getUsername()).getData();
        if (Objects.isNull(adminDTO)) {
            throw new CustomException(AuthErrorCodeEnum.AUTH_NOT_EXIST.getCode(), AuthErrorCodeEnum.AUTH_NOT_EXIST.getDesc());
        }
        if (!adminDTO.getIsEnable()) {
            throw new CustomException(AuthErrorCodeEnum.AUTH_DISABLE.getCode(), AuthErrorCodeEnum.AUTH_DISABLE.getDesc());
        }
        // 密码错误
        if (!PasswordUtils.matches(loginParam.getPassword(), adminDTO.getPassword())) {
            throw new CustomException(AuthErrorCodeEnum.AUTH_PASSWORD_ERROR.getCode(), AuthErrorCodeEnum.AUTH_PASSWORD_ERROR.getDesc());
        }
        // 登录成功，生成token
        String token = jwtUtils.generateToken(adminDTO.getId());
        int expire = jwtProperties.getExpire();
        // 查询路由
        List<SysMenuTreeVO> sysMenuTrees = sysAdminFeignClient.getRouterUrls(adminDTO.getRoleId(), adminDTO.isAdmin()).getData();
        // 保存到Redis
        UserDetail userDetail = UserDetail.builder()
                .id(adminDTO.getId())
                .username(adminDTO.getUsername())
                .password(adminDTO.getPassword())
                .realName(adminDTO.getRealName())
                .headPortrait(adminDTO.getHeadPortrait())
                .phone(adminDTO.getPhone())
                .mail(adminDTO.getMail())
                .roleId(adminDTO.getRoleId())
                .roleName(adminDTO.getRoleName())
                .token(token)
                .build();
        userDetailRedisUtils.set(userDetail, expire);
        //TODO  更新最后登录时间
        //授权信息
        return PlatformLoginVO.builder()
                .id(adminDTO.getId())
                .username(adminDTO.getUsername())
                .realName(adminDTO.getRealName())
                .phone(adminDTO.getPhone())
                .token(token)
                .expire(expire)
                .routerUrls(sysMenuTrees)
                .build();
    }

    @Override
    public MiniAppLoginVO minAppLogin(LoginParam loginParam) {
        return null;
    }


    @Override
    public void logout(String userId) {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        UserDetail user = SecurityUser.getUser();
        //退出日志
        SysLogLogin log = new SysLogLogin();
        log.setType(LogTypeEnum.LOGIN.value());
        log.setOperation(LoginOperationEnum.LOGOUT.value());
        log.setIp(IpUtils.getIpAddr(request));
        log.setUserAgent(Objects.requireNonNull(request).getHeader(HttpHeaders.USER_AGENT));
        log.setIp(IpUtils.getIpAddr(request));
        log.setStatus(LoginStatusEnum.SUCCESS.value());
        log.setCreateBy(Objects.requireNonNull(user).getId());
        log.setCreatorName(user.getUsername());
        // log.setCreateTime(LocalDateTime.now());
        logProducer.saveLog(log);
        userDetailRedisUtils.logout(userId);
    }

}
