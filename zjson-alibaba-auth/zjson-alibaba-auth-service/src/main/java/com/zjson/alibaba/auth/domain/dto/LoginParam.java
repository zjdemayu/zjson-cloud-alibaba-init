package com.zjson.alibaba.auth.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 登录请求参数实体
 *
 * @author admin
 */
@Data
@ApiModel(value = "登录请求参数实体")
public class LoginParam implements Serializable {

    private static final long serialVersionUID = -726827184849927074L;

    @ApiModelProperty(value = "登录账号", required = true)
    @NotBlank(message = "请填写登录账号！")
    private String username;

    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "请填写密码！")
    private String password;


}
