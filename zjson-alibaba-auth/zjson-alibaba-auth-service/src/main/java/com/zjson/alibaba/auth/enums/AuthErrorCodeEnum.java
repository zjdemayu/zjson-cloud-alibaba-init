package com.zjson.alibaba.auth.enums;

import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.exception.ErrorCode;
import lombok.Getter;


/**
 * 错误code码
 *
 * @author zengzhi
 */
@Getter
public enum AuthErrorCodeEnum {
    /**
     * 错误code码
     */
    AUTH_PASSWORD_ERROR(ErrorCode.AUTH_PASSWORD_ERROR, "密码错误！"),
    AUTH_DISABLE(ErrorCode.AUTH_DISABLE, "账号已被禁用！"),
    AUTH_NOT_EXIST(ErrorCode.AUTH_NOT_EXIST, "账号不存在！");

    private final Integer code;

    private final String desc;

    AuthErrorCodeEnum(final Integer code, final String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static AuthErrorCodeEnum parse(Integer errorCode) {
        for (AuthErrorCodeEnum authErrorCodeEnum : values()) {
            if (authErrorCodeEnum.getCode().equals(errorCode)) {
                return authErrorCodeEnum;
            }
        }
        throw new CustomException("非法的手工费类型：" + errorCode);
    }
}
