package com.zjson.alibaba.auth.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 登录请求参数实体
 *
 * @author admin
 */
@Data
@ApiModel(value = "appId、appKey参数实体")
public class TokenParam implements Serializable {

    private static final long serialVersionUID = 8926743436318087324L;

    @ApiModelProperty(value = "appId", required = true)
    @NotBlank(message = "请填写appId！")
    private String appId;

    @ApiModelProperty(value = "appKey", required = true)
    @NotBlank(message = "请填写appKey！")
    private String appKey;


}
