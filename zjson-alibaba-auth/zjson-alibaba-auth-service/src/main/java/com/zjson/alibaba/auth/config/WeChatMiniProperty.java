package com.zjson.alibaba.auth.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 微信小程序配置
 *
 * @author wangbing
 * @version v1.0.0
 * @since 2020/9/24 10:56
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "wechat.mini")
public class WeChatMiniProperty {

    /**
     * APPID
     */
    private String appid;

    /**
     * secret
     */
    private String secret;

    /**
     * auth.code2Session 地址
     */
    private String code2SessionUrl;

    /**
     * 格式化auth.code2Session请求
     *
     * @param jsCode 微信JScode
     * @return 格式化结果
     */
    public String formatCode2SessionUrl(String jsCode) {
        return String.format(code2SessionUrl, jsCode);
    }

}
