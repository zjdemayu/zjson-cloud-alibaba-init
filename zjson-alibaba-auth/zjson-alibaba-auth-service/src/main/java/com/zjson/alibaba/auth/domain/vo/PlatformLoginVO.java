package com.zjson.alibaba.auth.domain.vo;

import com.zjson.alibaba.system.vo.SysMenuTreeVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * 平台登录，返回结果的VO
 *
 * @author zz
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("平台登录，返回结果的VO")
public class PlatformLoginVO implements Serializable {

    private static final long serialVersionUID = 7015584536992647437L;

    @ApiModelProperty(value = "账号表id")
    private String id;

    @ApiModelProperty(value = "登陆账号")
    private String username;

    @ApiModelProperty(value = "管理员姓名")
    private String realName;

    @ApiModelProperty(value = "管理员联系电话")
    private String phone;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "过期时长，单位秒")
    private Integer expire;
    /**
     * 权限列表
     */
    @ApiModelProperty(value = "权限列表")
    private Set<String> permissions;

    @ApiModelProperty(value = "路由urls")
    private List<SysMenuTreeVO> routerUrls;

}
