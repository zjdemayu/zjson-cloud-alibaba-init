package com.zjson.alibaba.auth.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 小程序登录，返回结果的VO
 *
 * @author zz
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("商户登录，返回结果的VO")
public class MiniAppLoginVO implements Serializable {

    private static final long serialVersionUID = -8783234418521147126L;

    @ApiModelProperty(value = "账号表id")
    private String id;

    @ApiModelProperty(value = "登陆账号")
    private String username;

    @ApiModelProperty(value = "商户id")
    private String merchantId;

    @ApiModelProperty(value = "商户名称")
    private String merchantName;

    @ApiModelProperty(value = "门店id")
    private String storeId;

    @ApiModelProperty(value = "门店名称")
    private String storeName;

    @ApiModelProperty(value = "门店Logo")
    private String storeLogo;

    @ApiModelProperty(value = "小程序端，是否有发放权限")
    private Boolean isHasIssuedPermission;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "过期时长，单位秒")
    private Integer expire;

    @ApiModelProperty(value = "phone")
    private String phone;

}
