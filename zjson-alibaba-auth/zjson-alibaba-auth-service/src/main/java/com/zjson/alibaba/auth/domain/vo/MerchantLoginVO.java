package com.zjson.alibaba.auth.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 商户登录，返回结果的VO
 *
 * @author zz
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("商户登录，返回结果的VO")
public class MerchantLoginVO implements Serializable {

    private static final long serialVersionUID = 7895920793892579541L;

    @ApiModelProperty(value = "账号表id")
    private String id;

    @ApiModelProperty(value = "登陆账号")
    private String username;

    @ApiModelProperty(value = "商户id")
    private String merchantId;

    @ApiModelProperty(value = "商户编码")
    private String merchantNum;

    @ApiModelProperty(value = "商户名称")
    private String merchantName;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "过期时长，单位秒")
    private Integer expire;

}
