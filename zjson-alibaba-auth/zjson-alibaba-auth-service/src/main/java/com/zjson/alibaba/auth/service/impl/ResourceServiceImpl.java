package com.zjson.alibaba.auth.service.impl;

import com.zjson.alibaba.auth.jwt.JwtUtils;
import com.zjson.alibaba.auth.service.ResourceService;
import com.zjson.alibaba.commons.tools.constant.Constant;
import com.zjson.alibaba.commons.tools.enums.ErrorCodeEnum;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import com.zjson.alibaba.commons.tools.exception.ErrorCode;
import com.zjson.alibaba.commons.tools.security.enums.UserKillEnum;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import com.zjson.alibaba.commons.tools.security.util.UserDetailRedisUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * 资源服务
 *
 * @author zz
 */
@Slf4j
@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private UserDetailRedisUtils userDetailRedisUtils;

    @Autowired
    private JwtUtils jwtUtils;

    @Override
    public UserDetail resource(String token, String url, String method) {
        // TODO 资源权限判断
        // 获取用户信息
        return getUserDetail(token);
    }

    /**
     * 根据token，获取用户信息
     *
     * @param token 用户token
     * @return 返回用户信息
     */
    private UserDetail getUserDetail(String token) {
        //token为空
        if (StringUtils.isBlank(token)) {
            throw new CustomException(ErrorCode.NOT_NULL, Constant.TOKEN_HEADER);
        }

        //是否过期
        Claims claims = jwtUtils.getClaimByToken(token);
        if (claims == null || jwtUtils.isTokenExpired(claims.getExpiration())) {
            throw new CustomException(ErrorCodeEnum.UNAUTHORIZED.getCode(), ErrorCodeEnum.UNAUTHORIZED.getDesc());
        }

        //获取用户ID
        String userId = claims.getSubject();

        //查询Redis，如果没数据，则保持用户信息到Redis
        UserDetail userDetail = userDetailRedisUtils.get(userId);
        if (userDetail == null) {
            log.info("缓存中无用户[{}]登录信息，强制用户重新登录", userId);
            throw new CustomException(ErrorCodeEnum.UNAUTHORIZED.getCode(), ErrorCodeEnum.UNAUTHORIZED.getDesc());
        }

        //Redis有数据，则判断是否被踢出，如果被踢出，则提示重新登录
        if (userDetail.getKill() == UserKillEnum.YES.getValue()) {
            throw new CustomException(ErrorCodeEnum.UNAUTHORIZED.getCode(), ErrorCodeEnum.UNAUTHORIZED.getDesc());
        }
        // 您的账号已在其他设备登录
        if (StringUtils.isNotBlank(userDetail.getToken()) && !userDetail.getToken().equals(token)) {
            throw new CustomException(ErrorCodeEnum.AUTH_SIGNED_IN_OTHER.getCode(), ErrorCodeEnum.AUTH_SIGNED_IN_OTHER.getDesc());
        }
        Optional.ofNullable(userDetail.getSecurityStatusEnum())
                .ifPresent(securityStatusCodeEnum -> {
                    switch (securityStatusCodeEnum) {
                        //账号信息发生变更，请重新登录
                        case AUTH_INFORMATION_CHANGED:
                            throw new CustomException(securityStatusCodeEnum.getCode(), securityStatusCodeEnum.getDesc());
                            //账号权限发生变更，请重新登录
                        case AUTH_PERMISSION_CHANGED:
                            throw new CustomException(securityStatusCodeEnum.getCode(), securityStatusCodeEnum.getDesc());
                        case AUTH_ROLE_CHANGED:
                            throw new CustomException(securityStatusCodeEnum.getCode(), securityStatusCodeEnum.getDesc());
                        default:
                            throw new CustomException(ErrorCodeEnum.UNAUTHORIZED.getCode(), ErrorCodeEnum.UNAUTHORIZED.getDesc());
                    }
                });
        return userDetail;
    }


}
