package com.zjson.alibaba.auth.config;

import com.zjson.alibaba.commons.tools.constant.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Swagger配置
 * @author
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        List<Parameter> pars = new ArrayList<>();

        ParameterBuilder token = new ParameterBuilder();
        token.name("token").defaultValue("").description("token")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).build();
        // header中的终端类型参数必填
        ParameterBuilder terminalClient = new ParameterBuilder();
        terminalClient.name("terminal_client").defaultValue("0").description("terminal_client")
                .modelRef(new ModelRef("int")).parameterType("header")
                .required(false).build();
        pars.add(token.build());
        pars.add(terminalClient.build());

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //加了ApiOperation注解的类，才生成接口文档
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(pars)
                .directModelSubstitute(java.util.Date.class, String.class)
                .securitySchemes(security());

    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("美源科技")
            .description("登录认证模块")
            .termsOfServiceUrl("http://www.meiy360.com/")
            .version("1.0.0")
            .build();
    }

    private List<ApiKey> security() {
        return newArrayList(
            new ApiKey(Constant.TOKEN_HEADER, Constant.TOKEN_HEADER, "header")
        );
    }

}
