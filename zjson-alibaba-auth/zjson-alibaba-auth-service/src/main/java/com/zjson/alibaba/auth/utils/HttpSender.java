package com.zjson.alibaba.auth.utils;

import com.alibaba.fastjson.JSONObject;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.*;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Map;

/**
 * httpClient请求发送类
 *
 * @author tokyoliu
 * @version 1.0
 * @since 2020/9/10 9:05
 */
@Slf4j
@Component
public class HttpSender {

    private static final int MAX_TIMEOUT = 3000;
    private static final int MAX_TOTAL = 10;
    private static final int ROUTE_MAX_TOTAL = 3;
    private static final int MAX_RETRY = 2;

    private static PoolingHttpClientConnectionManager connMgr;
    private static HttpRequestRetryHandler retryHandler;

    static {
        // 初始化连接池
        poolConfig();
        // 初始化重试机制
        retryHandlerConfig();
    }

    /**
     * 获取HttpClient
     *
     * @return httpClient
     */
    private static CloseableHttpClient getHttpClient() {
        return HttpClients.custom()
                .setConnectionManager(connMgr)
                .setRetryHandler(retryHandler)
                .build();
    }

    /**
     * 请求头和超时时间配置
     *
     * @param httpReqBase 请求配置类
     */
    private static void config(HttpRequestBase httpReqBase) {
        // 配置请求的超时设置
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(MAX_TIMEOUT)
                .setConnectTimeout(MAX_TIMEOUT)
                .setSocketTimeout(MAX_TIMEOUT)
                .build();
        httpReqBase.setConfig(requestConfig);
    }

    /**
     * 设置重试机制
     */
    private static void retryHandlerConfig() {
        retryHandler = (e, excCount, ctx) -> {
            // 超过最大重试次数，就放弃
            if (excCount > MAX_RETRY) {
                return false;
            }
            // 服务器丢掉了链接，就重试
            if (e instanceof NoHttpResponseException) {
                return true;
            }
            // 不重试SSL握手异常
            if (e instanceof SSLHandshakeException) {
                return false;
            }
            // 中断
            if (e instanceof InterruptedIOException) {
                return false;
            }
            // 目标服务器不可达
            if (e instanceof UnknownHostException) {
                return false;
            }
            // 连接超时
            if (e instanceof ConnectTimeoutException) {
                return false;
            }
            // SSL异常
            if (e instanceof SSLException) {
                return false;
            }
            HttpClientContext clientCtx = HttpClientContext.adapt(ctx);
            HttpRequest req = clientCtx.getRequest();
            // 如果是幂等请求，就再次尝试
            return !(req instanceof HttpEntityEnclosingRequest);
        };
    }

    /**
     * 连接池配置
     */
    private static void poolConfig() {
        ConnectionSocketFactory plainsf = PlainConnectionSocketFactory.getSocketFactory();
        LayeredConnectionSocketFactory sslsf = SSLConnectionSocketFactory.getSocketFactory();

        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", plainsf)
                .register("https", sslsf)
                .build();
        //连接池管理器
        connMgr = new PoolingHttpClientConnectionManager(registry);
        //最大连接数
        connMgr.setMaxTotal(MAX_TOTAL);
        //每个路由基础的连接数
        connMgr.setDefaultMaxPerRoute(ROUTE_MAX_TOTAL);
    }

    /**
     * 发送http请求
     *
     * @param reqType 请求方式
     * @param url     请求地址
     * @param headers 请求头
     * @param param   参数(nullable)
     * @return 返回json对象
     * @throws CustomException
     */
    public JSONObject sendHttp(HttpRequestTypeEnum reqType, String url, Map<String, String> headers, Object param)
            throws CustomException {
        HttpRequestBase reqBase = reqType.getHttpType(url);
        log.debug("\n--->>开始向地址[{}]发起 [{}] 请求", url, reqBase.getMethod());
        log.debug("--->>请求头为{}", JSONObject.toJSONString(headers));
        long startTime = System.currentTimeMillis();
        CloseableHttpClient httpClient = getHttpClient();
        // 设置请求url
        config(reqBase);

        // 设置请求头
        if (!CollectionUtils.isEmpty(headers)) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                reqBase.setHeader(entry.getKey(), entry.getValue());
            }
        }

        if (param instanceof String) {
            String paramStr = String.valueOf(param);
            log.info("请求参数为：{}", paramStr);
            ((HttpEntityEnclosingRequest) reqBase).setEntity(
                    new StringEntity(String.valueOf(paramStr), ContentType.create("application/json", "UTF-8")));
        } else if (param instanceof byte[]) {
            log.info("请求参数为文件流");
            byte[] paramBytes = (byte[]) param;
            ((HttpEntityEnclosingRequest) reqBase).setEntity(new ByteArrayEntity(paramBytes));
        }

        // 返回对象
        CloseableHttpResponse res = null;
        // 返回内容
        String resCtx = null;
        try {
            // 执行请求
            res = httpClient.execute(reqBase);
            log.info("http请求完毕，响应状态：{}", res.getStatusLine());

            if (res.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new CustomException("http访问异常:" + res.getStatusLine());
            }

            // 获取请求响应对象和响应entity
            HttpEntity httpEntity = res.getEntity();
            if (httpEntity != null) {
                resCtx = EntityUtils.toString(httpEntity, "utf-8");
                log.info("http请求返回：{}", resCtx);
            }

        } catch (Exception e) {
            throw new CustomException("http请求异常", e);
        } finally {
            if (res != null) {
                try {
                    res.close();
                } catch (IOException e) {
                    throw new CustomException("关闭请求响应失败", e);
                }
            }
        }
        long endTime = System.currentTimeMillis();
        log.info("请求执行完毕，耗费时长：{} ms", (endTime - startTime));
        return JSONObject.parseObject(resCtx);
    }

    /**
     * 下载文件
     *
     * @param downloadUrl 下载地址
     * @return 文件数据
     */
    public byte[] getFileBytes(String downloadUrl) {
        try {
            URL url = new URL(downloadUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5 * 1000);
            InputStream inStream = conn.getInputStream();
            return readInputStream(inStream);
        } catch (Exception e) {
            throw new CustomException("下载文件失败");
        }
    }

    private byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }
}
