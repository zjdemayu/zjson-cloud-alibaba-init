package com.zjson.alibaba.auth.contrller;

import com.zjson.alibaba.auth.domain.dto.LoginParam;
import com.zjson.alibaba.auth.domain.vo.MiniAppLoginVO;
import com.zjson.alibaba.auth.domain.vo.PlatformLoginVO;
import com.zjson.alibaba.auth.service.AuthService;
import com.zjson.alibaba.auth.service.ResourceService;
import com.zjson.alibaba.commons.tools.annotation.LogOperation;
import com.zjson.alibaba.commons.tools.constant.Constant;
import com.zjson.alibaba.commons.tools.log.enums.LogTypeEnum;
import com.zjson.alibaba.commons.tools.security.user.SecurityUser;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import com.zjson.alibaba.commons.tools.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 登录授权
 *
 * @author zz
 */
@RestController
@RequestMapping("")
@Api(value = "登录授权", tags = "登录授权")
public class AuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private ResourceService resourceService;

    @GetMapping("/resource")
    public Result<UserDetail> resource(@RequestParam(value = "token", required = false) String token,
                                       @RequestParam("url") String url, @RequestParam("method") String method) {
        return Result.succ(resourceService.resource(token, url, method));
    }

    /**
     * 平台登录
     *
     * @param loginParam loginParam
     * @return PlatformLoginVO
     */
    @PostMapping(value = "/v1/platformLogin")
    @ApiOperation(value = "【v1.0.0 zz】平台端密码登录")
    @LogOperation(type = LogTypeEnum.LOGIN, value = "0")
    public Result<PlatformLoginVO> platformLogin(@RequestBody LoginParam loginParam) {
        return Result.succ(authService.platformLogin(loginParam));
    }


    /**
     * 小程序登录
     *
     * @param loginParam loginParam
     * @return MiniAppLoginVO
     */
    @PostMapping(value = "/v1/minAppLogin")
    @ApiOperation(value = "【v1.0.0 zz】小程序密码登录")
    @LogOperation(type = LogTypeEnum.LOGIN, value = "0")
    public Result<MiniAppLoginVO> minAppLogin(@RequestBody LoginParam loginParam) {
        return Result.succ(authService.minAppLogin(loginParam));
    }


    /**
     * 退出登录
     *
     * @param request request
     * @return Result<Boolean>
     */
    @DeleteMapping(value = "/v1/logout")
    @LogOperation(type = LogTypeEnum.LOGIN, value = "1")
    @ApiOperation(value = "退出登录")
    public Result<Boolean> logout(HttpServletRequest request) {
        String userId = request.getHeader(Constant.USER_KEY);
        authService.logout(userId);
        return Result.succ(Boolean.TRUE);
    }

    @GetMapping("/test")
    public String test() {
        UserDetail user = SecurityUser.getUser();
        return user.getId();
    }

}
