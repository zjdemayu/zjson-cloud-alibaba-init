package com.zjson.alibaba.system.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author zahng jie
 * @description
 * @date 2019/10/17 14:02
 */
@Data
@ApiModel("商户列表查询模型")
public class AreaRspVO {

    @ApiModelProperty(value = "地区编码")
    private String areacode;

    @ApiModelProperty(value = "地区名")
    private String areaname;

    @ApiModelProperty(value = "子节点")
    private List<AreaRspVO> children;

    @ApiModelProperty(value = "区域表id")
    @JsonIgnore
    private Integer id;

    @ApiModelProperty(value = "地区父节点")
    @JsonIgnore
    private Integer parentId;
}
