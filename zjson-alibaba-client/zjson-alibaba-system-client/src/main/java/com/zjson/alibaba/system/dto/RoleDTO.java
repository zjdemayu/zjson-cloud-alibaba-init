package com.zjson.alibaba.system.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 系统角色(SysRole)表实体类
 *
 * @author zengzhi
 * @date 2021-05-21 10:08:22
 */
@Data
public class RoleDTO implements Serializable {

    private static final long serialVersionUID = 575387180313894690L;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id,编辑时传")
    private String id;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称", required = true)
    @NotBlank(message = "角色名称不能为空")
    @Length(max = 20, message = "角色名称，不超过20字符")
    private String name;

    /**
     * 备注
     */
    @ApiModelProperty(value = "角色描述", required = true)
    @Length(max = 100, message ="角色描述，不超过100字符")
    private String remark;

    /**
     * 是否是系统默认角色 0：否，1：是'
     */
    @JsonIgnore
    private Integer isDefault;

    /**
     * 号状态（0：禁用；1：正常【默认】）
     */
    @JsonIgnore
    private Boolean isEnable;

}
