package com.zjson.alibaba.system.feign;

import com.zjson.alibaba.commons.tools.constant.ServerConstant;
import com.zjson.alibaba.commons.tools.utils.Result;
import com.zjson.alibaba.system.dto.AdminDTO;
import com.zjson.alibaba.system.feign.impl.SysAdminFeignClientFallbackFactory;
import com.zjson.alibaba.system.vo.SysMenuTreeVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * sysAdmin
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/7/19 14:43
 */
@FeignClient(name = ServerConstant.ZJSON_ALIBABA_SYSTEM, fallbackFactory = SysAdminFeignClientFallbackFactory.class)
public interface SysAdminFeignClient {

	/**
	 *  保存 admin
	 * @param adminDTO dto
	 * @return result
	 */
	@PostMapping(value = "/sysAdmin/saveAdmin")
	Result<String> save(@RequestBody AdminDTO adminDTO);

	/**
	 * 通过用户名查询用户
	 * @param username 用户名
	 * @return result
	 */
	@GetMapping(value = "/sysAdmin/findAdminByUsername")
	Result<AdminDTO> findAdminByUsername(@RequestParam("username") String username);

	/**
	 * 获取路由
	 * @param roleId 角色id
	 * @param admin admin
	 * @return result
	 */
	@GetMapping(value = "/sysAdmin/getRouterUrls")
	Result<List<SysMenuTreeVO>> getRouterUrls(@RequestParam(value = "roleId", required = false) String roleId,
											  @RequestParam("admin") boolean admin);
}
