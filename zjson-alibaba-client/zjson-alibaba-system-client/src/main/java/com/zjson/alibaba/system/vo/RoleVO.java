package com.zjson.alibaba.system.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统角色(SysRole)表实体类
 *
 * @author zj
 * @date 2021-05-21 10:08:22
 */
@Data
@ApiModel("角色返回模型")
public class RoleVO implements Serializable {

    private static final long serialVersionUID = -5744706614599839106L;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id")
    private String id;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String name;

    /**
     * 备注
     */
    @ApiModelProperty(value = "职能描述")
    private String remark;

    /**
     * 角色数量
     */
    @ApiModelProperty(value = "角色数量")
    private Integer roleCount;

    @ApiModelProperty(value = "添加时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 状态（0：禁用；1：正常【默认】）
     */
    @ApiModelProperty(value = "是否启用")
    private Boolean isEnable;

}
