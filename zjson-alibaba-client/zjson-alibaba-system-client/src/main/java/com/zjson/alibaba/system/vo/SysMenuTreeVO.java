package com.zjson.alibaba.system.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zjson.alibaba.commons.tools.tree.TreeNode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 系路由tree返回模型
 *
 * @author zj
 * @date 2021-05-21 10:08:22
 */
@Data
@ApiModel("路由tree返回模型")
public class SysMenuTreeVO extends TreeNode<SysMenuTreeVO> {

    private static final long serialVersionUID = 2250147650515671804L;
    /**
     * 菜单id
     */
    @JsonIgnore
    private String id;
    /**
     * 菜单parentId
     */
    @JsonIgnore
    private String parentId;
    /**
     * 菜单名
     */
    @ApiModelProperty(value = "菜单名")
    private String name;

    @ApiModelProperty(value = "路由Url")
    private String url;

}
