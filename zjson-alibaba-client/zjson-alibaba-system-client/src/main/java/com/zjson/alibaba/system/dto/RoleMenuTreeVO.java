package com.zjson.alibaba.system.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zjson.alibaba.commons.tools.tree.TreeNode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 角色菜单Vo
 * @author zj
 * @date 2021-05-20 09:42:00
 */
@Data
public class RoleMenuTreeVO extends TreeNode<RoleMenuTreeVO>  implements Serializable {


    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "菜单ID")
    private String id;
    /**
     * 上级ID，一级菜单为0
     */
    @JsonIgnore
    private String parentId;

    /**
	 * 菜单名
     */
    @ApiModelProperty(value = "菜单名")
    private String name;



}
