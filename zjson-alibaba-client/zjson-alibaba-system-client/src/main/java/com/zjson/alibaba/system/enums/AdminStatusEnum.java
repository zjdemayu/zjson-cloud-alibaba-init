package com.zjson.alibaba.system.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import lombok.Getter;


/**
 * 管理员状态
 *
 * @author zengzhi
 */
@Getter
public enum AdminStatusEnum {
    /**
     * 0：启用
     */
    NORMAL(0, "启用"),
    /**
     * 1：禁用
     */
    FORBIDDEN(1, "禁用");

    private final Integer value;

    @JsonValue
    private final String desc;

    AdminStatusEnum(final Integer value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static AdminStatusEnum parse(Integer handworkFeeType) {
        for (AdminStatusEnum type : values()) {
            if (type.getValue().equals(handworkFeeType)) {
                return type;
            }
        }
        throw new CustomException("非法的手工费类型：" + handworkFeeType);
    }
}
