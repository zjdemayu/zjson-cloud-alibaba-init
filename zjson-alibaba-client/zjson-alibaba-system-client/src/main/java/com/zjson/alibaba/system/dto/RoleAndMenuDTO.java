package com.zjson.alibaba.system.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * 系统角色(SysRole)表实体类
 *
 * @author zengzhi
 * @date 2021-05-21 10:08:22
 */
@Data
public class RoleAndMenuDTO implements Serializable {

    private static final long serialVersionUID = 9209987474102294306L;
    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id",required = true)
    @NotBlank(message = "角色不能为空")
    private String roleId;

    /**
     * 菜单id集合
     */
    @ApiModelProperty(value = "菜单id集合", required = true)
    private List<String> menuIds;

}
