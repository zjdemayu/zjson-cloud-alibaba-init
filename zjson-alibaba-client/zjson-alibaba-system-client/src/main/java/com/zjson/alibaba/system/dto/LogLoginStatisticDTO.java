package com.zjson.alibaba.system.dto;

import lombok.Data;

/**
 * 门店登录次数统计
 *
 * @author zengzhi
 * @version 1.0
 * @since 2021/6/28 17:23
 */
@Data
public class LogLoginStatisticDTO {

    /**
     * 门店Id
     */
    private String storeId;

    /**
     * 登录次数
     */
    private Integer loginTimes;

}
