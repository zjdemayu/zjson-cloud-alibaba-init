package com.zjson.alibaba.system.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.zjson.alibaba.commons.tools.exception.CustomException;
import lombok.Getter;


/**
 * 性别
 *
 * @author zengzhi
 */
@Getter
public enum GenderEnum {
    /**
     * 0：男
     */
    MALE(0, "男"),
    /**
     * 1：女
     */
    FEMALE(1, "女"),
    /**
     * 2：保密
     */
    PRIVACY(2, "保密");

    private final Integer value;

    @JsonValue
    private final String desc;

    GenderEnum(final Integer value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static GenderEnum parse(Integer handworkFeeType) {
        for (GenderEnum type : values()) {
            if (type.getValue().equals(handworkFeeType)) {
                return type;
            }
        }
        throw new CustomException("非法的手工费类型：" + handworkFeeType);
    }
}
