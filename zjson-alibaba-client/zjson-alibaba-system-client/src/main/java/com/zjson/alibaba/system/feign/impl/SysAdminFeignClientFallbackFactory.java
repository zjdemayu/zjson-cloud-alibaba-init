package com.zjson.alibaba.system.feign.impl;

import com.zjson.alibaba.commons.tools.utils.Result;
import com.zjson.alibaba.system.dto.AdminDTO;
import com.zjson.alibaba.system.feign.SysAdminFeignClient;
import com.zjson.alibaba.system.vo.SysMenuTreeVO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * SysAdmin
 *
 * @author Zhang Jie
 * @version 1.0.0 @since 2021/7/19 14:44
 */
@Slf4j
@Component
public class SysAdminFeignClientFallbackFactory implements FallbackFactory<SysAdminFeignClient> {
	@Override
	public SysAdminFeignClient create(Throwable throwable) {
		log.error("调用[系统]服务失败！", throwable);
		return new SysAdminFeignClient() {
			@Override
			public Result<String> save(AdminDTO adminDTO) {
				return Result.fail();
			}

			@Override
			public Result<AdminDTO> findAdminByUsername(String username) {
				return Result.fail();
			}

			@Override
			public Result<List<SysMenuTreeVO>> getRouterUrls(String roleId, boolean admin) {
				return Result.fail();
			}
		};
	}
}
