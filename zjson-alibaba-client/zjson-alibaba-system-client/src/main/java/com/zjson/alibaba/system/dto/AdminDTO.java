package com.zjson.alibaba.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zjson.alibaba.commons.tools.constant.Constant;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 后台管理员信息
 *
 * @author zengzhi
 * @date 2021-05-13 11:52:03
 */
@Data
public class AdminDTO implements Serializable {

    private static final long serialVersionUID = -2741010664339095994L;

    private String id;

    /**
     * 管理员账号
     */
    private String username;

    /**
     * 管理员密码
     */
    private String password;

    /**
     * 管理员姓名
     */
    private String realName;

    /**
     * 管理员联系电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String mail;

    /**
     * 头像
     */
    private String headPortrait;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 管理员账号状态（0：禁用；1：正常【默认】）
     */
    private Boolean isEnable;

    /**
     * 备注
     */
    private String remark;

    /**
     * 最后登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastLoginTime;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 是否是超级管理员
     *
     * @return boolean
     */
    public boolean isAdmin() {
        return Constant.SUPER_ADMIN_ID.equals(this.id);
    }

}
