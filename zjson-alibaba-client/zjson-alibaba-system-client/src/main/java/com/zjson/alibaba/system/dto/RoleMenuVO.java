package com.zjson.alibaba.system.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 角色菜单Vo
 * @author zj
 * @date 2021-05-20 09:42:00
 */
@Data
public class RoleMenuVO  implements Serializable {

    private static final long serialVersionUID = -5133849633602892467L;
    @ApiModelProperty(value = "菜单ID")
    private List<String> keys;
    /**
	 * 菜单名
     */
    @ApiModelProperty(value = "菜单名")
    private List<RoleMenuTreeVO> menuTree;



}
