package com.zjson.alibaba.gateway.feign;

import com.zjson.alibaba.commons.tools.constant.Constant;
import com.zjson.alibaba.commons.tools.constant.ServerConstant;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import com.zjson.alibaba.commons.tools.utils.Result;
import com.zjson.alibaba.gateway.feign.fallback.ResourceFeignClientFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 资源接口
 */
@FeignClient(name = ServerConstant.ZJSON_ALIBABA_SYSTEM, fallbackFactory = ResourceFeignClientFallbackFactory.class)
public interface ResourceFeignClient {

    /**
     * 是否有资源访问权限
     *
     * @param token  token
     * @param url    资源URL
     * @param method 请求方式
     * @return 有访问权限，则返回用户信息
     */
    @GetMapping("/resource")
    Result<UserDetail> resource(@RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) String language,
                                @RequestParam("token") String token,
                                @RequestParam("url") String url, @RequestParam("method") String method,
                                @RequestHeader(Constant.TERMINAL_CLIENT) String terminalClient);

}
