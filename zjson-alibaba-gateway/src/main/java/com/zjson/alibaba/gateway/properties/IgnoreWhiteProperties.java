package com.zjson.alibaba.gateway.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 白名单配置
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/5/13 10:25
 */
@Getter
@Setter
@Configuration
@RefreshScope
@ConfigurationProperties(prefix = "rights.ignore")
public class IgnoreWhiteProperties {

    private List<String> uris;

}
