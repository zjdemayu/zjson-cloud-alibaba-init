package com.zjson.alibaba.gateway.config;

import feign.codec.Decoder;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * GateWay的feign配置
 *
 * @author zz
 */
@SpringBootConfiguration
public class GatewayFeignConfig {

    @Bean
    public Decoder feignDecoder() {
        return new ResponseEntityDecoder(new SpringDecoder(() ->
                new HttpMessageConverters(new GateWayMappingJackson2HttpMessageConverter())));
    }

    private static class GateWayMappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {

        GateWayMappingJackson2HttpMessageConverter() {
            List<MediaType> mediaTypes = new ArrayList<>();
            mediaTypes.add(MediaType.valueOf(MediaType.TEXT_HTML_VALUE + ";charset=UTF-8"));
            setSupportedMediaTypes(mediaTypes);
        }

    }

}