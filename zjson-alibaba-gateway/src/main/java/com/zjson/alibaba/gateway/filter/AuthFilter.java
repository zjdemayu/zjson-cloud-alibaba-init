package com.zjson.alibaba.gateway.filter;

import com.alibaba.fastjson.JSON;
import com.zjson.alibaba.gateway.feign.ResourceFeignClient;
import com.zjson.alibaba.commons.tools.constant.Constant;
import com.zjson.alibaba.commons.tools.enums.ErrorCodeEnum;
import com.zjson.alibaba.commons.tools.exception.ErrorCode;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import com.zjson.alibaba.commons.tools.utils.Result;
import com.zjson.alibaba.gateway.properties.IgnoreWhiteProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.List;

/**
 * 权限过滤器
 */
@Slf4j
@Configuration
@ConfigurationProperties(prefix = "saas")
public class AuthFilter implements GlobalFilter, Ordered {

    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    private final ResourceFeignClient resourceFeignClient;

    /**
     * 不拦截的uris
     */
    private final List<String> ignoreUris;

    public AuthFilter(ResourceFeignClient resourceFeignClient, IgnoreWhiteProperties ignoreWhiteProperties) {
        this.resourceFeignClient = resourceFeignClient;
        this.ignoreUris = ignoreWhiteProperties.getUris();
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String requestUri = request.getPath().pathWithinApplication().value();
        // 获取用户token
        String token = getRequestToken(request);
        String userId = Strings.EMPTY;
        // xxl-job调用
        if (jobMatcher(token, request.getHeaders().getFirst("timestamp"))) {
            return chain.filter(exchange);
        }
        boolean isIgnoreUrl = pathMatcher(requestUri);
        // 请求终端
        String terminalClient = request.getHeaders().getFirst(Constant.TERMINAL_CLIENT);
        if (StringUtils.isNoneBlank(token)) {
            // 资源访问权限
            String language = request.getHeaders().getFirst(HttpHeaders.ACCEPT_LANGUAGE);
            Result<UserDetail> result = resourceFeignClient.resource(language, token, requestUri, request.getMethod().toString(), terminalClient);
            // 没权限访问，直接返回
            if (!result.success()) {
                // 用户[Token]登录已过期但请求非必须登录API，访问放行
                boolean isDirectReturn = !isIgnoreUrl || result.getCode() != ErrorCode.UNAUTHORIZED;
                if (isDirectReturn) {
                    return response(exchange, result);
                }
                if (log.isDebugEnabled()) {
                    log.debug("用户[Token]登录已过期但请求非必须登录API，访问放行");
                }
            } else {
                // 获取用户信息
                UserDetail userDetail = result.getData();
                if (userDetail != null) {
                    // 当前登录用户userId，添加到header中
                    userId = userDetail.getId();
                }
            }
        }
        if (StringUtils.isNoneBlank(userId)) {
            ServerHttpRequest build = exchange.getRequest().mutate().header(Constant.USER_KEY, new String[]{userId}).build();
            return chain.filter(exchange.mutate().request(build).build());
        }
        // 请求放行，无需验证权限
        if (isIgnoreUrl) {
            return chain.filter(exchange);
        }
        return response(exchange, Result.fail(ErrorCodeEnum.UNAUTHORIZED.getCode(), ErrorCodeEnum.UNAUTHORIZED.getDesc()));
    }

    private boolean jobMatcher(String token, String timestamp) {
        String nowToken = md5((timestamp + "job"));
        return StringUtils.isNotEmpty(timestamp)
                && StringUtils.isNotEmpty(token)
                && token.equals(nowToken);
    }

    private String md5(String source) {
        StringBuilder sb = new StringBuilder(32);

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(source.getBytes(StandardCharsets.UTF_8));

            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).toUpperCase(), 1, 3);
            }
        } catch (Exception e) {
            return null;
        }

        return sb.toString();
    }

    private Mono<Void> response(ServerWebExchange exchange, Object object) {
        String json = JSON.toJSONString(object);
        DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(json.getBytes(StandardCharsets.UTF_8));
        exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        exchange.getResponse().setStatusCode(HttpStatus.OK);
        return exchange.getResponse().writeWith(Flux.just(buffer));
    }

    private boolean pathMatcher(String requestUri) {
        for (String url : ignoreUris) {
            if (antPathMatcher.match(url, requestUri)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取用户token
     *
     * @param request 请求信息
     * @return token
     */
    private String getRequestToken(ServerHttpRequest request) {
        String token = request.getHeaders().getFirst(Constant.TOKEN_HEADER);
        if (StringUtils.isBlank(token)) {
            token = request.getQueryParams().getFirst(Constant.TOKEN_HEADER);
        }
        if (StringUtils.isBlank(token)) {
            HttpCookie tokenMap = request.getCookies().getFirst(Constant.TOKEN_HEADER);
            if (tokenMap != null && StringUtils.isNotBlank(tokenMap.getValue())) {
                token = tokenMap.getValue();
            }
        }
        return token;
    }

    @Override
    public int getOrder() {
        return 1;
    }

}
