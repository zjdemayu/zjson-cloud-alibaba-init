package com.zjson.alibaba.gateway.feign.fallback;

import com.zjson.alibaba.gateway.feign.ResourceFeignClient;
import com.zjson.alibaba.commons.tools.security.user.UserDetail;
import com.zjson.alibaba.commons.tools.utils.Result;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 资源接口 Fallback
 */
@Slf4j
@Component
public class ResourceFeignClientFallbackFactory implements FallbackFactory<ResourceFeignClient> {

    @Override
    public ResourceFeignClient create(Throwable throwable) {
        log.error("调用权限认证服务失败", throwable);
        return new ResourceFeignClient() {

            @Override
            public Result<UserDetail> resource(String language, String token, String url, String method, String terminalClient) {
                return Result.fail();
            }
        };
    }

}
