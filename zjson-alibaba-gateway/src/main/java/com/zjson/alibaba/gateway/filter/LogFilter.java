package com.zjson.alibaba.gateway.filter;

import brave.Tracer;
import com.zjson.alibaba.commons.tools.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.List;

/**
 * 日志过滤，用于打印完整请求参数
 * created on 2020/12/7 19:49
 *
 * @author yaozou
 * @since v1.0.0
 */
@Slf4j
@Component
public class LogFilter implements GlobalFilter, Ordered {

    /**
     * 未知常量
     */
    private static final String UNKNOWN = "unknown";

    private static final String TRACE_ID = "Trace-Id";

    @Resource
    private Tracer tracer;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        exchange.getResponse().getHeaders().add(TRACE_ID, tracer.currentSpan().context().traceIdString());
        logRequestInfo(request);
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }

    /**
     * 获取请求真实IP地址
     *
     * @param request HttpServletRequest
     * @return IP地址
     */
    public static String getRealIp(ServerHttpRequest request) {
        // 这个一般是Nginx反向代理设置的参数
        HttpHeaders headers = request.getHeaders();
        String ip = null;
        List<String> ips = headers.get("X-Real-IP");
        if (!CollectionUtils.isEmpty(ips)) {
            ip = ips.get(0);
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ips = headers.get("X-Forwarded-For");
            if (!CollectionUtils.isEmpty(ips)) {
                ip = ips.get(0);
            }
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ips = headers.get("Proxy-Client-IP");
            if (!CollectionUtils.isEmpty(ips)) {
                ip = ips.get(0);
            }
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ips = headers.get("WL-Proxy-Client-IP");
            if (!CollectionUtils.isEmpty(ips)) {
                ip = ips.get(0);
            }
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            if (request.getRemoteAddress() != null) {
                ip = request.getRemoteAddress().toString();
            }
        }
        // 处理多IP的情况（只取第一个IP）
        if (ip != null && ip.contains(",")) {
            String[] ipArray = ip.split(",");
            ip = ipArray[0];
        }
        return ip;
    }

    /**
     * LOG 请求信息
     *
     * @param request 请求信息
     */
    private void logRequestInfo(ServerHttpRequest request) {
        if (log.isDebugEnabled()) {
            String method = request.getMethodValue();
            String contentType = request.getHeaders().getFirst("Content-Type");
            log.debug("request-ip:{}", getRealIp(request));
            log.debug("request-uri:{} {} {}", method, contentType, request.getURI());
            if (!CollectionUtils.isEmpty(request.getHeaders())) {
                log.debug("request-headers:{}", JsonUtils.objectToJson(request.getHeaders()));
            }
            if (!CollectionUtils.isEmpty(request.getQueryParams())) {
                log.debug("request-params:{}", JsonUtils.objectToJson(request.getQueryParams()));
            }
        }
    }

}
