package com.zjson.alibaba;

import cn.hutool.http.HttpStatus;
import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.registry.NacosRegistration;
import com.alibaba.cloud.nacos.registry.NacosServiceRegistry;
import com.zjson.alibaba.gateway.filter.LogFilter;
import com.zjson.alibaba.commons.tools.constant.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;

import javax.annotation.Resource;

/**
 * 服务下线 与 健康检查
 *
 * @author Zhang Jie
 * @version 1.0.0
 * @since 2021/5/20 10:39
 */
@RestController
@RequestMapping("check")
@Slf4j
public class CheckController {

	 @Resource
	 private NacosDiscoveryProperties nacosDiscoveryProperties;
	@Resource
	private NacosRegistration nacosRegistration;
	@Resource
	private NacosServiceRegistry nacosServiceRegistry;


	/**
	 *  服务下线
	 * @return HttpStatus
	 */
	@GetMapping(value = "/deregister")
	public int deregisterInstance(ServerWebExchange exchange) {
		String realIp = getRealIp(exchange);
		if (!Constant.LOCALHOST_KEY1.equals(realIp)
				&& !Constant.LOCALHOST_KEY2.equals(realIp)) {

			log.error("realIp:{},调用下线接口被阻止。", realIp);
			return HttpStatus.HTTP_FORBIDDEN;
		}
		String serviceName = nacosDiscoveryProperties.getService();
		String groupName = nacosDiscoveryProperties.getGroup();
		String clusterName = nacosDiscoveryProperties.getClusterName();
		String ip = nacosDiscoveryProperties.getIp();
		int port = nacosDiscoveryProperties.getPort();
		log.info("deregister from nacos, serviceName:{}, groupName:{}, clusterName:{}, ip:{}, port:{}", serviceName, groupName, clusterName, ip, port);
		nacosServiceRegistry.deregister(nacosRegistration);
		return HttpStatus.HTTP_OK;
	}

	private String getRealIp(ServerWebExchange exchange) {
		return LogFilter.getRealIp(exchange.getRequest()).substring(1,16).split(":")[0];
	}

	/**
	 * 健康检查
	 * @return HttpStatus
	 */
	@GetMapping(value = "/health")
	public int healthCheck() {
		return HttpStatus.HTTP_OK;
	}


}
